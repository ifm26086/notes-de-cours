import React from 'react';

import styles from './Video.module.css'

export default function Video(props) {
    return <div className={ styles.video }>
        <div>
            <iframe 
                title={ props.title }
                src={ props.src }
                frameBorder="0" 
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                allowFullScreen>
            </iframe>
        </div>
    </div>
}
