import React, { useState } from 'react'

import styles from './Switch.module.css'

export default function Switch(props) {
    const [checked, setChecked] = useState(props.checked);

    const handleChange = (event) => {
        setChecked(event.target.checked);
        props.onChange(event);
    }

    return <label className={ styles.switch }>
        <input type="checkbox" checked={ checked } onChange={ handleChange } />
        <span className={ styles.slider }></span>
    </label>
}
