import React, { lazy } from 'react';
import { Helmet } from 'react-helmet-async';
import Breadcrumb from './Breadcrumb';

import styles from './Page.module.css';
import PageSwitcher from './PageSwitcher';

export default function Page(props) {
    const PageComponent = lazy(() => import('../pages/' + props.page.component))
    return <>
        <Helmet>
            <title>{ props.page.title }</title>  
            <meta name="description" content={ props.page.description } />   
        </Helmet>

        <Breadcrumb section={ props.section } page={ props.page } />

        <section className={ styles.page }>
            <h1>{ props.page.title }</h1>
            <PageComponent />
        </section>

        <PageSwitcher section={ props.section } page={ props.page } />
    </>
}