import React, { useRef, useState } from 'react';
import { Link, NavLink } from "react-router-dom";
import { ThemeContext } from '../ThemeContext'
import Logo from './Logo'
import SearchBar from './SearchBar'
import Switch from './Switch'
import useOnClickOutside from './useOnClickOutside';

import data from '../data';

import styles from './Header.module.css'

export default function Header(props) {
    const [open, setOpen] = useState(false);
    const [searchOpen, setSearchOpen] = useState(false);
    const headerRef = useRef(null);

    const switchTheme = (setTheme) => {
        return (event) => {
            setTheme(event.target.checked ? 'dark' : 'light');
        }
    }

    const toggleSearchBar = () => {
        setSearchOpen((searchOpen) => !searchOpen);
    }
    
    const toggleMenu = () => {
        if(window.innerWidth < 640){
            setOpen((open) => !open);
        }
    }

    const closeMenu = () => {
        if(open){
            toggleMenu();
        }
    }

    useOnClickOutside(headerRef, closeMenu);

    return <header ref={ headerRef } className={ open ? styles.open : '' }>
        <nav>
            <div className={ styles.menu }>
                <Link to="/"><Logo /></Link>
                <button onClick={ toggleMenu }><i className="material-icons">menu</i></button>
            </div>
            <ul className={ styles.list }>
                { Object.values(data.groups).filter((group) => !group.noIndex).map((group) => (
                    <li key={ group.id } className={ searchOpen ? styles.hidden : '' }>
                        <NavLink exact strict to={ '/group/' + group.id } activeClassName={ styles.active } onClick={ closeMenu }>
                            { group.label }
                        </NavLink>
                    </li>
                )) }
                <li className={ styles.search }>
                    <SearchBar visible={ searchOpen } onSearch={ closeMenu } onToggleSearchBar={ toggleSearchBar }/>
                </li>
                <li className={ styles.switch }>
                    <ThemeContext.Consumer> 
                        { themeContext => (
                            <Switch checked={ themeContext.theme === 'dark' } onChange={ switchTheme(themeContext.setTheme) } />
                        )} 
                    </ThemeContext.Consumer>
                </li>
            </ul>
        </nav>
    </header>
}
