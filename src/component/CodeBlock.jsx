import React, { useState } from 'react';
import AnimateHeight from 'react-animate-height';
import { PrismLight as SyntaxHighlighter } from 'react-syntax-highlighter';
import { ThemeContext } from '../ThemeContext';
import { syntaxHiglight as syntaxHighlight } from '../settings';

import styles from './CodeBlock.module.css';

export default function CodeBlock(props) {
    // Shortcut variable to the language object used
    const languageObject = syntaxHighlight.languages[props.language];

    /**
     * Load a language for the syntax highlighter.
     * @param {Object} languageObject The language object from the settings.
     */
    const loadLanguage = async (languageObject) => {
        // Check if this language highlighting is already loaded
        if(!languageObject.loaded) {
            // If the language needs dependencies, load them at first
            if(Array.isArray(languageObject.dependencies)) {
                for(let dependency of languageObject.dependencies){
                    loadLanguage(syntaxHighlight.languages[dependency]);
                }
            }
            else if(languageObject.dependencies) {
                loadLanguage(syntaxHighlight.languages[languageObject.dependency]);
            }
            
            // Mark the language as loaded
            languageObject.loaded = true;

            // Load the language highlighting
            if (languageObject.parser) {
                const language = (await import(`react-syntax-highlighter/dist/esm/languages/prism/${languageObject.parser}`)).default;
                
                // Register the language in the syntax highlighter
                SyntaxHighlighter.registerLanguage(languageObject.parser, language);
            }
        }
    }

    // Is loaded variable indicating whether the coding language highlighting is loaded
    const [isLoaded, setLoaded] = useState(false);
    const [height, setHeight] = useState(0);
    loadLanguage(languageObject).then(() => {
        setLoaded(true);
        setHeight('auto');
    });


    return <div className={ styles.container }>
        <div className={ styles.tag }>{ languageObject.name }</div>
            { props.children && isLoaded && 
                <ThemeContext.Consumer>
                { themeContext => (
                    <AnimateHeight
                        height={ height }
                        duration={ 300 }
                        animateOpacity={ true }>
                        <SyntaxHighlighter 
                            language={ languageObject.parser } 
                            style={ syntaxHighlight.themes[themeContext.getSyntaxHighlightStyle()] }
                            customStyle={ { margin: 0, border: 'none' } }>
                            { props.children }
                        </SyntaxHighlighter>
                    </AnimateHeight>
                )}
                </ThemeContext.Consumer>
            }
    </div>
}
