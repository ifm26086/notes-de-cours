import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import CodeBlock from './CodeBlock';

import styles from './WebExample.module.css';

/**
 * Code component that sould be inside the WebExample component.
 */
function Code(props) {
    return <>
        { props.display &&
            <CodeBlock language={ props.type }>
                { props.children }
            </CodeBlock>
        }
    </>
}

// Properties validation
Code.propTypes = {
    display: PropTypes.bool,
    type: function(props, propName, componentName){
        if(!['html', 'css', 'js'].includes(props[propName])){
            return new Error(
                'Invalid prop `' + propName + '` supplied to `' + 
                componentName + '`. The prop `' + propName + '` should ' + 
                'have a value of either `html`, `css` or `js`. Validation ' + 
                'failed.'
              );
        }
    }
}

// Properties default value
Code.defaultProps = {
    display: true
}

/**
 * Web example component that can contain HTML, CSS or Javascript code to 
 * display as code and in a frame.
 */
function WebExample(props) {
    /**
     * Additionnal height (in pixel) to give to iframe to be sure to clear an 
     * horizontal scrollbar.
     */
    const IFRAME_HEIGHT_BUFFER = 20;

    /**
     * HTML Iframe reference.
     */
    const iframeRef = useRef(null);

    const getCode = () => {
        let code = { html: '', css: '', js: ''}
        const codeTypes = [
            { name: 'html' }, 
            { name: 'css', tag: 'style' }, 
            { name: 'js', tag: 'script' }
        ];

        React.Children.forEach(props.children, child => {
            if(child.type !== Code){
                return;
            }

            for(let type of codeTypes){
                if(child.props.type === type.name){
                    if(type.tag){
                        code[type.name] = `<${ type.tag }>${ child.props.children }</${ type.tag }>`;
                    }
                    else{
                        code[type.name] = child.props.children;
                    }
                }
            }
        });

        return code.css + code.html + code.js;
    }

    const code = getCode();

    useEffect(() => {
        const iFrameDocument = iframeRef.current.contentWindow.document;

        // Write to iframe
        iFrameDocument.open();
        iFrameDocument.write(code);
        iFrameDocument.close();

        // Set iframe height
        for(let img of iFrameDocument.images){
            img.onload = () => { 
                iframeRef.current.height = iFrameDocument.body.scrollHeight + IFRAME_HEIGHT_BUFFER;
            } 
        }
    
        iframeRef.current.height = iFrameDocument.body.scrollHeight + IFRAME_HEIGHT_BUFFER;
    }, [iframeRef, code, props.children]);

    return <div className={ styles.container }>
        { props.children }
        <iframe ref={ iframeRef } title={ props.title }></iframe>
    </div>
}

WebExample.Code = Code;

export default WebExample;
