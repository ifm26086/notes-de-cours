import React, { useEffect, useState } from 'react';

import styles from './DownloadBlock.module.css';

function File(props) {
    const name = props.name.substring(0, props.name.lastIndexOf('.'));
    const extension = props.name.substring(props.name.lastIndexOf('.')).toLowerCase();
    const [size, setSize] = useState(0);
    useEffect(() => {
        const controller = new AbortController();
        fetch(props.path, { method: 'HEAD', signal: controller.signal })
            .then((response) => setSize(response.headers.get('content-length')))
            .catch(() => {});

        return () => {
            controller.abort();
        }
    }, [props.path]);

    const isArchive = () => {
        return ['.zip', '.7z', '.tar', '.gz', '.rar', '.bz2'].includes(extension);
    }

    const displaySize = () => {
        const UNIT = ['B', 'KB', 'MB', 'GB'];
        let displaySize = size;
        let unitIndex = 0;
        while(displaySize > 100){
            displaySize /= 1024;
            unitIndex++;
        }

        return displaySize.toFixed(2) + UNIT[unitIndex];
    }

    return <a className={ styles.file } href={ props.path } download={ props.name }>
        <i className="material-icons">
            { isArchive() ? 'archive' : 'insert_drive_file' }
        </i>
        <div className={ styles.info }>
            <span className={ styles.name }>
                <span>{ name }</span>
                <span>{ extension }</span>
            </span>
            <span className={ styles.size }>{ displaySize() }</span>
        </div>
    </a>
}

function DownloadBlock(props) {
    return <div className={ styles.block }>
        { props.children }
    </div>
}

DownloadBlock.File = File;

export default DownloadBlock;
