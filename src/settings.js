// Syntax highlighter imports for styles
import vs from 'react-syntax-highlighter/dist/esm/styles/prism/vs';
import vsdark from 'react-syntax-highlighter/dist/esm/styles/prism/vsc-dark-plus';

// Settings object
let settings = {
    naming: {
        title: 'Programmation Internet II',
        sections: 'Module'
    },
    themes: {
        default: 'light',
        properties: {
            light: {
                bgColor: '#fff',
                bgAccentColor: '#de3838',
                bgAccentGradientColor: '#f56155',
                bgInteractiveColor: '#6d6a62',
                bgInteractiveAccentColor: '#f7d1d1',
                bgHighlight: '#ffdbdb',
                bgHighlightAccent: '#f7d1d1',
                borderInteractiveColor: '#f5afa9',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#222',
                textInvertedColor: '#fff',
                textAsideColor: '#222',
                textSwitcherColor: '#de3838',
                syntaxHighlightStyle: 'vs'
            },
            dark: {
                bgColor: '#2e2a2a',
                bgAccentColor: '#c7244f',
                bgAccentGradientColor: '#ce4a6b',
                bgHighlight: '#918183',
                bgHighlightAccent: '#c7244f',
                bgInteractiveColor: '#fac3c9',
                bgInteractiveAccentColor: '#ffdee4',
                borderInteractiveColor: '#f24668',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#fff',
                textInvertedColor: '#fff',
                textAsideColor: '#fcb8c0',
                textSwitcherColor: '#fcb8c0',
                syntaxHighlightStyle: 'vsdark'
            }
        }
    },
    syntaxHiglight: {
        languages: {
            html: { name: 'HTML', parser: 'markup' },
            css: { name: 'CSS', parser: 'css' },
            js: { name: 'Javascript', parser: 'javascript' },
            php: { name: 'PHP', parser: 'php' },
            csharp: { name: 'C#', parser: 'csharp' },
            xml: { name: 'XML', parser: 'markup' },
            json: { name: 'JSON', parser: 'json' },
            shell: { name: 'Terminal', parser: 'powershell' },
            apache: { name: 'Apache', parser: 'apacheconf' },
            properties: { name: 'Propriétés', parser: 'properties' },
            handlebars: { name: 'Handlebars', parser: 'handlebars' },
            procfile: { name: 'Procfile' }
        },
        themes: {
            vs: vs,
            vsdark: vsdark
        }
    }
}

export default settings;
export let naming = settings.naming;
export let themes = settings.themes;
export let syntaxHiglight = settings.syntaxHiglight;