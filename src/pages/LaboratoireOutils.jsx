import React from 'react';

export default function LaboratoireOutils() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <ol>
                <li>Installer Google Chrome ou autre navigateur supportant les dernières fonctionnalités du web</li>
                <li>Installer Visual Studio Code ou autre éditeur de code</li>
                <li>Installer Node.js</li>
                <li>Installer Postman</li>
                <li>Installer SQLiteStudio</li>
                <li>Installer Git</li>
            </ol>
        </section>
    </>;
}
