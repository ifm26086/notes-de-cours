import React from 'react';
import IC from '../component/InlineCode';
import Video from '../component/Video';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import session from '../resources/session.png';

const importation =
`import session from 'express-session';
import memorystore from 'memorystore';`;

export default function SessionIntro() {
    return <>
        <section>
            <h2>Concept</h2>
            <p>
                Une session est un moyen de sauvegarder des informations de façon temporaire pour chaque connexion 
                courante à notre application Web. Essentiellement, chaque client connecté à votre application a un 
                espace mémoire pour y stocker des données sur le serveur. Si le client n'intéragit pas avec le 
                serveur pendant un certain temps, ces données sont automatiquement supprimé pour faire de la place 
                sur le serveur.
            </p>
            <p>
                Cette pratique est donc très utile pour les services de connexion, pour les applications utilisant
                beaucoup de communication en temps réel, pour amasser de l'information sur des utilisateurs ou
                simplement pour enregistrer des données temporaires pour chaque utilisateur.
            </p>
        </section>

        <section>
            <h2>Fonctionnement</h2>
            <p>
                Pour transférer l'information sur les connexions entre le client et le serveur, une session utilisera
                un petit espace mémoire dans les requêtes et les réponses HTTP. Cet espace mémoire est appelé
                un <IC>cookie</IC>. Un cookie permettra au serveur de sauvegarder des données qui seront copié et
                envoyé automatiquement dans chaque requête et réponse HTTP. 
            </p>
            <p>
                Un des problèmes du cookie est qu'il n'est pas possible de sauvegarder beaucoup d'information dedans.
                C'est le cas puisque l'on ne veut pas que les requêtes et réponses HTTP deviennent trop volumineuses.
                Puisque nous n'avons pas beaucoup d'espace, le serveur sauvegardera typiquement un identifiant (ID)
                dans le cookie. Cet identifiant correspondera à une entrée dans une base de données dans laquelle la
                session conservera les données sur la connexion. Ces données peuvent être n'importe quoi. C'est le
                programmeur du serveur qui décide ce qu'il veut sauvegarder.
            </p>
            <img src={session} alt="Fonctionnement des sessions sur un serveur web" />
            <p>
                Puisque le serveur garde des informations sur chaque connexion, la base de données peut rapidement
                devenir très volumineuse. Pour régler ce problème, au bout d'un certain temps, le serveur va vouloir
                supprimer les données de connexion qui n'ont pas été utilisé depuis un certain temps, ce qu'il fera
                automatiquement.
            </p>
            <ColoredBox heading="Attention">
                <p>
                    L'utilisation de session avec des cookies est aujourd'hui régit par plusieurs lois strictes dans
                    plusieurs pays. En effet, il est aujourd'hui demandé aux sites web d'avoir l'accord de leurs
                    utilisateurs pour utiliser les cookies. Ces règles ont été créé après que de nombreuses entreprises,
                    comme Amazon et Facebook, commencent à utiliser les cookies pour amasser de l'information sur des
                    utilisateurs dans le but de la vendre à d'autres partie.
                </p>
                <p>
                    Bref, si vous voulez utiliser les sessions, vous utilisez des cookies. Vous devez donc ajouter une
                    façon d'avertir vos utilisateurs que vous utilisez des cookies et de les laisser accepter ou refuser
                    leur utilisation. Pour en apprendre d'avantage, voici la loi qui régit les cookies au Canada:
                </p>
                <a href="https://crtc.gc.ca/fra/internet/anti.htm" target="_blank"  rel="noopener noreferrer">
                    La Loi canadienne anti-pourriel
                </a>
            </ColoredBox>
        </section>

        <section>
            <h2>Installation</h2>
            <p>
                Pour utiliser les sessions, il faut commencer par installer le middleware nous permettant de gérer
                les sessions. Vous pouver le faire en exécutant la commande suivante dans un terminal dans votre
                projet:
            </p>
            <CodeBlock language="shell">{'npm install express-session'}</CodeBlock>
            <p>
                Les sessions doivent être sauvegardé à quelque part sur le serveur. Avec Express, il y a plusieurs
                façon de faire la sauvegarde des sessions. Nous avons théoriquement beaucoup de choix. Nous pourrions
                entres autres sauvegarder les sessions dans une base de données, dans un fichier ou simplement les
                garder en mémoire. C'est cette dernière option que nous utiliserons. Nous utiliserons simplement une
                base de données mémoire qui se supprime dès que le serveur est redémarré. Il faut toutefois
                télécharger et installer un autre package.
            </p>
            <CodeBlock language="shell">{'npm install memorystore'}</CodeBlock>
            <p>
                Une fois installé, nous devons accéder à ces 2 librairies de code dans votre 
                fichier <IC>server.js</IC>. Vous devez donc ajouter les <IC>import</IC> nécessaires.
            </p>
            <CodeBlock language="js">{importation}</CodeBlock>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Sessions - Conserver des données sur les connexions" src="https://www.youtube.com/embed/XRCyNIro9Bc" />
        </section>
    </>
}