export default function RevisionIntro() {
    return <>
        <section>
            <h2>Cours précédent</h2>
            <p>
                Pour ce cours, nous supposons que vous connaisez les bases
                de la programmation Web client, c'est à dire que vous êtes
                capable de construire des interfaces graphiques, de les styler et de
                programmer certains comportements de base dans ceux-ci. L'important n'est pas de connaître par coeur
                comment tout programmer, mais plutôt de savoir en général comment développer vos applications et 
                comment trouver rapidement et efficacement l'information nécessaire pour votre développement.
            </p>
            <p>
                Si vous n'êtes pas totalement à l'aise avec ce contenu, n'hésitez pas à visiter le site web du cours
                précédant pour relire la matière et de faire les exercices avec lesquels vous avez de la difficulté. 
                Vous pouvez trouver le site Web du cours précédant ici:
            </p>
            <p>
                <a href="https://ord14737.gitlab.io/notes-de-cours/" target="_blank" rel="noopener noreferrer">Introduction à la programmation web client</a>
            </p>
            <p>
                Les pages de ce module vous donneront quelques références supplémentaires à aller voir si vous en
                avez besoin.
            </p>
        </section>
    </>
}