import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';
import DownloadBlock from '../component/DownloadBlock';

import formToObject from '../resources/formToObject.txt';

const sendForm = 
`import formToObject from './formToObject.js';

form.addEventListener('submit', (event) => {
    event.preventDefault();

    // Envoyer le formulaire uniquement s'il est valide
    if(form.checkValidity()){
        // Contruire un objet avec le formulaire
        let data = formToObject(form);

        // Envoyer les données au serveur
        fetch('/route-pour-envoyer', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    }
});`;

const groupCheckbox =
`{ 
    "nomGroupeCheckbox": [
        "valeurChecked1",
        "valeurChecked2",
        "valeurChecked5",
    ] 
}`;

const multipleSelect = 
`{ 
    "nomSelect": [
        "optionSelect1",
        "optionSelect3",
        "optionSelect4",
    ] 
}`;

const exampleObject =
`{
    "username": "Test1212",
    "password": "AlloMaman12",
    "rememberMe": true,
    "services": [
        "admin",
        "management",
        "user"
    ]
}`;

class ValidationFormToObject extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Une fois que la validation du côté client est faite, il est temps d'envoyer nos données au
                    serveur. Organiser et mettre les données pour une requête qui nécessite une ou 2 valeur à envoyer
                    est simple, mais lorsqu'on doit envoyer une très grande quantité de données, notre code peut
                    rapidement devenir complexe. 
                </p>
                <p>
                    Un exemple typique est celle d'un formulaire contenant plusieurs
                    pages de questions. Convertir l'ensemble d'un formulaire en objet pour l'envoyer en JSON est une
                    tâche définitivement répétitive et longue. Nous allons donc utiliser la fonction de code
                    Javascript suivante pour faire la conversion automatiquement:
                </p>
                <DownloadBlock>
                    <DownloadBlock.File path={ formToObject } name="formToObject.js" />
                </DownloadBlock>
            </section>

            <section>
                <h2>Utilisation</h2>
                <p>
                    Ce fichier Javascript crée une fonction qui se nomme <IC>formToObject()</IC>. Pour pouvoir 
                    utiliser cette fonction, vous devrez tout d'abords ajouter ce fichier Javascript dans votre page 
                    HTML. Ajouter donc ce fichier le dossier <IC>/public/js/</IC> et ajouter le code suivant dans 
                    votre fichier <IC>index.html</IC>:
                </p>
                <CodeBlock language="html">{'<script type="module" src="./js/formToObject.js"></script>'}</CodeBlock>
                <p>
                    Cette fonction convertit les champs d'un formulaire automatiquement dans un objet quii est prêt à
                    être envoyé au serveur. Pour ce faire, il est donc nécessaire de mettre tous les champs dans une
                    balise <IC>&lt;form&gt;</IC>
                </p>
                <p>
                    Nous voulons généralement utiliser cette fonction lorsque le formulaire est valide et que celui 
                    est prêt à être soumis au serveur. Cette fonction prendra en paramètre le formulaire HTML que vous 
                    voulez soumettre et le convertira en objet, que vous pourrez par la suite convertir en JSON pour 
                    l'envoyer.
                </p>
                <CodeBlock language="js">{sendForm}</CodeBlock>
                <ColoredBox heading="Attention">
                    N'oubliez pas de mettre le <IC>import</IC> en haut de votre code Javascript, comme dans l'exemple
                    ci-dessus. Assurez-vous aussi que le chemin vers le fichier Javascript est le bon. Cette
                    importation vous permettra d'accéder à la fonction <IC>formToObject</IC>.
                </ColoredBox>
            </section>

            <section>
                <h2>Fonctionnement</h2>
                <p>
                    La fonction <IC>formToObject()</IC> utilise l'attribut <IC>name</IC> et <IC>value</IC> des <IC>{ '<input>' }</IC>, <IC>{ '<textarea>' }</IC> et <IC>{ '<select>' }</IC>.
                    Vous devez donc spécifier un attribut <IC>name</IC> pour tout les champs que vous voulez envoyer 
                    au serveur dans le HTML. De plus, la valeur envoyé au serveur sera celle spécifié dans l'attribut <IC>value</IC>.
                    Il sera donc nécessaire de spécifier l'attribut <IC>value</IC> pour tous les champs qui ne sont 
                    pas des champs textuels ou numériques.
                </p>
                <p>
                    Voici comment les champs d'un formulaire sont convertis en objet:
                </p>
                <dl>
                    <dt>Champs textuel et numérique</dt>
                    <dd>
                        La fonction retourne simplement la valeur écrite dans le champ.
                        <CodeBlock language="json">{ '{ "nomChamp": "valeur" }' }</CodeBlock>
                    </dd>
                    <dt>Radio Buttons</dt>
                    <dd>
                        La fonction retourne l'attribut <IC>value</IC> du radio button sélectionné dans le groupe.
                        <CodeBlock language="json">{ '{ "nomGroupeRadio": "valeurRadioSelect" }' }</CodeBlock>
                    </dd>
                    <dt>Checkbox unique</dt>
                    <dd>
                        Si un checkbox ne possède pas d'attribut <IC>value</IC>, la fonction retourne simplement s'il 
                        est coché ou non à l'aide d'un booléen.
                        <CodeBlock language="json">{ '{ "nomCheckbox": true/false }' }</CodeBlock>
                    </dd>
                    <dt>Checkbox en groupe</dt>
                    <dd>
                        Si un checkbox possède un attribut <IC>value</IC>, la fonction retourne un tableau contenant 
                        toutes les valeurs des checkbox qui sont cochés ayant le même attribut <IC>name</IC>.
                        <CodeBlock language="json">{ groupCheckbox }</CodeBlock>
                    </dd>
                    <dt>Select à choix unique</dt>
                    <dd>
                        Retourne l'attribut <IC>value</IC> de l'option sélectionnée dans le select.
                        <CodeBlock language="json">{ '{ "nomSelect": "valeurOptionSelect" }' }</CodeBlock>
                    </dd>
                    <dt>Select à choix multiple</dt>
                    <dd>
                        Retourne un tableau contenant l'attribut <IC>value</IC> de toutes les options sélectionnées 
                        dans le select.
                        <CodeBlock language="json">{ multipleSelect }</CodeBlock>
                    </dd>
                </dl>
                <p>
                    Si vous avez plusieurs champs dans votre formulaire, l'objet bâtit contiendra tous les champs 
                    dans le même objet de la façon suivante:
                </p>
                <CodeBlock language="json">{ exampleObject }</CodeBlock>
            </section>
        </>;
    }
}

export default ValidationFormToObject;
