import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';
import Video from '../component/Video';

import gabarit from '../resources/gabarit-base.zip';

export default function TemplateBase() {
    return <>
        <section>
            <h2>Guide d'utilisation</h2>
            <p>
                Pour utiliser ce gabarit, veuillez suivre les étapes suivates:
            </p>
            <ol>
                <li>Télécharger le gabarit</li>
                <li>Décompresser le gabarit</li>
                <li>Changer le nom du dossier du projet</li>
                <li>
                    Ouvrir le fichier <IC>package.json</IC>
                    <ol>
                        <li>Modifier le <IC>name</IC> du package</li>
                        <li>Modifier la <IC>description</IC> du package</li>
                        <li>Modifier l' <IC>author</IC> du package</li>
                    </ol>
                </li>
                <li>
                    Ouvrir un terminal dans le dossier du gabarit
                    <ol>
                        <li>Lancer la commande <IC>npm install</IC></li>
                        <li>Lancer la commande <IC>npm start</IC></li>
                    </ol>
                </li>
            </ol>
            <p>
                Ce gabarit de base contient un projet express vide contenant quelques middlewares utiles ainsi que 
                quelques outils de développement pratique pour créer un serveur web simple.
            </p>
        </section>

        <section>
            <h2>Fichiers supplémentaires</h2>
            <p>
                Contrairement à ce qu'il y a de montré dans les autres pages de note, j'utilise une configuration 
                spéciale pour Helmet dans ce gabarit. C'est le fichier <IC>csp-options.js</IC>. Vous n'avez pas 
                besoin de comprendre tout ce qui se passe ici, mais sachez que c'est pour être moins sévère avec 
                les règles de sécurité en développement. Essentiellement, ça nous permet d'utiliser certains outils 
                que nous verront plus tard lors du développement.
            </p>
            <p>
                Vous remarquerez aussi un fichier <IC>.gitignore</IC>. Ce fichier permet de mettre votre projet sur 
                Git de façon sécuritaire et efficace en ignorant le fichier <IC>.env</IC> et le dossier <IC>node_modules</IC>.
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <p>
                <Video title="Création d'un gabarit de serveur web simple" src="https://www.youtube.com/embed/ZUj-O-Rc-eQ" />
            </p>
        </section>

        <section>
            <h2>Téléchargement</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ gabarit } name="gabarit-base.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>
}