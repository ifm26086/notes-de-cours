import React from 'react';
import CodeBlock from '../component/CodeBlock'

export default function NJSIntro() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Node.js est un environnement d'exécution de Javascript à open source et multiplateforme qui 
                s'exécute à l'extérieur d'un navigateur Web, soit dans un terminal ou au travers de services. On
                l'utilise principalement pour le développement de serveur Web, mais aussi pour le développement 
                d'outils liés au Web. Node.js nous permettra donc de créer des serveurs Web pour que nos 
                applications ou sites Web puissent s'y connecter. Node.js a été initialement publié en 2009.
            </p>
            <p>
                Un de ses buts était d'avoir l'option de créer des serveurs Web avec le langage Javascript puisque 
                ce langage était déjà utilisé pour la programmation des interfaces graphiques Web. Ceci permettait 
                d'avoir un seul langage de programmation pour tout le développement d'une application Web 
                contrairement à avant, où la majorité des serveurs Web était programmé en PHP, Java, C# et même 
                VB.NET.
            </p>
            <p>
                Un autre des buts importants de Node.js était d'être beaucoup plus efficace que les autres 
                serveurs Web. En effet, les serveurs Web reçoivent aujourd'hui une quantité phénoménale de 
                requêtes. Par le passé, ces serveurs utilisaient multiples fils d'exécutions parallèles pour 
                répondre à ces besoins. Toutefois, cette façon de faire était hautement limité par le processeur
                du serveur. De plus, certains fil d'exécution pouvait être en dormance pendant plusieurs secondes 
                lors de certains traitements, ce qui gaspillait du temps de traitement. Node.js règle ces 
                problèmes en utilisant sa boucle d'évènement asynchrone.
            </p>
            <p>
                Pour tester que Node.js est bien installé et fonctionne correctement, vous pouvez utiliser la commande 
                suivante dans un terminal:
            </p>
            <CodeBlock language="shell">$ node --version</CodeBlock>
        </section>

        <section>
            <h2>NPM</h2>
            <p>
                NPM est un gestionnaire de package qui vient avec Node.js. Il était autrefois l'acronyme de Node 
                Package Manager (aujourd'hui, on est plus vraiment certain). NPM est en fait une banque de code en
                ligne qui permet facilement aux développeurs d'ajouter des librairies de codes à leur projet. Nous
                utiliseront NPM à plusieurs reprises durant la session pour trouver ou ajouter des librairies de 
                code ou pour créer nos projets.
            </p>
            <p>
                Pour tester que NPM est bien installé et fonctionne correctement, vous pouvez utiliser la commande 
                suivante dans un terminal:
            </p>
            <CodeBlock language="shell">$ npm --version</CodeBlock>
        </section>
    </>;
}
