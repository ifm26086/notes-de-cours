import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const importation =
`import https from 'https';
import { readFile } from 'fs/promises';`;

const start =
`console.info('Serveurs démarré:');
if (process.env.NODE_ENV === 'production') {
    app.listen(process.env.PORT);
    console.info(\`http://localhost:${process.env.PORT}\`);
}
else {
    const credentials = {
        key: await readFile('./security/localhost.key'),
        cert: await readFile('./security/localhost.cert')
    }

    https.createServer(credentials, app).listen(process.env.PORT);
    console.info(\`https://localhost:${process.env.PORT}\`);
}`;

const redirect =
`export default function redirectToHTTPS(request, response, next) {
    if(process.env.NODE_ENV === 'development') {
        if(!request.secure) {
            return response.redirect('https://' + request.headers.host + request.url);
        }

        next();
    }
    else {
        if(request.headers["x-forwarded-proto"] !== "https") {
            return response.redirect('https://' + request.headers.host + request.url);
        }

        next();
    }
}`;

const middleware =
`// Importer les fichiers et librairies
// ...
import redirectToHTTPS from './redirect-to-https.js';

// ...

// Ajout de middlewares
// ...
app.use(redirectToHTTPS);

// ...`;

export default function HTTPSConfig() {
    return <>
        <section>
            <h2>Sécuriser le serveur</h2>
            <p>
                Pour sécuriser le serveur, nous avons tout d'abords besoin d'une nouvelle librairie. Il faut
                effectivement importer la librairie <IC>https</IC> qui vient déjà avec Node.js. Nous aurons aussi
                besoin de lire les fichier de certificat et de clé, donc la librairie <IC>fs</IC> sera nécessaire.
                Mettez ces importations dans votre fichier <IC>server.js</IC>.
            </p>
            <CodeBlock language="js">{importation}</CodeBlock>
            <p>
                Nous devons ensuite changer la façon dont nous lançons notre serveur. Nous allons donc modifier le
                démarrage au bas du fichier <IC>server.js</IC> pour y mettre ceci:
            </p>
            <CodeBlock language="js">{start}</CodeBlock>
            <p>
                Juste avec ces modifications, le serveur va maintenant se lancer en HTTPS lorsque nous sommes en
                développement. 
            </p>
            <ColoredBox heading="À noter">
                Vous noterez que lorsque le serveur n'est pas en mode developpement, donc lorsqu'il est en mode de
                production, il utilise toujours la vieille méthode. Ceci peut sembler bizarre puisqu'on n'utilise pas
                HTTPS en production. Ceci est dû au fait que la plupart des serveurs Node.js roulent en production
                derrière un proxy inverse comme Nginx qui s'occupera de sécuriser notre application. En développement,
                l'installation et la configuration d'un proxy inverse est plus compliqué, donc nous ne l'utiliserons
                pas.
            </ColoredBox>
        </section>

        <section>
            <h2>Redirigé vers le HTTPS</h2>
            <p>
                Le lancement du serveur en HTTPS fonctionne bien, mais si on essais d'accéder à notre serveur à partir
                du protocole HTTP, par exemple en entrant l'adresse <IC>http://localhost:5000</IC> dans votre
                navigateur, vous verrez que les pages ne sont plus accessible avec le protocole HTTP. Une bonne
                pratique est donc de rediriger les appels HTTP vers les pages HTTPS automatiquement.
            </p>
            <p>
                Pour ce faire, nous créerons un middleware pour notre serveur. Nous le créerons dans un fichier
                nommé <IC>redirect-to-https.js</IC> que nous mettrons à racine de notre projet. Voici son contenu:
            </p>
            <CodeBlock language="js">{redirect}</CodeBlock>
            <p>
                La technique est simple, si l'utilisateur utilise une route non sécurisé, nous le redirigeons vers
                une route sécurisé. Pour ajouter ce middleware, vous pouvez tout simplement mettre le code suivant
                dans votre fichier <IC>server.js</IC>.
            </p>
            <CodeBlock language="js">{middleware}</CodeBlock>
            <ColoredBox heading="À noter">
                Vous noterez encore une fois que le travail est différent dans le middleware si Node.js est en
                développement ou en production. Effectivement, en production, nous
                utilisons <IC>x-forwarded-proto</IC> pour déterminer si notre proxy inverse a reçu une requête
                sécurisé ou non.
            </ColoredBox>
        </section>
    </>
}
