import React from 'react';
import IC from '../component/InlineCode'
import ColoredBox from '../component/ColoredBox';
import DownloadBlock from '../component/DownloadBlock';

import distibue from '../resources/laboratoire-bogue-distribué.zip'
import solution from '../resources/laboratoire-bogue-solution.zip'

export default function LaboratoireBogue() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Pour chacun des numéros que vous retrouverez dans le HTML du project distribué, suivez les étapes 
                suivantes:
            </p>
            <ol>
                <li>
                    Retirer les commentaires de la question que vous voulez faire dans le fichier <IC>main.js</IC>.
                </li>
                <li>
                    Retirer les commentaires de la question que vous voulez faire dans le fichier <IC>server.js</IC>.
                </li>
                <li>
                    Corriger les erreurs dans le code selon la situation expliqué dans le HTML.
                </li>
            </ol>
            <p>
                Assurez-vous de qu'il n'y ait plus d'erreur à la question précédante avant de commencer la question 
                suivante. Cela vous évitera des conflits d'erreur entre les questions.
            </p>

            <ColoredBox heading="Attention">
                Le laboratoire utilise des concepts de validation que vous n'avez peut-être pas encore vu en classe.
                Si vous ne comprenez pas certains morceaux de code, n'hésitez pas à vous fier à la solution.
            </ColoredBox>
        </section>

        <section>
            <h2>Solution et fichiers de départ</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
};
