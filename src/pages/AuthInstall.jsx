import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const install =
`npm install express-session
npm install memorystore
npm install passport
npm install passport-local
npm install bcrypt`;

const session =
`// Importations
import session from 'express-session';
import memorystore from 'memorystore';
// ...

// Création du serveur
const app = express();
const MemoryStore = memorystore(session);
// ...

// Ajout de middlewares
app.use(session({
    cookie: { maxAge: 3600000 },
    name: process.env.npm_package_name,
    store: new MemoryStore({ checkPeriod: 3600000 }),
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESSION_SECRET
}));
// ...`;

const passport =
`// Importations
import passport from 'passport';
// ...

// Ajout de middlewares
// app.use(session({ ... });
app.use(passport.initialize());
app.use(passport.session());
// ...`;

export default function AuthInstall() {
    return <>
        <section>
            <h2>Librairies de code</h2>
            <p>
                Pour pouvoir créer un système d'authentification simple et sécuritaire avec Express et Node.js, nous
                aurons besoin d'installer plusieurs librairies de code. Les voici:
            </p>
            <CodeBlock language="shell">{install}</CodeBlock>
            <p>
                Ces librairies de code ont chacune leur rôle à jouer dans notre système d'authentification. Voici le
                rôle que chacune aura à faire:
            </p>
            <dl>
                <dt>express-session</dt>
                <dd>
                    Permet de créer des sessions sur les différentes connexion au serveur web. Dans notre système
                    d'authentification, on l'utilisera pour stocker les données de l'utilisateur lorsqu'il sera
                    connecté.
                </dd>

                <dt>memorystore</dt>
                <dd>
                    Base de données qui sera utilisé pour stocker les sessions des utilisateurs.
                </dd>

                <dt>passport</dt>
                <dd>
                    Librairie permettant de gérer l'authentification dans un serveur web. Une bonne partie de ce
                    module consiste à configurer cette librairie.
                </dd>

                <dt>passport-local</dt>
                <dd>
                    Librairie associé à <IC>passport</IC> indiquant que l'authentification sera géré à même notre
                    serveur web et non à partir d'un service d'authentification externe, comme Google, Facebook,
                    auth0 ou autres.
                </dd>

                <dt>bcrypt</dt>
                <dd>
                    Permet de chiffrer des mots de passe de façon efficace et sécuritaire. Cette librairie nous
                    permettra aussi de comparer les mots de passe pour s'assurer de leur validité.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Configuration de base</h2>
            <p>
                Dans le fichier <IC>server.js</IC>, nous devrons mettre la configuration de base de nos librairies.
                Les premières configurations à mettre seront celle pour la session:
            </p>
            <CodeBlock language="js">{session}</CodeBlock>
            <p>
                Ensuite, nous devrons lancer les configuration de base de l'authentification avec <IC>passport</IC>:
            </p>
            <CodeBlock language="js">{passport}</CodeBlock>
            <p>
                Comme vous pouvez le constater, nous n'utiliserons pas les
                librairies <IC>bcrypt</IC> et <IC>passport-local</IC> immédiatement. Nous les utiliserons dans d'autres
                fichiers plus tard dans notre installation de l'authentification.
            </p>
        </section>
    </>
}