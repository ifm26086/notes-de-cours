import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

export default class ASPProject extends React.Component {
    render() {
        return <>
            <section>
                <h2>Installation</h2>
                <p>
                    Télécharger et installer la dernière version de la plateforme .NET au lien suivant:
                </p>
                <p>
                    <a href="https://dotnet.microsoft.com/download" target="_blank" rel="noopener noreferrer">
                        Download .NET
                    </a>
                </p>
                <p>
                    Si vous avez déjà installer Visual Studio Community (pas Visual Studio Code), vous devez le mettre 
                    à jour. Même si nous n'utilisons pas directement Visual Studio Community, nous utiliserons 
                    certaines de ses librairies. Cette étape est donc importante. Suivez les instructions suivantes 
                    pour faire la mise à jour:
                </p>
                <ol>
                    <li>
                        Ouvrir l'application <IC>Visual Studio Installer</IC>.
                    </li>
                    <li>
                        Cliquer sur le bouton <IC>Update</IC> dans la section de Visual Studio Community s'il est présent.
                    </li>
                    <li>
                        Patienter et redémarrer votre ordinateur au besoin.
                    </li>
                </ol>
                <p>
                    La plateforme .NET nous permet de faire du développement en HTTPS plus facilement qu'avec d'autres 
                    plateforme. Pour ce faire, vous devez toutefois autoriser les certificats de développement. Vous 
                    pouvez le faire en utilisant suivant les étapes suivantes. 
                </p>
                <ol>
                    <li>
                        Ouvrir un terminal.
                    </li>
                    <li>
                        Entrer la commande suivante:
                        <CodeBlock language="shell">{ 'dotnet dev-certs https --trust' }</CodeBlock>
                    </li>
                </ol>
            </section>

            <section>
                <h2>Créer un projet</h2>
                <p>
                    Pour créer un projet, suivez les étapes suivantes:
                </p>
                <ol>
                    <li>
                        Ouvrir un terminal dans le dossier ou vous voulez créer le projet.
                    </li>
                    <li>
                        Entrer la commande suivante:
                        <CodeBlock language="shell">{ 'dotnet new webapp -o nom_de_projet' }</CodeBlock>
                    </li>
                    <li>
                        Ouvrir le dossier de projet créé dans Visual Studio Code.
                    </li>
                    <li>
                        Télécharger l'extension <IC>C#</IC> de Microsoft dans Visual Studio Code.
                    </li>
                </ol>
                <p>
                    Nous utiliserons Visual Studio Code dans le cours puisqu'il peut être utilisé de la même façon sur 
                    Windows ou MacOS. Si vous voulez plutôt utiliser Visual Studio Community, vous pouvez, mais le 
                    support du professeur pourrait être limité. 
                </p>
                <p>
                    Pour lancer un projet, suivez les étapes suivantes:
                </p>
                <ol>
                    <li>
                        Ouvrir le terminal dans Visual Studio Code.
                    </li>
                    <li>
                        Entrer la commande suivante:
                        <CodeBlock language="shell">{ 'dotnet watch run' }</CodeBlock>
                    </li>
                </ol>
                <p>
                    Le projet devrait ouvrir une page Web en HTTPS sur le port 5001. Si vous modifié le projet, le 
                    serveur Web sera automatiquement relancé et votre navigateur devrait automatiquement se 
                    rafraîchir.
                </p>
            </section>

            <section>
                <h2>Structure d'un projet</h2>
                <p>
                    Dans un projet Web de .NET, vous trouverez les éléments suivants:
                </p>
                <dl>
                    <dt><IC>bin/</IC></dt>
                    <dd>
                        Dossier contenant les exécutables de votre projet. Il est regénéré automatiquement à chaque 
                        fois que vous lancez votre serveur. Ce n'est donc pas très grave si vous le supprimez 
                        accidentellement.
                    </dd>
                    
                    <dt><IC>obj/</IC></dt>
                    <dd>
                        Dossier contenant les librairies C# utilisé par votre projet. C'est l'équivalent 
                        du <IC>node_module</IC> de Node.js. Il est regénéré automatiquement à chaque fois que vous 
                        lancez votre serveur. Ce n'est donc pas très grave si vous le supprimez accidentellement.
                    </dd>
                    
                    <dt><IC>wwwroot/</IC></dt>
                    <dd>
                        Dossier contenant les fichiers statiques que votre application Web utilisera. Vous mettrez en 
                        général vos fichiers CSS et Javascript ici.
                    </dd>

                    <dt><IC>Pages/</IC></dt>
                    <dd>
                        Dossier contenant les différentes pages de votre projet. Chaque page est un couple de 2 
                        fichiers:
                        <ul>
                            <li>
                                Un fichier <IC>.cshtml</IC> qui contient le HTML de la page.
                            </li>
                            <li>
                                Un fichier <IC>.cs</IC> qui contient le code serveur de la page.
                            </li>
                        </ul>
                    </dd>

                    <dt><IC>Pages/Shared</IC></dt>
                    <dd>
                        Dossier contenant les fichiers partagés par toutes les pages. Vous trouverez entre autre le 
                        fichier <IC>_Layout.cshtml</IC> qui est la page maître contenant les éléments réutilisables de 
                        votre page.
                    </dd>
                    
                    <dt><IC>Startup.cs</IC></dt>
                    <dd>
                        Fichier permettant de programmer la configuration du serveur au lancement. Nous ajouterons 
                        certaines configurations dans ce fichiers.
                    </dd>
                    
                    <dt><IC>nom_de_projet.csproj</IC></dt>
                    <dd>
                        Fichier contenant les propriétés de base de votre projet. C'est l'équivalent 
                        du <IC>package.json</IC> dans Node.js. Si vous téléchargez des librairies, vous les verrez 
                        dans ce fichier. Vous pouvez aussi voir la version de la plateforme .NET utilisé par votre 
                        projet.
                    </dd>
                </dl>
                <p>
                    Les autres fichiers ou dossiers qui ne sont pas mentionné ne seront pas important pour nous. Vous 
                    pouvez donc les ignorer.
                </p>
            </section>
        </>;
    }
};
