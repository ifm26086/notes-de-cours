import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const serverVar = 
`namespace app.Pages
{
    public class IndexModel : PageModel
    {
        public List<string> ListeDeHero { get; set; }
        public int Reponse { get; set; }
        public bool EstValide { get; set; }

        public IndexModel()
        {
            this.ListeDeHero = new List<string>();
            this.ListeDeHero.Add("Batman");
            this.ListeDeHero.Add("Ironman");
            this.Reponse = 42;
            this.EstValide = true;
        }
    }
}`;

const razorVar =
`<div>
    <span>Réponse: </span>
    <span>@Model.Reponse</span>
</div>`;

const razorCondLoop =
`@if(Model.EstValide){
    <ul>
        @for(int i = 0 ; i < Model.ListeDeHero.Count ; i++){
            <li>
                <span>@i</span> 
                -
                <span>@Model.ListeDeHero[i]</span>
            </li>
        }
    </ul>
}`;

export default class ASPRazor extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Les pages Razor sont des fichiers HTML dans lequel nous pouvons faire des insertions de code 
                    similaire à C#. Dans votre projet, ce sont les fichier <IC>.cshtml</IC>. Dans un projet Web 
                    ASP.NET, les pages Razor sont toujours accompagné d'un fichier C# <IC>.cs</IC> pour le 
                    comportement de la page sur le serveur.
                </p>
                <p>
                    Pour plus d'information sur la syntaxe des pages Razor, vous pouvez consulter les liens suivants:
                </p>
                <ul>
                    <li>
                        <a href="https://docs.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-5.0" target="_blank" rel="noopener noreferrer">
                            Razor syntax reference for ASP.NET Core
                        </a>
                    </li>
                    <li>
                        <a href="https://docs.microsoft.com/en-us/aspnet/core/razor-pages/?view=aspnetcore-5.0&tabs=visual-studio" target="_blank" rel="noopener noreferrer">
                            Introduction to Razor Pages in ASP.NET Core
                        </a>
                    </li>
                </ul>
                <p>
                    Comme vous pourrez le constater dans cette page, le fichier <IC>.cshtml</IC> contient du HTML, 
                    exactement comme un fichier <IC>.html</IC> normal, mais lorsque l'opérateur <IC>@</IC> est 
                    présent, il traitera le code de façon différente.
                </p>
            </section>

            <section>
                <h2>Commande de base</h2>
                <p>
                    Dans un projet ASP.NET, chaque fichier <IC>.cshtml</IC> doit commencer par 
                    l'instruction <IC>@page</IC>. Cette instruction indique simplement au moteur de ASP.NET que ce 
                    fichier est une page à interpréter.
                </p>
                <p>
                    Vous trouverez ensuite l'instruction <IC>@model NomDeClasse</IC>. Cette instruction indique le nom 
                    de la classe utilisé par la page sur le serveur. Assurez-vous que le nom de la classe est bien le 
                    même que celui utilisé dans le fichier <IC>.cs</IC> associé à votre fichier <IC>.cshtml</IC>.
                </p>
                <p>
                    Vous trouverez parfois ici un <IC>@using</IC>. Cette instruction permet d'importer une librairie 
                    C# dans votre page Razor. Cela peut être pratique si vous utiliser des classes ou des fonctions 
                    appartenant à d'autres namespace.
                </p>
                <p>
                    Vous trouverez ensuite un bloc <IC>{'@{ ... }'}</IC>. Ce bloc permet de déclarer des variables. 
                    En général, on modifiera les valeurs de la variable <IC>ViewData</IC> dans un bloc comme celui-ci 
                    en haut de votre page. Ceci vous permettra de changer certaines valeur, comme le titre de la page, 
                    dans le gabarit de base <IC>_Layout.cshtml</IC>.
                </p>
            </section>

            <section>
                <h2>Afficher des données sur le serveur</h2>
                <p>
                    Dans le côté serveur de cotre page Razor, il est fréquent de mettre des données que vous pourrez 
                    utiliser pour l'affichage de l'interface graphique. Ces données doivent être dans des variables 
                    publiques, et si possible, dans une property.
                </p>
                <CodeBlock language="csharp">{ serverVar }</CodeBlock>
                <p>
                    Vous pourrez accéder à ces valeurs dans votre page Razor dans le fichier <IC>.cshtml</IC> en 
                    utilisant la variable <IC>Model</IC>, bien entendu précédé de l'opérateur <IC>@</IC>.
                </p>
                <CodeBlock language="html">{ razorVar }</CodeBlock>
                <p>
                    Vous pouvez utiliser des opérations comme des conditions ou des boucles qui utilisent ces valeurs.
                    Les pages Razor nous permmettent en effet d'utiliser la plupart des structures de contrôle 
                    disponible dans C#.
                </p>
                <CodeBlock language="html">{ razorCondLoop }</CodeBlock>
            </section>
        </>;
    }
};
