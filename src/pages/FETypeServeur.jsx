import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const staticMiddleware = 
`import express from 'express';

/** Création du serveur */
const app = express();

// Ajout de middlewares
// ... Autres middlewares ici ...
app.use(express.static('public'));

// Ajout des routes
// ... Routes ici ...

// Démarrage du serveur
// ...`;

class FETypeServeur extends React.Component {
    render() {
        return <>
            <section>
                <h2>Statique vs Dynamique</h2>
                <p>
                    Les serveurs Web ont principalement 2 type de fonctionnement, soit statique et dynamique.
                </p>
                <p>
                    Un serveur Web statique sert simplement à retourner de fichiers. On utilise donc ce type de 
                    fonctionnement pour héberger les sites Web simple. En effet, un serveur Web statique pourra sans 
                    problème retourner des fichier HTML, CSS, Javascript, JSON, des images, des vidéos ou même du 
                    texte. La majorité des sites Web sont hébergés sur un serveur statique.
                </p>
                <p>
                    Un serveur Web dynamique, quant à lui, ne servira donc pas à retourner des fichiers, 
                    mais plutôt à exécuter du code lorsqu'il reçoit une requête. Ce code pourra modifier des variables, 
                    faire des recherches dans une base de données, écrire dans des fichiers et plus encore. Les 
                    applications Web plus complexe utiliseront ce type de fonctionnement. Les sites Web utilisant des connexions, 
                    nécessitant des bases de données ou permettant l'intéraction avec d'autres utilisateurs sont 
                    hébergés sur des serveurs dynamiques.
                </p>
                <p>
                    La plupart des serveurs aujourd'hui sont hybride. C'est-à-dire qu'ils permettent de retourner des 
                    fichiers de façon statique, mais permettent aussi de programmer certains comportement à l'aide 
                    d'un langage de programmation. Tous les serveurs Web que nous utiliserons durant la session 
                    fonctionneront de façon hybride.
                </p>
                <table>
                    <tr><th>Serveur</th><th>Langages de programmation</th></tr>
                    <tr><td>Express (Node.js)</td><td>Javascript, TypeScript, CoffeeScript</td></tr>
                    <tr><td>Apache</td><td>PHP, Python, Perl</td></tr>
                    <tr><td>Microsoft IIS (ASP.NET)</td><td>C#, F#, VB.NET</td></tr>
                </table>
            </section>

            <section>
                <h2>Statique et dynamique dans Express</h2>
                <p>
                    Jusqu'à maintenant, nous avons uniquement travaillé en mode dynamique avec Express puisque nous 
                    exécutions du code lors d'appel à certaines routes. Héberger des 
                    fichiers de façon statique est toutefois très simple à faire. Nous avons uniquement besoin d'un 
                    middleware pour nous aider. Ce middleware est déjà intégré dans Express. Nous n'avons donc pas 
                    besoin de faire une nouvelle installation.
                </p>
                <CodeBlock language="js">{ staticMiddleware }</CodeBlock>
                <p>
                    Lorsque nous utilisons le middleware <IC>static</IC>, nous spécifions quel dossier contiendra 
                    les fichiers à remettre de façon statique. Je vous suggère de mettre par convention dans un dossier 
                    nommé <IC>public</IC>. Assurez-vous d'appeler ce middleware après ceux que l'on utilise par défaut, 
                    mais avant les routes que vous aurez à spécifier, sinon vous pourriez avoir des conflits de route 
                    ou de fonctionnalités.
                </p>
                <p>
                    Pour tester le serveur statique, vous pouvez maintenant mettre un fichier <IC>index.html</IC> dans le 
                    dossier <IC>public</IC>. Si vous démarrer votre serveur, vous serez en mesure de voir la page html dans 
                    votre navigateur si vous accédez à l'address <IC>http://localhost:VOTRE_PORT_ICI</IC>.
                </p>
            </section>

            <section>
                <h2>Code client et code serveur dans Express</h2>
                <p>
                    Dans le code de notre projet, il peut être difficile de voir quel code s'exécute sur le client (dans 
                    le navigateur) et quel code s'éxécute sur le serveur (dans Node.js). En effet, puisque le code du 
                    client et du serveur se retrouve dans le même projet, cela complique un peu notre tâche.
                </p>
                <p>
                    Dans notre structure de projet, tout le code dans le dossier <IC>public</IC> est du code exécuté Dans
                    le navigateur web, donc sur le client. Ce code est envoyé par Express aux différents navigateurs qui 
                    demandent la page Web, mais il n'est jamais exécuté par Node.js. On va donc dire que c'est du code 
                    client.
                </p>
                <p>
                    Le code à l'extérieur du dossier <IC>public</IC> est exécuté par le serveur. C'est donc du code serveur.
                </p>
            </section>
        </>;
    }
}

export default FETypeServeur;
