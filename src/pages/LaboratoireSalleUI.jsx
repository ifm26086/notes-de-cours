import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distibue from '../resources/laboratoire-salle-v15-distribué.zip';
import solution from '../resources/laboratoire-salle-v15-solution.zip';

export default function LaboratoireSalleUI() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Voici ce que vous avez à faire pour compléter ce laboratoire:
            </p>
            <ol>
                <li>Télécharger le laboratoire distribué et le décompresser</li>
                <li>Ouvrir un terminal dans le projet et lancer la commande <IC>npm install</IC></li>
                <li>Dans le terminal, exécuter <IC>npm start</IC> pour démarrer le serveur</li>
                <li>
                    Dans le fichier <IC>/public/main.js</IC>, compléter les fonctions suivantes:
                    <ul>
                        <li><IC>getRoomOnServer</IC></li>
                        <li><IC>getUserInRoomOnServer</IC></li>
                        <li><IC>createRoomOnServer</IC></li>
                        <li><IC>createUserOnServer</IC></li>
                        <li><IC>deleteRoomOnServer</IC></li>
                        <li><IC>deleteUserOnServer</IC></li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Astuces</h2>
            <p>
                Il a a beaucoup d'information sur comment écrire les fonctions dans les commentaires au-dessus 
                de celle-ci. Je vous recommande fortement de les lire.
            </p>
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
