import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const installSQLite = 
`npm install sqlite3
npm install sqlite`;

const connexion =
`import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

// Base de données en mémoire
const connectionPromise = open({
    filename: ':memory:',
    driver: sqlite3.Database
});

export default connectionPromise;`;

const connexionFichier =
`import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

// Base de données dans un fichier
const connectionPromise = open({
    filename: process.env.DB_FILE,
    driver: sqlite3.Database
});

export default connectionPromise;`;

const env =
`# Serveur
PORT=5000
NODE_ENV=development

# Base de données
DB_FILE=nom_base_donnee.db`;

const structure =
`import { existsSync } from 'fs';
import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

// Constante indiquant si la base de données existe au démarrage du serveur 
// ou non.
const IS_NEW = !existsSync(process.env.DB_FILE)

// Crée la structure de la base de données et y ajoute des données
const createDatabase = async (connectionPromise) => {
    let connection = await connectionPromise;

    await connection.exec(
        \`CREATE TABLE IF NOT EXISTS products(
            product_id INTEGER PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            price REAL NOT NULL
        );\`
    );

    return connection;
});

// Base de données dans un fichier
let connectionPromise = open({
    filename: process.env.DB_FILE,
    driver: sqlite3.Database
});

// Si le fichier de base de données n'existe pas, on crée la base de données
// et on y insère des données fictive de test.
if (IS_NEW) {
    connectionPromise = createDatabase(connectionPromise);
}

export default connectionPromise;`;

const productsGet = 
`import connectionPromise from './connection.js';

export const getProducts = async () => {
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Envoyer une requête à la base de données
    let results = await connection.all(
        'SELECT * FROM products'
    );

    // Retourner les résultats
    return results;
}`;

const productsGetOne = 
`// ...

let results = await connection.get(
    'SELECT * FROM products WHERE product_id = 15'
);

// ...`;

const productsAdd =
`exports const addProduct = async (productCode, productName, 
    description, standardCost, listPrice) => 
{
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Envoyer une requête à la base de données
    // .run est utilisé parce que la base de données ne
    // retournera pas de résultats
    connection.run(
        \`INSERT INTO products(
            product_code, 
            product_name, 
            description, 
            standard_cost, 
            list_price)
         VALUES(?, ?, ?, ?, ?)\`,
        [productCode, productName, description, standardCost, listPrice]
    );
}`;

const ordersAdd =
`exports const addOrder = async (employeeId, customerId, productArray) => 
{
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Créer le 'orders'
    let result = await connection.run(
        \`INSERT INTO orders(employee_id, customer_id)
        VALUES(?, ?)\`,
        [employeeId, customerId]
    );

    // Aller chercher le ID généré du 'orders'
    const orderId = result.lastID;

    // Créer les 'order_details'
    for(let product in productArray)
    {
        connection.run(
            \`INSERT INTO order_details(order_id, product_id, quantity)
             VALUES(?, ?, ?)\`,
            [orderId, product.id, product.quantity]
        );
    }
}`;

export default function DBSQLite() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                SQLite est un petit gestionnaire de base de données qui s'intègre facilement dans un programme. Bien
                qu'il ne bénéficie pas des optimisations et de la puissance d'un serveur de base de données complet,
                il est très pratique puisqu'il ne prends pas beaucoup d'espace, peut être ajouté complètement à
                l'intérieur d'une application et offre des capacités de stockage et de recherche avancé. Il est donc
                très pratique pour les petites applications ou pour des preuves de concepts. Pour nous, il sera
                beaucoup plus facile d'héberger nos serveurs avec SQLite qu'avec MySQL.
            </p>
            <p>
                Pour intégré SQLite à l'intérieur de nos serveurs Node.js, nous aurons besoin de télécharger 2
                librairies de code supplémentaire:
            </p>
            <CodeBlock language="shell">{installSQLite}</CodeBlock>
            <p>
                La première librairie, <IC>sqlite3</IC>, nous permet de nous créer, ouvrir et lancer des requêtes SQL
                à une base de données SQLite. La deuxième librairie, <IC>sqlite</IC>, permet d'encapsuler tous les
                appels dans des promesses pour nous simplifier la tâche.
            </p>
        </section>

        <section>
            <h2>Connexion</h2>
            <p>
                Une fois la librairie téléchargé, vous créerez un fichier qui va
                vous permettre d'ouvrir la base de données. Je nomme généralement ce fichier <IC>connexion.js</IC>.
            </p>
            <CodeBlock language="js">{ connexion }</CodeBlock>
            <p>
                Lors de la connexion à la base de données, nous devons spécifier le fichier qui est utilisé par la
                base de données. Dans le cas ci-dessus, nous n'utilisons pas de fichier puisque la base de données est
                marqué comme <IC>:memory:</IC>. Une base de données en mémoire fonctionne bien pour faire des tests,
                mais perd complètement ses données une fois à la fermeture de l'application. Bref, dès que vous fermez
                ou redémarrez le serveur, les données n'existerons plus. Pour avoir la persitance des données, nous
                spécifierons un fichier de la façon suivante:
            </p>
            <CodeBlock language="js">{ connexionFichier }</CodeBlock>
            <p>
                Vous devez spécifier le nom du fichier de SQLite dans le
                fichier <IC>.env</IC>. Le nom du fichier peut être n'importe quoi, mais je vous recommande de le
                terminer par l'extension <IC>.db</IC>. Si le fichier existe déjà, SQLite va l'utiliser, mais s'il
                n'existe pas, il va en créer un nouveau. Bref, si vous voulez conserver vos tables et vos données,
                ne supprimez pas votre fichier <IC>.db</IC>.
            </p>
            <CodeBlock language="properties">{ env }</CodeBlock>
        </section>

        <section>
            <h2>Créer la strucure de la base de données</h2>
            <p>
                Pour la création des tables dans la base de données, puisque la base de données SQLite est intégré
                dans notre serveur, il est recommandé d'essayer de créer toutes la structure de données au démarrage
                du serveur. Si les tables existent déjà, rien ne sera fait, mais autrement, les tables seront
                recréées. C'est très pratique en développement puisque nous pouvons supprimer le fichier de base de
                données pour effacer les données et il sera recréé automatiquement avec toute la structure voulu.
            </p>
            <p>
                Pour réussir à atteindre ce but, nous devons modifier le fichier <IC>connexion.js</IC> pour y inclure
                la création des tables.
            </p>
            <CodeBlock language="js">{structure}</CodeBlock>
            <p>
                Si vous voulez créer plusieurs tables, vous pouvez ajouter leur code SQL directement dans la chaîne de
                caractère. Assurez-vous simplement de bien séparer les <IC>CREATE TABLE</IC> par des
                point-virgules <IC>;</IC>.
            </p>
            <p>
                Lors de la création de tables, vous noterez quelques différences avec MySQL. Entre autres pour les
                types de données. Aussi les clés primaires entière sont automatiquement autogénéré, donc il n'est pas
                nécessaire de le spécifier. Je vous recommande fortement d'aller voir les pages suivante pour avoir plus
                d'informations:
            </p>
            <p>
                <a href="https://www.sqlite.org/datatype3.html" target="_blank" rel="noopener noreferrer">Datatypes in SQLite</a>
            </p>
            <p>
                <a href="https://www.sqlite.org/lang_createtable.html" target="_blank" rel="noopener noreferrer">CREATE TABLE</a>
            </p>
        </section>

        <section>
            <h2>Envoyer une requête SQL</h2>
            <p>
                Pour faire des requêtes SQL, je vous recommande de faire un fichier Javascript pour chaque table 
                que vous utiliserez dans votre base de données ou pour chaque fonctionnalité de votre serveur. 
                Vous mettrez ces fichiers dans le dossier <IC>/model</IC> de votre projet.
                À titre d'exemple, j'utiliserai une table <IC>products</IC>. J'ai donc créé un
                fichier <IC>/model/products.js</IC> dans lequel j'écrirai mes requêtes SQL.
            </p>
            <CodeBlock language="js">{ productsGet }</CodeBlock>
            <p>
                Comme vous pouvez le constater, nous devrons utiliser notre fichier de connexion pour pouvoir 
                faire des requêtes à la base de données. Cette séparation du code dans plusieurs fichiers est 
                vraiment nécessaire si vous utilisez plusieurs tables dans votre base de données.
            </p>
            <p>
                Vous pouvez lancer le code SQL de 3 façons différente:
            </p>
            <ul>
                <li>
                    Avec la fonction <IC>.all()</IC> :
                    <div>
                        Si votre requête peut retourner plusieurs rangées de données.
                    </div>
                </li>
                <li>
                    Avec la fonction <IC>.get()</IC> :
                    <div>
                        Si votre requête retourne une seule rangée de données ou rien.
                    </div>
                </li>
                <li>
                    Avec la fonction <IC>.run()</IC> :
                    <div>
                        Si vous ne voulez pas avoir le résultat de la requête.
                    </div>
                </li>
            </ul>
            <CodeBlock language="js">{productsGetOne}</CodeBlock>
            <p>
                Finalement, portez une attention particulière aux promesses, aux await et aux async.
            </p>
        </section>

        <section>
            <h2>Prévenir les injections de SQL</h2>
            <p>
                Si vous voulez envoyer des données à SQLite, comme lorsque vous faite une recherche 
                avec condition paramétrée ou lorsque vous faite une insertion de données, vous devez faire 
                attention à la façon d'insérer les données dans vos requêtes.
            </p>
            <p>
                En effet, si vous ajoutez directement les données dans la chaîne de caractères SQL, vous vous 
                ouvrez à un type de piratage qui se nomme l'injection de SQL. Pour prévenir ce problème, nous pourrons
                ajouter un paramètre aux fonctions de SQLite avec nos données dedans.
            </p>
            <CodeBlock language="js">{ productsAdd }</CodeBlock>
            <p>
                Vous remarquerez que partout où nous voulons mettre des valeurs dans la requête SQL, nous mettrons 
                plutôt un point d'intérogation <IC>?</IC>. Nous fournirons par la suite un tableau contenant nos 
                données à la fonction <IC>run</IC>, <IC>get</IC> ou <IC>all</IC> pour qu'elle puisse remplacer
                les <IC>?</IC> par nos valeurs automatiquement et sécuritairement. L'ordre des valeurs dans le tableau
                doit être le même que celui de l'apparition des <IC>?</IC> dans la requête SQL.
            </p>
        </section>

        <section>
            <h2>Identifiant de la dernière insertion</h2>
            <p>
                Lorsque vos modèles de base de données contiennent des identifiants autogénérés, il est fréquent
                de vouloir insérer une rangée pour ensuite insérer l'identifiant (ID) de cette rangée dans une autre table. 
                Le problème: puisque l'identifiant est autogénéré, nous ne connaissons pas sa valeur. Par exemple, si vous 
                voulez
                créer une commande dans la base de données Northwind, vous devez insérer des rangées dans la
                table <IC>orders</IC> et <IC>order_details</IC>. Toutefois, pour insérer les <IC>order_details</IC>,
                vous devrez préalablement avoir créer le <IC>orders</IC> pour y mettre son identifiant. Il est donc
                nécessaire de connaître l'identifiant du <IC>orders</IC> que l'on vient de créer
            </p>
            <p>
                Pour aller chercher l'identifiant de la dernière insertion, nous utiliserons le résultat retourné par
                SQLite. Celui-ci contient entre autre l'identifiant de l'élément que nous venons d'insérer. 
                Ce résultat est disponible uniquement lorsque vous utilisez la fonction <IC>.run()</IC>. 
                Nous l'utiliserons avec Node.js de la façon suivante:
            </p>
            <CodeBlock language="js">{ ordersAdd }</CodeBlock>
        </section>
    </>;
};
