import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-18-solution.zip';

const todoJs = 
`const todo = (() => {
    /** <ul> contenant les éléments de ToDo */
    let todoList = document.getElementById('todo-list');

    /** <input type="submit"> de soumission de ToDo */
    let addSubmit = document.getElementById('add-submit');

    /** <input type="text"> contenant la soumission des ToDo */
    let addTextbox = document.getElementById('add-textbox');

    /** Gabarit HTML pour chaque ToDo */
    let todoTemplate = document.getElementById('todo-template');

    /**
     * Crée et ajoute un ToDo dans l'interface graphique.
     * @param {Object} todo Objet todo à ajouter dans l'interface graphique.
     */
    const addTodoInHTML = (index, text, cheked) => {
        // Copier le template et le remplir avec les données en paramètre
        let todoItem = todoTemplate.content.firstElementChild.cloneNode(true);
        let todoCheckbox = todoItem.querySelector('input[type=checkbox]');
        todoItem.querySelector('.text').innerText = text;
        todoCheckbox.checked = cheked;
        
        // Ajouter l'index du ToDo dans ses données HTML
        todoCheckbox.dataset.index = index;

        // Ajouter la copie du template dans l'interface graphique
        todoList.append(todoItem);
    }

    /**
     * Envoie une requête pour ajouter un ToDo sur le serveur.
     */
    const addTodoOnServer = async () => {
        // Envoyer la requête au serveur
        let response = await fetch('./todo', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ text: addTextbox.value })
        });

        if(response.ok){
            // On va chercher le ToDo ajouté sur le serveur
            let index = await response.json();

            // On ajoute le ToDo dans l'interface graphique
            addTodoInHTML(index, addTextbox.value, false);

            // On vide le textbox et on remet le focus dessus
            addTextbox.value = '';
            addTextbox.focus();
        }
    }

    /**
     * Envoie une requête pour modifier le checkbox d'un ToDo sur le serveur.
     */
    const checkTodoOnServer = async (event) => {
        if(event.target.tagName === 'INPUT' && 
           event.target.type === 'checkbox'){
            // Envoyer la requête au serveur
            let id = event.target.dataset.index
            fetch(\`./todo/\${ id }\`, { method: 'PATCH' });
        }
    }

    // Ajouter un listener sur le click du bouton de soumission
    addSubmit.addEventListener('click', addTodoOnServer);
    todoList.addEventListener('change', checkTodoOnServer);

    // Ajouter un listener sur la touche enter lorsqu'on est dans le input 
    // pour créer un ToDo
    addTextbox.addEventListener('keypress', (event) => {
        if(event.key === 'Enter'){
            addSubmit.click();
        }
    });
})();`;

export default class Laboratoire18 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Dans ce projet, nous programmerons la liste TODO que nous avons fait avec Node.js, mais à l'aide 
                    d'un controlleur en ASP.NET. Voici les points importants pour réussir ce laboratoire:
                </p>
                <ul>
                    <li>
                        Créer le projet ASP.NET.
                    </li>
                    <li>
                        Copier le HTML et le CSS de la liste TODO dans votre nouveau projet dans les bons fichiers.
                    </li>
                    <li>
                        Copier le fichier Javascript suivant pour votre page.
                        <CodeBlock language="js">{ todoJs }</CodeBlock>
                    </li>
                    <li>
                        Programmer une classe représentant un TODO et un contexte contenant la liste de TODO qui sera utilisé comme service.
                    </li>
                    <li>
                        Dans votre page, afficher par défaut les Todo qui sont dans la liste (dans le contexte)
                    </li>
                    <li>
                        Créer un controlleur utilisant le contexte défini prédcédemment supportant les routes suivante:
                        <ul>
                            <li>Get <IC>/todo</IC> qui retourne tous les Todos dans la liste (dans le contexte)</li>
                            <li>Post <IC>/todo</IC> qui ajoute un Todo dans la liste (dans le contexte)</li>
                            <li>Patch <IC>/todo/{'{ index }'}</IC> qui coche ou décoche un Todo dans la liste (dans le contexte)</li>
                        </ul>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
};
