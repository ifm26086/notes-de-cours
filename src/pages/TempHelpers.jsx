import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Video from '../component/Video';

const conditionJS =
`app.get('/compte', (request, response) => {
    response.render('compte', {
        user: {
            estConnecte: true,
            nom: 'Jonathan Wilkie',
            image: ''
        }
    });
});`;

const conditionHTML =
`{{#if user.estConnecte}}
    <h1>{{user.nom}}</h1>
    {{#if user.image}}
        <img 
        src="{{user.image}}" 
        alt="Image de l'utilisateur {{user.nom}}">
    {{/if}}
{{/if}}`;

const conditionElseHTML =
`{{#if user.estConnecte}}
    <h1>{{user.nom}}</h1>
    {{#if user.image}}
        <img 
        src="{{user.image}}" 
        alt="Image de l'utilisateur {{user.nom}}">
    {{else}}
        <div>Aucune image :(</div>
    {{/if}}
{{/if}}`;

const boucleJS =
`app.get('/fruits', (request, response) => {
    response.render('test', {
        fruits: [
            'pomme',
            'banane',
            'orange',
            'poire',
            'kiwi'
        ]
    });
});`;

const boucleHTML =
`<ul>
    {{#each fruits}}
        <li>
            {{this}} - {{@index}}
        </li>
    {{/each}}
</ul>`;

const todoJS =
`app.get('/', (request, response) => {
    response.render('home', {
        todos: [
            {
                texte: 'Écouter le cours',
                estFait: true
            },
            {
                texte: 'Lire les notes de cours',
                estFait: true
            },
            {
                texte: 'Travailler sur le projet',
                estFait: false
            }
        ]
    });
});`;

const todoHTML =
`<ul>
{{#each todos}}
    <li>
        <label>
            <input 
                type="checkbox" 
                {{#if this.estFait}}checked{{/if}}>
            <span>
                {{this.texte}}
            </span>
        </label>
    </li>
{{/each}}
</ul>`;

export default function TempHelpers() {
    return <>
        <section>
            <h2>Fonction d'aide</h2>
            <p>
                Lors de la génération du HTML, il est possible d'utiliser certaines fonction d'aide pour nous
                faciliter la tâche. Ces fonctions d'aide pourront utiliser les variables passées au
                fichier <IC>.handlebars</IC> performer différentes opérations logiques, comme des conditions ou des
                boucles.
            </p>
            <p>
                Pour utiliser ces fonctions dans le HTML, nous utiliserons la syntaxe <IC>{'{{#nomFonction}}'}</IC> pour
                ouvrir un bloc de fonction et la syntaxe <IC>{'{{/nomFonction}}'}</IC> pour fermer un bloc de fonction.
            </p>
            <p>
                Dans cette page, nous verrons seulement 2 fonctions d'aide, mais il en existe plusieurs autres. Il est
                même possible de créer nos propres fonctions d'aide. Vous pouvez consulter les pages suivantes pour
                avoir plus d'information sur ce sujet:
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/builtin-helpers.html" target="_blank" rel="noopener noreferrer">
                    Built-in Helpers
                </a>
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/#custom-helpers" target="_blank" rel="noopener noreferrer">
                    Custom Helpers
                </a>
            </p>
        </section>

        <section>
            <h2>Affichage conditionnel</h2>
            <p>
                La fonction d'aide d'affichage conditionnel permet d'afficher ou d'ajouter du HTML conditionnellement
                à l'aide d'une variable retournant une valeur booléene. Elle s'utilise avec les
                instructions <IC>{'{{#if}}'}</IC> et <IC>{'{{/if}}'}</IC>. Vous pouvez l'utiliser de la façon
                suivante:
            </p>
            <CodeBlock language="js">{conditionJS}</CodeBlock>
            <CodeBlock language="handlebars">{conditionHTML}</CodeBlock>
            <p>
                Dans l'exemple ci-dessus, l'affichage se fait puisque la variable <IC>estConnecte</IC> a la
                valeur <IC>true</IC>. Toutefois, l'image n'est pas affiché puisque sa valeur est une chaîne de
                caractères vide, ce qui équivaut à la valeur <IC>false</IC> en Javascript.
            </p>
            <p>
                Il est aussi possible d'utiliser un <IC>{'{{else}}'}</IC> pour afficher ou ajouter du HTML si la
                condition n'est pas respecté. Voici un exemple:
            </p>
            <CodeBlock language="handlebars">{conditionElseHTML}</CodeBlock>
            <p>
                Vous pouvez trouver plus de détails sur la fonction d'aide <IC>{'{{#if}}'}</IC> dans la documentation
                officielle de Handlebars.
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/builtin-helpers.html#if" target="_blank" rel="noopener noreferrer">
                    Built-in Helpers - if
                </a>
            </p>
        </section>

        <section>
            <h2>Répétition d'affichage</h2>
            <p>
                La répétition d'affichage ou d'ajout de HTML agit comme une boucle. Elle va répéter une section du
                HTML pour chaque élément dans un tableau ou dans un objet. Elle s'utilise avec les
                instructions <IC>{'{{#each}}'}</IC> et <IC>{'{{/each}}'}</IC>. Vous pouvez l'utiliser de la façon
                suivante:
            </p>
            <CodeBlock language="js">{boucleJS}</CodeBlock>
            <CodeBlock language="handlebars">{boucleHTML}</CodeBlock>
            <p>
                Comme vous pouvez le constater avec l'exemple ci-dessus, l'instruction <IC>{'{{#each}}'}</IC> fonctionne
                de façon très similaire à une boucle <IC>foreach</IC>. À l'intérieur de la boucle, vous pouvez
                utiliser l'instruction <IC>{'{{this}}'}</IC> pour représenter l'élément sur lequel vous êtes en train
                de boucler. Vous pouvez aussi utiliser l'instruction <IC>{'{{@index}}'}</IC> pour avoir l'index sur
                lequel vous êtes en train de boucler.
            </p>
            <p>
                Il est possible de boucler sur des tableaux contenant des données plus complexe. Vous pouvez, par
                exemple, boucler sur un tableaux d'objets de TODO et utiliser les variables dans les objets pour
                l'affichage dans la boucle:
            </p>
            <CodeBlock language="js">{todoJS}</CodeBlock>
            <CodeBlock language="handlebars">{todoHTML}</CodeBlock>
            <p>
                Vous pouvez trouver plus de détails sur la fonction d'aide <IC>{'{{#each}}'}</IC> dans la documentation
                officielle de Handlebars.
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/builtin-helpers.html#each" target="_blank" rel="noopener noreferrer">
                    Built-in Helpers - each
                </a>
            </p>
        </section>

        <section>
            <h2>Vidéos</h2>
            <Video title="Handlebars - Utilisation de Helpers" src="https://www.youtube.com/embed/L-8C6vg0M_0?si=FSGjXWvfvxoXRoW1" />
            <Video title="Handlebars - Styles et scripts client" src="https://www.youtube.com/embed/spMu2Gj4vdE?si=TwRsxawl_HNctWtv" />
        </section>
    </>;
}
