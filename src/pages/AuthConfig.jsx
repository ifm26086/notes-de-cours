import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const importation =
`import bcrypt from 'bcrypt';
import passport from "passport";
import { Strategy } from "passport-local";
import { getUtilisateur } from "./model/utilisateur.js";`;

const strategy =
`// Configuration générale de la stratégie.
// On indique ici qu'on s'attends à ce que le client
// envoit un variable "courriel" et "motDePasse" au
// serveur pour l'authentification.
const config = {
    usernameField: 'courriel',
    passwordField: 'motDePasse'
};

// Configuration de quoi faire avec l'identifiant
// et le mot de passe pour les valider
passport.use(new Strategy(config, async (courriel, motDePasse, done) => {
    // S'il y a une erreur avec la base de données,
    // on retourne l'erreur au serveur
    try {
        // On va chercher l'utilisateur dans la base
        // de données avec son identifiant, le
        // courriel ici
        const utilisateur = await getUtilisateur(courriel);

        // Si on ne trouve pas l'utilisateur, on
        // retourne que l'authentification a échoué
        // avec un message
        if (!utilisateur) {
            return done(null, false, { error: 'mauvais_utilisateur' });
        }

        // Si on a trouvé l'utilisateur, on compare
        // son mot de passe dans la base de données
        // avec celui envoyé au serveur. On utilise
        // une fonction de bcrypt pour le faire
        const valide = await bcrypt.compare(motDePasse, utilisateur.mot_de_passe);

        // Si les mot de passe ne concorde pas, on
        // retourne que l'authentification a échoué
        // avec un message
        if (!valide) {
            return done(null, false, { error: 'mauvais_mot_de_passe' });
        }

        // Si les mot de passe concorde, on retourne
        // l'information de l'utilisateur au serveur
        return done(null, utilisateur);
    }
    catch (error) {
        return done(error);
    }
}));`;

const serialize =
`passport.serializeUser((utilisateur, done) => {
    // On mets uniquement le courriel dans la session
    done(null, utilisateur.courriel);
});`;

const deserialize =
`passport.deserializeUser(async (email, done) => {
    // S'il y a une erreur de base de donnée, on
    // retourne l'erreur au serveur
    try {
        // Puisqu'on a juste l'adresse courriel dans
        // la session, on doit être capable d'aller
        // chercher l'utilisateur avec celle-ci dans
        // la base de données.
        const user = await getUtilisateur(email);
        done(null, user);
    }
    catch (error) {
        done(error);
    }
})`;

export default function AuthConfig() {
    return <>
        <section>
            <h2>Fichier pour Passport.js</h2>
            <p>
                Maintenant que la partie de la base de données est faite, nous devons configurer la
                librairie <IC>passport</IC> pour qu'elle puisse exécuter l'authentification sans problème. Puisque
                nous avons plusieurs fonction à paramétrer, je vous suggère de créer un fichier disctinct pour
                contenir ces configurations. J'ai nommé ce fichier <IC>authentification.js</IC>
            </p>
            <p>
                Dans le fichier de configuration de l'authentification, nous aurons besoin de quelques librairies et
                fonctions. Voici donc les importations nécessaires:
            </p>
            <CodeBlock language="js">{importation}</CodeBlock>
            <p>
                Vous noterez que nous utilisons la fonction <IC>getUtilisateur</IC> programmé lors de la création de
                la base de données. Si vous utilisez un nom de fonction ou de fichier différent, assurez-vous de le
                changer dans l'importation.
            </p>
            <p>
                N'oubliez pas d'inclure ce fichier dans le fichier <IC>server.js</IC>. Puisque ce fichier ne retourne
                aucune variable puisqu'il n'y aura pas de <IC>export</IC>, vous pouvez simplement ajouter cette
                importation à la fin de celle disponible dans le fichier <IC>server.js</IC>
            </p>
            <CodeBlock language="js">{`import './authentification.js';`}</CodeBlock>
        </section>

        <section>
            <h2>Ajouter la stratégie d'authentification</h2>
            <p>
                La première fonction de paramétrage à écrire est celle indiquant quelle stratégie d'authentification
                notre système utilise et son paramétrage. Dans notre cas, la stratégie est simplement l'utilisation
                d'un identifiant et mot de passe dont nous validerons l'authenticité directement sur notre serveur.
                Dans le fichier <IC>authentification.js</IC>, nous feront l'intégration de cette fonction de
                paramétrage ci-dessous. Les commentaires indiquent bien ce que le code fait.
            </p>
            <CodeBlock language="js">{strategy}</CodeBlock>
        </section>

        <section>
            <h2>Sérialisation des données</h2>
            <p>
                La sérialisation est le processus de traduction des données vers un format qui peut être stocké
                facilement. La librairie <IC>passport</IC> utilise cette terminologie pour indiquer comment elle doit
                stocker les données de l'utilisaeur dans sa session et comment retraduire ces données formattées en un
                objet Javascript. Nous devons donc programmer ces 2 fonctions dans le fichier <IC>authentification.js</IC>.
            </p>
            
                    <h3>Sérialisation</h3>
                    <p>
                        Processus de sauvegarde dans la session. La fonction de sérialisation s'exécute immédiatement
                        après la fonction de stratégie. Elle va donc recevoir l'objet d'utilisateur en paramètre et
                        nous devons lui indiquer comment stocker l'utilisateur dans la session.
                    </p>
                    <p>
                        Les données d'un compte utilisateur peuvent être volumineuses. Pour éviter de remplir la base
                        de données de session inutilement, nous sauvegarderons uniquement l'identifiant de
                        l'utilisateur, dans notre cas, l'adresse courriel. C'est une bonne pratique puisque la base de
                        données de session, souvent en mémoire, dispose de beaucoup moins d'espace qu'une base de
                        données physique, sauvegardé sur un disque dur.
                    </p>
            <CodeBlock language="js">{serialize}</CodeBlock>
            
                    <h3>Désérialisation</h3>
                    <p>
                        Processus d'aller reconstruire l'utilisateur à partir de ce qu'il y a dans la session. Dans
                        notre cas, la session contient uniquement l'adresse courriel de l'utilisateur. Nous devons
                        donc trouver une façon de reconstruire l'utilisateur à partir de son adresse courriel. pour ce
                        faire, nous irons simplement chercher l'utilisateur complet dans la base de données à partir
                        de son adresse courriel.
                    </p>
                    <CodeBlock language="js">{deserialize}</CodeBlock>
        </section>
    </>
}