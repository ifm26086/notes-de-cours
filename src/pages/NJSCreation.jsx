import React from 'react';
import CodeBlock from '../component/CodeBlock'
import IC from '../component/InlineCode'

const npmInit =
`$ npm init

...

package name: (nom-du-projet)
version: (1.0.0)
description: Description de votre projet ici.
entry point: (index.js) server.js
test command:
git repository:
keywords:
author: Votre nom ici
license: (ISC)
About to write to D:\\Programmation Internet 2\\nom-du-projet\\package.json:

{
  "name": "nom-du-projet",
  "version": "1.0.0",
  "description": "Description de votre projet ici.",
  "main": "server.js",
  "scripts": {
    "test": "echo "Error: no test specified" && exit 1"
  },
  "author": "Votre nom ici",
  "license": "ISC"
}

Is this OK? (yes)`;

const npmStart =
`$ cd /dossier/du/projet
$ npm start`;

const typeModule = 
`"type": "module",`

export default function NJSCreation() {
    return <>
        <section>
            <h2>Créer un projet</h2>
            <p>
                Voici les étapes pour créer un projet avec Node.js et NPM:
            </p>
            <ol>
                <li>Créer un dossier vide ayant pour nom le nom de votre projet</li>
                <li>
                    Dans le dossier créé, exécuter la commande suivante dans un terminal
                    <CodeBlock language="shell">$ npm init</CodeBlock>
                </li>
                <li>
                    Répondez aux questions posé dans la console.
                    <ol>
                        <li>Le nom du package doit être le nom de votre projet</li>
                        <li>Ajouter une description</li>
                        <li>Mettre <IC>server.js</IC> comme point d'entrée au programme</li>
                        <li>Ajouter votre nom comme auteur</li>
                        <li>Les autres options peuvent rester tel quel</li>
                    </ol>

                    <CodeBlock language="shell">{npmInit}</CodeBlock>
                </li>
                <li>
                    Modifier le fichier <IC>package.json</IC> et y rajouter la ligne suivante en dessous de la ligne
                    de description:
                    <CodeBlock language="json">{typeModule}</CodeBlock>
                </li>
                <li>Ajouter un fichier <IC>server.js</IC> dans votre dossier de projet</li>
            </ol>
            <p>
                Après avoir fait toutes ces étapes, vous devriez vous retrouver avec un dossier de projet 
                contenant les fichiers <IC>package.json</IC> et <IC>server.js</IC>. 
            </p>
        </section>

        <section>
            <h2>Écrire un programme</h2>
            <p>
                Le fichier <IC>server.js</IC> est le point d'entrée à notre programme. Nous allons donc écrire 
                notre code Javascript dans ce fichier. Pour tester que tout fonctionne bien, nous pouvons écrire 
                un programme simple comme celui-ci:
            </p>
            <CodeBlock language="js">console.log('J\'aime le Javascript')</CodeBlock>
            <p>
                Éventuellement, nous allons séparer notre code dans plusieurs fichiers, mais nous nous 
                contenterons de ce merveilleux programme pour l'instant.
            </p>
        </section>

        <section>
            <h2>Exécuter un programme</h2>
            <p>
                Pour exécuter du code avec Node.js, vous avez 2 options:
            </p>
            <ol>
                <li>
                    Exécuter un fichier Javascript directement
                    <CodeBlock language="shell">
                        $ node nom-du-fichier.js
                    </CodeBlock>
                </li>
                <li>
                    Exécuter un projet
                    <CodeBlock language="shell">{ npmStart }</CodeBlock>
                </li>
            </ol>
            <p>
                Lorsque vous exécuter un projet, c'est toujours le fichier <IC>server.js</IC> qui est exécuté. 
                Assurez-vous que ce fichier est bien présent dans votre projet.
            </p>
        </section>
    </>;
}
