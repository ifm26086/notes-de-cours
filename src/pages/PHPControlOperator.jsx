import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const functionExample = 
`<?php 
    function nomDeFonction($param1, $param2) {
        return $param1 + $param2;
    }

    echo nomDeFonction(20, 22);
?>`;

const echoDump =
`<?php 
    echo "Bonjour";
    var_dump(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
?>`;

export default class PHPControlOperator extends React.Component {
    render() {
        return <>
            <section>
                <h2>Opérateurs</h2>
                <p>
                    PHP met à notre disposition une gamme complète d'opérateurs pour manipuler nos variables. La 
                    majorité de ces opérateurs sont très similaire à ceux du langage Java ou Javascript. Vous pouvez 
                    avoir la référence complète des opérateurs du langage PHP ici:
                </p>
                <p>
                    <a href="https://www.w3schools.com/php/php_operators.asp" target="_blank" rel="noopener noreferrer">
                        PHP Operators
                    </a>
                </p>
                <ColoredBox heading="Attention">
                    La différence majeure entre les opérateurs de Java/Javascript et PHP est pour les chaînes de 
                    caractères. Pour une raison quelconque, PHP a décidé d'utiliser l'opérateur <IC>.</IC> pour faire 
                    la concaténation au lieu de l'opérateur <IC>+</IC>. C,est une source d'erreur fréquente pour les 
                    nouveaux  programmeurs de PHP.
                </ColoredBox>
            </section>

            <section>
                <h2>Instructions de contrôle</h2>
                <p>
                    PHP utilise les mêmes instructions de contrôle que les langages populaires comme Java ou C#. Vous 
                    avez donc les structures de contrôles suivantes: 
                </p>
                <ul>
                    <li>if / else</li>
                    <li>switch</li>
                    <li>while</li>
                    <li>do while</li>
                    <li>for</li>
                    <li>foreach</li>
                </ul>
                <p>
                    Vous avez aussi aux instructions de contrôles <IC>continue</IC> et <IC>break</IC> dans les boucles 
                    pour respectivement passer à la prochaine itération ou sortir de la boucle.
                </p>
                <p>
                    Pour avoir plus de détails et des exemples pour chacune des structures de contrôle, vous pouvez 
                    visiter le lien suivant:
                </p>
                <p>
                    <a href="https://www.php.net/manual/en/language.control-structures.php" target="_blank" rel="noopener noreferrer">
                        Control Structures
                    </a>
                </p>
            </section>

            <section>
                <h2>Fonctions</h2>
                <p>
                    Pour créer une fonction en PHP, nous utiliserons le mot-clé <IC>function</IC>. Les fonctions en 
                    PHP ressemblent beaucoup aux fonctions des autres langages de programmation populaire. Les 
                    paramètres se retrouvent entre les paranthèses et vous pouvez retourner des valeurs avec le 
                    mot-clé <IC>return</IC>.
                </p>
                <CodeBlock language="php">{ functionExample }</CodeBlock>
                <p>
                    Les fonctions en PHP s'appellent comme dans les autres langages de programmation, soit en 
                    utilisant les paranthèses <IC>()</IC> après le nom de la fonction. Dans un fichier, vous pouvez 
                    appeler une fonction avant ou après sa déclaration. Cela n'a pas d'importance.
                </p>
            </section>

            <section>
                <h2>Afficher des valeurs</h2>
                <p>
                    Vous avez peut-être remarqué que dans les exemples jusqu'à présent, nous utisons 2 fonctions pour 
                    afficher des données dans notre navigateur.
                </p>
                <ul>
                    <li>
                        <IC>echo</IC>: Pour afficher la valeur d'une variable qui peut se transformer en String.
                    </li>
                    <li>
                        <IC>var_dump()</IC>: Pour afficher le contenu d'une variable, peu importe son type.
                    </li>
                </ul>
                <p>
                    On utilisera en général la fonction <IC>echo</IC>, mais dans le cas où nous voulons déboguer notre 
                    code, je vous recommande d'utiliser <IC>var_dump()</IC>. Ceci est principalement dû au fait que 
                    certains types de variables ne se convertissent pas bien en String en PHP. C'est le cas notament 
                    pour les types suivants:
                </p>
                <ul>
                    <li>Boolean</li>
                    <li>Array</li>
                    <li>Object</li>
                    <li>Null</li>
                </ul>
                <CodeBlock language="php">{ echoDump }</CodeBlock>
            </section>
        </>;
    }
};
