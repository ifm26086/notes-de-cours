import React from 'react';
import CodeBlock from '../component/CodeBlock';
import IC from '../component/InlineCode'

import imgAsynchrone from '../resources/asynchrone.png'

const func =
`function additionner(nombre1, nombre2){
    return nombre1 + nombre2;
};`

const varFunc =
`const additionner = function(nombre1, nombre2){
    return nombre1 + nombre2;
};`

const arrowFunc =
`const additionner = (nombre1, nombre2) => nombre1 + nombre2;`

const callbackBrowser = 
`let button = document.getElementById('clicker');
clicker.addEventListener('click', (event) => {
    console.log('Cette fonction est un callback');
});`;

const callbackNode = 
`import { readFile } from 'fs';
readFile('./chemin/vers/fichier.txt', (error, data) => {
    console.log(data.toString())
});`;

const callbackIndent = 
`import fs from 'fs';
fs.readFile('./fichier1.txt', (error, data1) => {
    // On a besoin du fichier1 avant de lire le fichier2
    fs.readFile('./' + data1.toString(), (error, data2) => {
        // On a besoin du fichier2 avant de lire le fichier3
        fs.readFile('./' + data2.toString(), (error, data3) => {
            // On affiche le contenu du fichier3
            console.log(data3.toString())

            // Autre code dépendant du fichier3 ici...
        });
    });
});`;

const promiseSimple = 
`import { readFile } from 'fs/promises';

// La lecture du fichier se fait de façon asynchrone
// et retourne une promesse.
let promesse = readFile('./fichier1.txt');

// On passe une fonction à la promesse. Cette fonction
// s'exécutera uniquement lorsque la lecture du fichier 
// est terminée. Le paramètre "resultat" contient le
// contenu du fichier lu.
promesse.then((resultat) => {
    console.log(resultat.toString());
});`;

const promiseChain =
`let promesse = longueFonction();
promesse.then((data) => {
    // Traitement des données de la longue fonction
})
.then((data2) => {
    // Autre traitement à faire après le 1er 
    // traitement
})
.then((data3) => {
    // Traitement à faire après les 2 premiers 
    // traitements
});`;

const asyncIntro = 
`let salutation = async () => {
    return "Allo";
}

let salutation2 = () => {
    return Promise.resolve("Allo");
}`;

const awaitIntro = 
`import { readFile } from 'fs/promises'

(async () => {
    let data = await readFile('./fichier.txt');
    console.log(data.toString())
})();`;

const asyncMain = 
`// Création d'une fonction main que l'on exécute après
let main = async () => {

    // Votre code ici ...

};
main();

// Création d'une fonction auto exécutante
(async () => {

    // Votre code ici ...
    
})();`;

export default function NJSAsynchrone() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                La programmation asynchrone est un style de programmation où le programme n'attends pas 
                nécessairement qu'une fonction soit terminé pour continuer d'exécuter le reste de son code. Le 
                programme exécutera le code associé à la fonction uniquement lorsque celle-ci aura fini de 
                s'exécuter. 
            </p>
            <p>
                Ce principe est plus facile à comprendre avec un schémas:
            </p>
            <img src={ imgAsynchrone } alt="Schémas synchrone asynchrone" />
            <p>
                Cette façon de programmer à plusieurs avantages, dont celui de ne pratiquement pas gaspiller de 
                ressources processeurs puisque certains morceaux de codes peuvent s'exécuter pendant que l'on 
                attends la réponse d'autres morceaux de codes. De cette façon, le processeur fait toujours quelque 
                chose et reste en attente rarement. Cette façon de faire est donc très bonne pour les serveurs qui 
                doivent parfois traiter plusieurs milliers de requêtes simultanément.
            </p>
            <p>
                La programmation asynchrone a toutefois ses inconvénients:
            </p>
            <ul>
                <li>
                    Elle est plus compliquée que la programmation séquentielle à laquelle nous sommes 
                    généralement habituée
                </li>
                <li>
                    Elle nécessite un peu plus de traitement pour ses fonctions, ce qui en fait généralement 
                    un mauvais choix pour les programmes qui nécessite une performance de traitement à court 
                    terme
                </li>
                <li>
                    Elle n'exécute pas toujours les fonctions au moment précis de leur appel et la durée avant 
                    leur retour est difficile à prévoir, ce qui est problématique pour les programmes nécessitant 
                    une précision en durée d'exécution.
                </li>
            </ul>
            <p>
                Javascript est déjà bâtit pour bien fonctionner avec la programmation asynchrone. Les évènements
                que nous enregistrons dans le DOM (comme le clic sur un bouton) en sont un bon exemple. En effet,
                le code du clic sur un bouton est exécuté uniquement lorsque le navigateur en a le temps, ce qui 
                en fait du code asynchrone. Node.js en fait de même avec sa boucle d'évènements asynchrone.
            </p>
        </section>

        <section>
            <h2>Fonctions</h2>
            <p>
                Javascript est un langage de programmation ayant une histoire un peu bizarre. Il a entre autre été 
                standardisé très tard dans son histoire pour un langage autant utilisé. Il a aussi été revue et mis 
                à jour à plusieurs reprises. Cela en fait un langage à la syntaxe particulière.
            </p>
            <p>
                Bref, il y a plusieurs façon de créer une fonction en Javascript et 
                il est important de les connaître avant de se lancer dans la programmation asynchrone.
            </p>
            <p>
                La fonction classique:
            </p>
            <CodeBlock language="js">{ func }</CodeBlock>
            <p>
                La fonction comme variable:
            </p>
            <CodeBlock language="js">{ varFunc }</CodeBlock>
            <p>
                La fonction lambda (flèche):
            </p>
            <CodeBlock language="js">{ arrowFunc }</CodeBlock>
            <p>
                La fonction de type lambda (flèche) est aujourd'hui beaucoup utilisé. Une de ses particularité est 
                que si elle contient une seule instruction, elle retourne automatiquement ce résultat et n'a pas 
                besoin d'accolades. Elle possède aussi quelques autres différences, mais il n'est pas nécessaire 
                de les connaître pour le moment.
            </p>
        </section>

        <section>
            <h2>Callback</h2>
            <p>
                Le callback est la façon originale de faire de la programmation asynchrone en Javascript. Cette 
                façon de faire est de moins en moins utilisé, principalement parce que la manipulation de promesse 
                (voir ci-dessous) est aujourd'hui généralement plus facile.
            </p>
            <p>
                Le callback est à première vu simple à utiliser. On passe simplement une fonction en paramètre à 
                une autre fonction. Dans le Javascript sur navigateur, ajouter un évènement au DOM se fait par 
                callback:
            </p>
            <CodeBlock language="js">{ callbackBrowser }</CodeBlock>
            <p>
                Dans Node.js, un bon exemple est la lecture d'un fichier sur votre ordinateur. Puisqu'on ne sait 
                pas combien de temps la lecture du fichier va durer (petit fichier = rapide, gros fichier = lent), 
                on lui passe un callback pour lui dire quoi faire lorsque la lecture sera terminée:
            </p>
            <CodeBlock language="js">{ callbackNode }</CodeBlock>
            <p>
                À première vu, le tout semble assez simple. Nous pouvons toutefois voir certains problèmes si nous 
                devons chaîner ce genre d'appel l'un dans l'autre. Ceci causera une indentation importante du code vers 
                la droite et deviendra beaucoup plus difficile à lire et comprendre. C'est ce qu'on appelle 
                le <strong>Callback Hell</strong>.
            </p>
            <CodeBlock language="js">{ callbackIndent }</CodeBlock>
        </section>

        <section>
            <h2>Promesse</h2>
            <p>
                Les promesses sont une première façon se simplifier la lecture et l'utilisation du code asynchrone. Une 
                promesse est en fait un objet qui est retourné par une fonction qui s'exécute de façon asynchrone. 
                Nous pourrons par la suite donner une fonction à cet objet (un peu comme un callback) pour que celle-ci 
                s'exécute lorsque la fonction asynchrone sera terminée. Pour ce faire, nous utiliserons la 
                fonction <IC>then</IC> de la promesse. Voici un exemple:
            </p>
            <CodeBlock language="js">{ promiseSimple }</CodeBlock>
            <p>
                Si vous trouvez qu'à première vu, cela a l'air plus compliqué que les callback, je vous comprends.
                Sachez simplement que si vous appelez une fonction qui renvoie une promesse, vous avez du code 
                asynchrone et vous devrez utiliser la fonction <IC>then</IC> pour lui dire quoi faire avec le 
                résultat par la suite.
            </p>
            <p>
                L'utilisation de promesse permet tout de même de simplifier le chaînage puisque la 
                fonction <IC>then</IC> retournent elle aussi une promesse. Voici un exemple:
            </p>
            <CodeBlock language="js">{ promiseChain }</CodeBlock>
        </section>
        
        <section>
            <h2>Async / Await</h2>
            <p>
                Bien que les promesses simplifie beaucoup le code asynchrone, il est toujours plus difficile à 
                lire que du code séquentiel. Avec les nouvelles versions de Javascript, une nouvelle syntaxe pour 
                manipuler les promesses est apparut. Les mots-clés <IC>async</IC> et <IC>await</IC> ont été ajouté
                pour que la programmation avec les promesses se fasse presque de la même façon que la 
                programmation séquentielle.
            </p>
            <p>
                Le mot-clé <IC>async</IC> doit être placé devant une fonction. Ci ce mot-clé est présent, cela 
                veut dire que la fonction retourne une promesse. Si la fonction ne retourne pas de 
                promesse, <IC>async</IC> va transformer le retour de la fonction pour qu'elle retourne 
                automatiquement une promesse. Par exemple, les 2 fonctions suivantes retournent la même chose:
            </p>
            <CodeBlock language="js">{ asyncIntro }</CodeBlock>
            <p>
                Le mot-clé <IC>await</IC> doit être placé devant une promesse. Ce mot-clé peut être utilisé tout seul 
                sans problème, mais s'il est placé dans une fonction, celle-ci doit absolutment être 
                marquée <IC>async</IC>. Il indique à Javascript qu'il doit attendre que la promesse soit 
                résolue avant de continuer l'exécution du code. De plus, au lieu de retourner la 
                promesse, <IC>await</IC> s'arrangera pour retourner directement le résultat de la promesse. Voici 
                un exemple:
            </p>
            <CodeBlock language="js">{ awaitIntro }</CodeBlock>
            <p>
                Puisque le <IC>await</IC> doit absolument être placé dans un bloc <IC>async</IC>, je vous 
                recommande fortement de vous faire une fonction "main" dans votre fichier principal. Vous pouvez 
                utiliser l'une des approches ci-dessous:
            </p>
            <CodeBlock language="js">{ asyncMain }</CodeBlock>
            <p>
                L'utilisation de <IC>async</IC> et <IC>await</IC> nous permet de vraiment simplifier notre code 
                asynchrone. Le code devient très concis et facile à lire. Je vous suggère donc fortement son 
                utilisation.
            </p>
        </section>

        <section>
            <h2>Plus d'informations</h2>
            <p>
                Pour plus d'information à propos du code asynchrone en Javascript, je vous suggère d'aller voir 
                les ressources suivantes:
            </p>
            <p>
                <a href="https://javascript.info/async" target="_blank" rel="noopener noreferrer">Promises, async/await</a>
            </p>
            <p>
                <a href="https://www.codecademy.com/learn/introduction-to-javascript/modules/asynch-js/cheatsheet" target="_blank" rel="noopener noreferrer">Async-Await Cheatsheets</a>
            </p>
        </section>
    </>;
}
