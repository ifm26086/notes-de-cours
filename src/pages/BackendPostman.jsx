import React from 'react';
import IC from '../component/InlineCode';

import collection1 from '../resources/postman-collections1.png'
import collection2 from '../resources/postman-collections2.png'
import collection3 from '../resources/postman-collections3.png'
import requete1 from '../resources/postman-requete1.png'
import requete2 from '../resources/postman-requete2.png'
import requete3 from '../resources/postman-requete3.png'
import reponse from '../resources/postman-reponse.png'

export default function BackendPostman() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Pour tester notre serveur Web, nous utiliserons le logiciel Postman. Postman est un logiciel qui 
                permet de créer des requêtes HTTP et de les envoyer. Ce logiciel nous permettra donc de tester nos 
                serveurs sans avoir à créer notre propre interface graphique pour envoyer des requêtes. Dans ce 
                cours, nous utiliserons uniquement la base de ce logiciel, mais ce sera suffisant pour nos 
                besoins.
            </p>
            <p>
                Vous pouvez télécharger Postman au lien suivant:
            </p>
            <p>
                <a href="https://www.postman.com/downloads/" target="_blank" rel="noopener noreferrer">Postman</a>
            </p>
        </section>

        <section>
            <h2>Créer une collection</h2>
            <p>
                Postman sauvegarde toutes les requêtes que nous allons créer dans une collection. Une collection 
                est simplement un ensemble de requêtes. Je vous conseille fortement de vous créer une nouvelle 
                collection pour chacun des projets serveurs que vous faites. Cela vous permettra de garder vos 
                requêtes assez organisées.
            </p>
            <p>
                Pour créer une collection, suivez les étapes suivantes: 
            </p>
            <ol>
                <li>
                    Cliquer sur le bouton suivant dans la section à gauche
                    <img src={ collection1 } alt="Ajouter une collection" />
                </li>
                <li>
                    Ajouter un nom à la collection et cliquer sur «&nbsp;Create&nbsp;»
                    <img src={ collection2 } alt="Spécifier le nom d'une collection" />
                </li>
                <li>
                    Une fois la collection crée, vous pouvez les voir dans la section à gauche
                    <img src={ collection3 } alt="Liste des collections" />
                </li>
            </ol>
        </section>

        <section>
            <h2>Créer une requête</h2>
            <p>
                Une fois votre collection créé, vous pouvez y ajouter des requêtes que vous pourrez par la suite 
                configurer. Postman vous permet de définir la méthode de la requête, sa route (adresse) et même 
                d'y ajouter des données.
            </p>
            <p>
                Pour créer une requête, vous pouvez suivre les étapes suivantes:
            </p>
            <ol>
                <li>
                    Cliquer sur le bouton suivant pour créer une requête
                    <img src={ requete1 } alt="Créer une requête" />
                </li>
                <li>
                    Dans l'interface qui apparaît, vous pourrez changer la méthode HTTP, changer la route 
                    (adresse) de la requête, envoyer la requête et même sauvegarder votre requête dans la 
                    collection pour pouvoir encore l'envoyer plus tard.
                    <img src={ requete2 } alt="Configurer une requête" />
                </li>
                <li>
                    Si vous désirez envoyer des données avec la requête, ajoutez-les en allant dans l'onglet 
                    «&nbsp;Body&nbsp;». Vous pouvez par la suite choisir le type de données à envoyer et le 
                    contenu. Dans le cours, nous enverrons majoritairement des données en <IC>raw/JSON</IC>.
                    <img src={ requete3 } alt="Ajouter des données à une requête" />
                </li>
            </ol>
        </section>

        <section>
            <h2>Inspection des réponses</h2>
            <p>
                Lorsque vous envoyez une requête, vous pourrez voir le contenu de la réponse dans la section en 
                bas. Dans cette section, vous trouverez la valeur retourné par le serveur et son type. Vous 
                pourrez aussi trouver le code de status, la temps nécessaire avant la réception ainsi que la 
                taille de la réponse.
            </p>
            <img src={ reponse } alt="Inspection d'un réponse" />
        </section>
    </>;
}
