import React from 'react';

export default function Outils() {
    return <>
        <section>
            <h2>Liste des logiciels</h2>
            <p>
                Pour ce cours, nous aurons besoin de nombreux logiciels. 
                Voici la liste des outils dont nous aurons besoin durant 
                la session:
            </p>
            <ul>
                <li>Google Chrome</li>
                <li>Visual Studio Code</li>
                <li>Node.js</li>
                <li>Postman</li>
                <li>SQLiteStudio</li>
                <li>Git</li>
            </ul>
        </section>

        <section>
            <h2>Google Chrome</h2>
            <p>
                Bien que ce navigateur amasse probablement une partie de 
                votre information personnelle pour la vendre, il offre les
                meilleurs outils de développement. Je vous recommande donc
                fortement ce navigateur pour ce cours. Vous pouvez 
                télécharger ce navigateur ici:
            </p>
            <p>
                <a href="https://www.google.com/chrome/" target="_blank" rel="noopener noreferrer">Google Chrome</a>
            </p>
            <p>
                Vous pouvez utiliser un autre navigateur, comme Firefox ou Edge, mais je ne pourrai pas nécessairement 
                vous aider si vous ne trouvez pas certaines options.
            </p>
        </section>

        <section>
            <h2>Visual Studio Code</h2>
            <p>
                Cet éditeur de code est présentement l'un des plus utilisé sur le marché du travail. Je vous le 
                recommande fortement. De plus, de nombreuses extensions nous permettrons de simplifier notre travail. 
                Vous pouvez le télécharger ici:
            </p>
            <p>
                <a href="https://code.visualstudio.com/" target="_blank" rel="noopener noreferrer">Visual Studio Code</a>
            </p>
            <p>
                Vous pouvez utiliser un autre éditeur de code, mais vous
                n'aurez probablement pas accès aux mêmes extensions 
                qu'avec Visual Studio Code. En voici tout de même 
                quelques uns:
            </p>
            <ul>
                <li>
                    <a href="https://notepad-plus-plus.org/" target="_blank" rel="noopener noreferrer">Notepad++</a>
                </li>
                <li>
                    <a href="https://www.sublimetext.com/" target="_blank" rel="noopener noreferrer">Sublime Text</a>
                </li>
                <li>
                    <a href="https://atom.io/" target="_blank" rel="noopener noreferrer">Atom</a>
                </li>
            </ul>
        </section>

        <section>
            <h2>Node.js</h2>
            <p>
                Node.js est un engin de Javascript asynchrone qui nous 
                permet d'exécuter du code Javascript dans une console sur
                votre ordinateur au lieu d'à l'intérieur d'un navigateur
                Web. Nous l'utiliserons pour programmer des serveurs et 
                des API REST.
            </p>
            <p>
                <a href="https://nodejs.org/" target="_blank" rel="noopener noreferrer">Node.js</a>
            </p>
        </section>

        <section>
            <h2>Postman</h2>
            <p>
                Postman est un outil pour tester les API REST et les 
                appels client-serveur. Nous l'utiliserons pour tester nos 
                serveurs Node.js.
            </p>
            <p>
                Le logiciel vous demandera probablement de vous faire un
                compte, mais vous n'êtes pas obligé de le faire pour 
                utiliser le logiciel.
            </p>
            <p>
                <a href="https://www.postman.com/downloads/" target="_blank" rel="noopener noreferrer">Postman</a>
            </p>
        </section>

        <section>
            <h2>Git</h2>
            <p>
                Git est un gestionnaire de version et un outil favorisant le travail d'équipe. Il est énormément 
                utilisé sur le marché du travail et dans la communauté des logiciels à code source ouvert 
                (open source). Nous l'utiliserons pour les projets d'équipes ainsi que pour le déploiement de nos 
                applications web.
            </p>
            <p>
                <a href="https://git-scm.com/downloads" target="_blank" rel="noopener noreferrer">Git</a>
            </p>
            <p>
                Par défaut, Git s'installe uniquement en ligne de commande. Si vous n'êtes pas à l'aise avec Git en 
                ligne de commande, je vous suggère d'utiliser une interface graphique. Visual Studio Code propose déjà 
                des options graphiques dans son interface. Vous pouvez aussi télécharger d'autres interfaces graphiques
                ici:
            </p>
            <p>
                <a href="https://git-scm.com/downloads/guis" target="_blank" rel="noopener noreferrer">Git GUI CLients</a>
            </p>
        </section>

        <section>
            <h2>SQLiteStudio</h2>
            <p>
                Dans ce cours, nous utiliserons une petite base de données nommée SQLite qui pourra être intégré dans vos 
                applications et serveurs. Pour facilité notre utilisation de cette base de données, SQLiteStudio est une 
                bonne solution. C'est une interface graphique qui nous permettra de visualiser le contenu d'une base de 
                données et d'y exécuter des requêtes.
            </p>
            <p>
                <a href="https://sqlitestudio.pl/" target="_blank" rel="noopener noreferrer">SQLiteStudio</a>
            </p>
        </section>
    </>;
};
