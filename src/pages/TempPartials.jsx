import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Video from '../component/Video';

const partial =
`<!-- Fichier /views/partials/partials.handlebars -->
<section>
    <h2>Partials</titre>
    <p>
        Lorem ipsum dolor sit, amet consectetur
        adipisicing elit. Vero ex nesciunt reiciendis
        dicta odio tenetur. Eligendi, voluptatibus!
    </p>
</section>`;

const partialUse =
`<!-- Fichier /views/home.handlebars -->
<h1>Layouts et partials</h1>
{{>partials}}`;

export default function TempPartials() {
    return <>
        <section>
            <h2>Layouts</h2>
            <p>
                Par défaut, avec Handlebars, nous utilisons un layout appelé <IC>main.handlebars</IC>. C'est
                en effet un fichier contenant tous les éléments répétés d'une page à l'autre. Essentiellement, si vous
                avez des éléments qui apparaîssent sur chaque page, comme l'entête, le pied de page ou un menu sur le
                côté, c'est dans le fichier de layout que vous devez le mettre.
            </p>
            <p>
                Par défaut, Handlebars suppose que nous utilisons un fichier de layout et celui-ci doit absolument
                s'appeler <IC>main.handlebars</IC>. Si le nom n'est pas bon, vous aurez des erreurs.
            </p>
            <p>
                Dans ce fichier, vous devrez aussi ajouter une instruction <IC>{'{{{body}}}'}</IC> à quelques part.
                Cette instruction indique l'emplacement ou le contenu spécifique de chaque page ira. Essentiellement,
                lors du chargement d'une page, par défaut, Handlebars va utiliser le fichier <IC>main.handlebars</IC> et
                remplacer l'instruction <IC>{'{{{body}}}'}</IC> par le contenu spécifique de la page spécifié dans un
                autre fichier <IC>.handlebars</IC>.
            </p>
            <p>
                Bref, tout fonctionne automatiquement, vous n'avez donc qu'à mettre votre contenu répété dans toutes
                les pages directement dans le fichier <IC>main.handlebars</IC> et le contenu spécifique à chaque page
                dans leur propre fichier <IC>.handlebars</IC>.
            </p>
        </section>

        <section>
            <h2>Partials</h2>
            <p>
                Les partials sont une façon de réutiliser du code HTML à plusieurs endroits dans vos
                fichiers <IC>.handlebars</IC> ou encore de séparer votre code HTML dans plusieurs fichiers. Les
                partials sont essentiellement des fichiers <IC>.handlebars</IC> qui contiennent du HTML que vous
                pouvez insérer dans d'autres fichiers <IC>.handlebars</IC>.
            </p>
            <p>
                Pour créer un partials, vous pouvez simplement créer un fichier <IC>.handlebars</IC> dans le dossier
                <IC>/views/partials</IC>. Ensuite, vous mettez le HTML que vous voulez dedans.
            </p>
            <CodeBlock language="handlebars">{partial}</CodeBlock>
            <p>
                Pour utiliser un partial, vous n'avez qu'à le spécifier dans une commande <IC>{'{{>nom-partial}}'}</IC> dans
                un autre fichier <IC>.handlebars</IC>. Il est juste important de mettre le nom du fichier à la place
                sans l'extension dans l'instruction. Pour utiliser le partial ci-dessus, vous pouvez donc utiliser la
                commande suivante:
            </p>
            <CodeBlock language="handlebars">{partialUse}</CodeBlock>
            <p>
                Il y a de nombreuses autres possibilités avec les partials. Il est entre autre possible de les
                paramétriser ou même de leur donner du contenu. Si vous voulez en apprendre davantage sur les
                partials, je vous recommande fortement d'aller voir la page suivante:
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/partials.html" target="_blank" rel="noopener noreferrer">
                    Partials
                </a>
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Handlebars - Utilisation de Partials" src="https://www.youtube.com/embed/14D1M6rgy5U?si=Lqq_CxNZDoH2HfH3" />
        </section>
    </>;
}
