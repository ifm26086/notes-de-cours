import React from 'react';

import authentification from '../resources/authentification.png';

export default function AuthIntro() {
    return <>
        <section>
            <h2>Définition</h2>
            <p>
                L'authentification est un mécanisme par lequel un utilisteur est capable de prouver son identité. Dans
                un site web, on l'utilise généralement avec une adresse courriel ou un nom d'utilisateur et un mot de
                passe. Toutefois, il existe de nombreuses autres façon de prouver son identité. Par exemple, nos
                téléphones intelligents aujourd'hui peuvent prouver notre identité à l'aide d'empreintes digitale ou
                par la morphologie de notre visage. Il existe aussi des méthodes de partage de secret et pleins
                d'autres techniques ingénieuses pour prouver qu'un utilisteur est bien celui qu'il prétend être. Dans
                notre cas, nous irons pour l'utilisation du mot de passe. C'est une technique simple à implémenter et
                assez efficace.
            </p>
            <p>
                Dans une architecture client-serveur, ce n'est pas le client qui veut savoir si l'utilisateur est le
                bon, mais le serveur. Comme ça, le serveur pourra contrôler ce que les utilisateur peuvent ou ne
                peuvent pas faire. bref, à chaque requête, le serveur veut savoir qui est l'utilisateur qui la fait
                pour contrôler les accès. Il ne serait toutefois vraiment pas pratique que l'utilisateur ait besoin
                d'envoyer son nom d'utilisateur et mot de passe à chaque requête qu'il fait au serveur.
            </p>
        </section>

        <section>
            <h2>Fonctionnement</h2>
            <p>
                Pour ne pas avoir à demander à chaque fois son nom d'utilisateur et mot de passe, un serveur utilisera
                les sessions. De cette façon, quand l'utilisateur se connectera au site web avec son mot de passe, le
                serveur ira mettre les informations de l'utilisateur dans sa session. Ainsi, à chaque requête faite
                par le client, le serveur pourra voir dans sa session s'il est connecté ou non. Il pourra même voir
                si l'utilisateur a des accès spéciaux ou tout autre droit supplémentaire.
            </p>
            <img src={authentification} alt="Schémas explicatif de l'authentification sur un serveur" />
        </section>

        <section>
            <h2>Sécurité</h2>
            <p>
                Un des problèmes important de l'authentification sur le web est la protection des données
                confidentielles, comme le mot de passe, lors de l'authentification avec le serveur. La protection du
                mot de passe se fera de 2 façons différentes:
            </p>
            <ol>
                <li>
                    En utilisant le protocole HTTPS
                    <p>
                        L'utilisation du protocole HTTPS permettra d'encrypter le mot de passe avant de l'envoyer au
                        serveur. Nous ferons cette étape dans un autre module dans le cours.
                    </p>
                </li>
                <li>
                    En encodant le mot de passe dans la base de données
                    <p>
                        Si la base de données se fait pirater, les mots de passe seront encodé, donc il n'est pas
                        possible de retrouver leur valeur originale. Nous ferons cette étape un peu plus loin dans ce
                        module.
                    </p>
                </li>
            </ol>
        </section>
    </>
}