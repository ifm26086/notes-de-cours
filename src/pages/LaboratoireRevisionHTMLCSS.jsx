import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import distribue from '../resources/laboratoire-revision-html-css-distribué.zip'
import solution from '../resources/laboratoire-revision-html-css-solution.zip'

export default function LaboratoireRevisionHTMLCSS() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <ol>
                <li>
                    Télécharger le projet de départ du laboratoire dans les fichiers de téléchargement ci-dessous.
                </li>
                <li>
                    Ouvrir le projet dans Visual Studio Code et dans votre navigateur Web. Les énoncés des exercices seront 
                    affichés dans votre navigateur.
                </li>
                <li>
                    Lorsque vous complétez correctement un exercice, un crochet vert apparaîtra à la droite de l'énoncé du
                    problème. 
                    <ul>
                        <li>
                            Si le crochet vert n'apparaît pas, essayez de rafraîchir la page manuellement.
                        </li>
                        <li>
                            Si le crochet vert n'apparaît toujours pas, mais que votre code semble fonctionner, allez voir
                            la solution du laboratoire. Votre solution est probablement bonne, mais différente de la 
                            mienne.
                        </li>
                    </ul>
                </li>
                <li>
                    Compléter les exercices de HTML dans le 
                    fichier <IC>/pages/exercices-html.html</IC> aux 
                    endroits indiqués par des commentaires.
                </li>
                <li>
                    Compléter les exercices de CSS dans le 
                    fichier <IC>/css/exercices-css.css</IC> aux 
                    endroits indiqués par des commentaires.
                </li>
            </ol>
        </section>

        <section>
            <h2>Solution et fichiers de départ</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
