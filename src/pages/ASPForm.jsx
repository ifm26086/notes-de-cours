import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const aspForm = 
`<form method="post">
    <!-- Page complète ici -->

    <input type="submit" asp-page-handler="Action">
</form>`;

const aspHandler =
`public IActionResult OnPostAction(){
    // Code serveur ici ...

    // Redirection à la page trouvé par le GET
    // Permet d'empêcher le renvois du formulaire en POST
    return RedirectToPage("./Index");
}`;

const aspMultipleSubmit =
`<form method="post">
    <!-- Page complète ici -->
    <input type="submit" asp-page-handler="Ajouter">
    <input type="submit" asp-page-handler="Modifier">
    <input type="submit" asp-page-handler="Supprimer">
</form>`;

const aspMultipleHandler = 
`public IActionResult OnPostAjouter(){
    // ...
    return RedirectToPage("./Index");
}

public IActionResult OnPostModifier(){
    // ...
    return RedirectToPage("./Index");
}

public IActionResult OnPostSupprimer(){
    // ...
    return RedirectToPage("./Index");
}`;

const aspSubmitJs =
`let form = document.getElementById('form');
form.action = "/?handler=Ajouter";
form.submit();`;

const aspBindHTML =
`<form method="post">
    <input type="text" asp-for="Utilisateur">
    <input type="password" asp-for="MotDePasse">
    <input type="submit" asp-page-handler="Connexion">
</form>`;

const aspBindVar = 
`[BindProperty]
public string Utilisateur { get; set; }

[BindProperty]
public string MotDePasse { get; set; }`;

const aspUseBind = 
`public IActionResult OnPostConnexion(){
    Console.Write(Utilisateur);
    Console.Write(":");
    Console.Write(MotDePasse);
    Console.Write("\\n");
    return RedirectToPage("./Login");
}`;

export default class ASPForm extends React.Component {
    render() {
        return <>
            <section>
                <h2>Appel d'un fonction serveur</h2>
                <p>
                    Par défaut, en ASP.NET, si vous faire des requêtes sur le serveur, il faut soumettre un 
                    formulaire avec méthode HTTP POST. Pour ce faire, nous encapsulerons très souvent l'ensemble du 
                    HTML d'une page directement dans un formulaire de la façon suivante:
                </p>
                <CodeBlock language="html">{ aspForm }</CodeBlock>
                <p>
                    Comme vous pouvez le constater ci-dessus, pour soumettre le formulaire, nous utiliserons 
                    un bouton de soumission. Vous remarquerez l'attribut <IC>asp-page-handler</IC> qui permet de 
                    définir un nom de fonction à appeler dans le code serveur de la page. Dans le cas ci-dessus, la
                    page serveur devrait contenir une fonction comme il suit:
                </p>
                <CodeBlock language="csharp">{ aspHandler }</CodeBlock>
                <p>
                    Il est possible d'avoir plusieurs boutons de soumission dans le même formulaire. Vous devez 
                    simplement vous assurer que chaque bouton ait un <IC>asp-page-handler</IC> unique dans le 
                    formulaire.
                </p>
                <CodeBlock language="html">{ aspMultipleSubmit }</CodeBlock>
                <CodeBlock language="csharp">{ aspMultipleHandler }</CodeBlock>
                <p>
                    Si vous n'aimez pas les boutons de formulaire et désirez plutôt envoyer le formulaire 
                    directement en Javascript, ceci est possible avec un code similaire à l'exemple ci-dessous. Dans 
                    ce genre de cas, vous devez spécifier le <IC>asp-page-handler</IC> directement dans l'URL du 
                    formulaire. C'est pourquoi nous modifions la valeur <IC>action</IC> du formulaire.
                </p>
                <CodeBlock language="js">{ aspSubmitJs }</CodeBlock>
            </section>

            <section>
                <h2>Envoyer des données au serveur</h2>
                <p>
                    La façon de procéder ci-dessus envois des messages au serveur, mais ces messages ne contiennent
                    pas de données. Pour ajouter des données, nous devrons brancher des champs de formulaire avec 
                    des variables sur le serveur. La première chose à faire sera tout simplement d'ajouter un attribut 
                    <IC>asp-for</IC> sur chaque champ du formulaire que nous voulons envoyer au serveur:
                </p>
                <CodeBlock language="html">{ aspBindHTML }</CodeBlock>
                <p>
                    Le nom que vous mettrez dans cet attribut est le nom de la propriété (variable) dans laquelle nous
                    recevrons les résultats. Ainsi, pour l'exemple ci-dessus, vous devez créer les 2 propriétés
                    ci-dessous sur le serveur. N'oubliez pas non plus d'ajouter l'étiquette <IC>[BindProperty]</IC> au
                    dessus de votre propriété. Si vous ne l'ajoutez pas, la valeur ne sera pas transféré.
                </p>
                <CodeBlock language="csharp">{ aspBindVar }</CodeBlock>
                <p>
                    Vous pouvez par la suite utiliser ces variables dans le code traitant vos requêtes sur le serveur.
                </p>
                <CodeBlock language="csharp">{ aspUseBind }</CodeBlock>
            </section>
        </>;
    }
};
