import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'
import DownloadBlock from '../component/DownloadBlock'

import middleware from '../resources/middleware-sse.txt'

const saveConnexion = 
`// Ensemble de connexion
let connexions = new Set();

app.get('/nom-de-la-route', (request, response) => {
    // Retourner le stream au client
    // ...

    // Ajouter la connexion à notre ensemble
    connexions.add(response);

    // Boucle pour garder la connexion en vie
    // ...
    
    // Arrêter la boucle si le client stop la connexion
    response.on('close', () => {
        clearInterval(intervalId);
        response.end();

        // On retire aussi la connexion de notre ensemble
        connexions.delete(response);
    });
});`;

const broadcast = 
`// Bâtir la chaîne de données
let data = { time: Math.floor(Date.now() / 1000) };
let dataString = 
    'event: epoch\\n' + 
    \`data: \${ JSON.stringify(data) }\\n\\n\`;

// Broadcast des évènements à toutes les connexions
for(let connexion of connexions){
    connexion.write(dataString);
    connexion.flush();
}`;

const middlewareUsage =
`app.get('/connexion', (request, response) => {
    response.initStream();
});

app.get('/broadcast', (request, response) => {
    let data = { text: 'Allo!' };
    let event = 'salutation';
    response.pushJson(data, event);
});`;

class SSEBroadcast extends React.Component {
    render() {
        return <>
            <section>
                <h2>Partage d'information en temps réel</h2>
                <p>
                    Les server sent events sont généralement utilisé pour partager de l'information en temps réel 
                    parmi plusieurs clients connectés sur votre serveur. Pour ce faire, votre serveur devra garder en 
                    mémoire tous les objets de réponse de tous les clients présentement connecté au serveur. Pour ce 
                    faire, nous utiliserons généralement un <IC>Set</IC> ou une <IC>Map</IC>.
                </p>
                <CodeBlock language="js">{ saveConnexion }</CodeBlock>
                <p>
                    Un <IC>Set</IC> et une <IC>Map</IC> sont des structures de données, similaire à des tableaux, mais 
                    qui permettent généralement des recherches plus rapide si on ne connais pas les index. 
                    L'apprentissage des structures de données sort grandement du cadre de ce cours, mais voici tout de 
                    même un peu d'information sur ces 2 structures:
                </p>
                <dl>
                    <dt>Set</dt>
                    <dd>
                        Ensemble d'éléments qui ne permet pas d'avoir 2 fois le même élément. On peut ajouter des 
                        éléments, supprimer des élément et trouver si un élément est à l'intérieur de notre 
                        ensemble. Les sets ne permettent généralement pas de rechercher par index comme un 
                        tableau.
                    </dd>

                    <dt>Map</dt>
                    <dd>
                        Un ensemble de clé-valeur. C'est un principe de dictionnaire: on recherche un mot (clé) et
                        on trouve une définition (valeur). Pratiquement, en informatique, la clé et la valeur 
                        peuvent être de n'importe quel type.
                    </dd>
                </dl>
                <p>
                    Nous utiliserons généralement le <IC>Set</IC> lorsque nous n'avons pas besoin de sauvegarder 
                    d'information sur la connexion et la <IC>Map</IC> lorsque nous voulons sauvegarder de 
                    l'information supplémentaire sur chaque connexion.
                </p>
            </section>

            <section>
                <h2>Broadcast des évènements</h2>
                <p>
                    Lorsqu'il sera temps d'envoyer un évènement à tous les clients, nous pouvons itérer au travers de 
                    l'ensemble pour envoyer les données. Vous pouvez le faire de la façon suivante:
                </p>
                <CodeBlock language="js">{ broadcast }</CodeBlock>
            </section>

            <section>
                <h2>Création de middleware</h2>
                <p>
                    Il peut être très pratique de séparer le code gérant nos connexions et les server sent event dans 
                    son propre middleware. Vous pouvez utiliser le middleware suivant pour vous aider à débuter:
                </p>
                <DownloadBlock>
                    <DownloadBlock.File path={ middleware } name="middleware-sse.js"></DownloadBlock.File>
                </DownloadBlock>
                <p>
                    Après avoir ajouté ce middleware à votre application Express, vous disposerez de 2 nouvelles 
                    fonctionnalités dans l'objet de réponse, soit <IC>initStream</IC> et <IC>pushJson</IC>. Vous 
                    pouvez les utiliser de la façon suivante:
                </p>
                <CodeBlock language="js">{ middlewareUsage }</CodeBlock>
                <p>
                    Vous remarquerez l'utilisation d'un <IC>id</IC> dans les évènements envoyés. Ce n'est pas 
                    obligatoire, mais cela permettra à un client qui est désynchronisé de se resynchroniser en 
                    exécutant les évènements manquant dans l'ordre.
                </p>
                <p>
                    Je vous suggère d'être assez à l'aise avec les différents composants de ce middleware puisque vous 
                    aurez probablement à le modifier selon vos besoins.
                </p>
            </section>
        </>;
    }
}

export default SSEBroadcast;
