import React from 'react';
import CodeBlock from '../component/CodeBlock';
import IC from '../component/InlineCode';

const npmInstall = 
`# Option longue
$ npm install nom-du-package --save

# Option courte
$ npm i nom-du-package`;

const packageDependencies = 
`{
    "name": "nom-du-projet",
    "version": "1.0.0",
    "main": "server.js",
    "type": "module",
    "dependencies": {
        "compression": "^1.7.4",
        "express": "^4.17.1",
        "helmet": "^4.1.1"
    }
}`;

const npmInstallDev = 
`# Option longue
$ npm install nom-du-package --save-dev

# Option courte
$ npm i nom-du-package -D`;

export default function BackendNPM() {
    return <>
        <section>
            <h2>npm install</h2>
            <p>
                Le gestionnaire de package de Node.js (NPM) nous permet d'installer des librairies de code externe 
                à Node.js que nous pourrons par la suite utiliser dans notre code. Pour ce faire, nous utiliserons 
                la commande suivante:
            </p>
            <CodeBlock language="shell">{ npmInstall }</CodeBlock>
            <p>
                Une fois la librairie externe installée, vous devriez voir quelques changements dans votre projet:
            </p>
            <ul>
                <li>
                    Un dossier <IC>node_modules</IC> va être créé dans votre projet. Il contient les fichiers de 
                    la librairie installé ainsi que les fichiers des autres librairies dont dépendent la librairie 
                    que vous venez d'installer. Vous n'avez pas besoin de toucher à ce dossier.
                </li>
                <li>
                    Un fichier <IC>package-lock.json</IC> va être généré dans votre projet. Il contient l'arbre 
                    des dépendances de votre projet. Vous n'avez pas besoin de toucher à ce fichier.
                </li>
                <li>
                    La dernière version de la librairie installé est ajouté automatiquement dans votre 
                    fichier <IC>package.json</IC> dans la section <IC>dependencies</IC>. Comme pour les autres 
                    changements, je vous suggère de ne pas y toucher.
                    <CodeBlock language="json">{ packageDependencies }</CodeBlock>
                </li>
            </ul>
            <p>
                Quand la librairie est installé, vous pourrez simplement l'utiliser en l'important dans votre code 
                à l'aide du mot-clé <IC>import</IC>:
            </p>
            <CodeBlock language="js">{ `import { express } from 'express';` }</CodeBlock>
        </section>

        <section>
            <h2>Librairie de développement</h2>
            <p>
                Il est possible d'installer des librairies utilitaires qui ne servent que durant le développement.
                La librairie <IC>nodemon</IC> en est un bon exemple. C'est une librairie qui relance notre serveur
                automatiquement lorsque l'on fait des changements dans notre code. Cette librairie est très utile
                pour le développement, mais complètement inutile lorsque notre application sera en production.
            </p>
            <p>
                Pour installer une librairie de développement, vous utiliserez la commande suivante dans la 
                console:
            </p>
            <CodeBlock language="shell">{ npmInstallDev }</CodeBlock>
        </section>

        <section>
            <h2>Soumettre ou partager le projet</h2>
            <p>
                Lorsqu'on installe des librairies externes, celles-ci sont installé dans le dossier <IC>node_modules</IC>. 
                En général, ces librairies utilisent aussi d'autres librairies. Ces librairies aussi sont installé dans le 
                dossier <IC>node_modules</IC>. Vous remarquez donc qu'avec seulement quelques installation, le 
                dossier <IC>node_modules</IC> peut finir par contenir plusieurs milliers de fichiers de code et devenir 
                beaucoup gros en poids (octets).
            </p>
            <p>
                Lorsqu'il est temps de partager le projet avec un coéquipier ou de soumettre un projet au professeur, vous 
                devez donc supprimer le dossier <IC>node_modules</IC> pour faciliter le transfert. Une fois le transfert ou 
                la soumission fini le professeur ou le coéquipier pourra rebâtir le dossier <IC>node_modules</IC> en 
                lançant la commande suivante:
            </p>
            <CodeBlock language="shell">$ npm install</CodeBlock>
            <p>
                Cette commande va simplement voir quelles librairies sont nécessaire pour votre projet dans le 
                fichier <IC>package.json</IC> et les télécharger automatiquement dans le dossier <IC>node_modules</IC>.
            </p>
        </section>
    </>;
}
