import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const expressRoute = 
`app.post('/user/order', (request, response) => {
    // ...
});`;

const apacheConfig = 
`DirectorySlash Off

RewriteEngine On
RewriteCond %{REQUEST_FILENAME} -d
RewriteCond %{REQUEST_URI} !/$
RewriteCond %{REQUEST_FILENAME}/index.php -f
RewriteRule (.*) $1/index.php [L]`;

const phpRoute = 
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // Code de la route ici
}`;

const phpRouteMultiple =
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // Code du POST de la route ici
}
else if($_SERVER['REQUEST_METHOD'] === 'DELETE'){
    // Code du DELETE de la route ici
}`;

const phpRoute404 = 
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // Code du POST de la route ici
}
else if($_SERVER['REQUEST_METHOD'] === 'DELETE'){
    // Code du DELETE de la route ici
}
else{
    // Méthode HTTP non supporté
    http_response_code(404);
    echo 'Not found';
}`;

const phpBody = 
`$body = json_decode(file_get_contents('php://input'), true);

// Définition des routes ici
// ...`;

const phpBodyProperty = 
`$body['nomPropriete']`;

const phpEcho = 
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // ...
    echo 'Success!';
}`;

const phpEchoJson = 
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

    header('Content-Type: application/json');
    echo json_encode($data);
}`;

const phpEchoError = 
`if($_SERVER['REQUEST_METHOD'] === 'POST'){
    http_response_code(400);
    echo 'Bad request';
}`;

const fetchApache =
`let data = { /* ... */ };
let response = await fetch('./user/order', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
});`;

export default class PHPRoute extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Jusqu'à présent, nous avons utilisé le langage PHP pour retourner des pages HTML à partir de notre 
                    serveur. Donc quand notre navigateur va chercher une page PHP sur notre serveur, celui-ci retourne
                    une page HTML qu'il construit. Nous pouvons toutefois retourner d'autres type des données avec 
                    PHP.
                </p>
                <p>
                    Un cas très fréquent aujourd'hui, est d'aller chercher des données sur un serveur avec des 
                    requêtes <IC>fetch()</IC> sur le client. Ces requêtes vont généralement chercher des données en 
                    JSON en contactant certaines routes sur un serveur. PHP nous permettra aussi de répondre à ces 
                    requêtes en retournant du JSON plutôt que du HTML.
                </p>
            </section>

            <section>
                <h2>Création d'une route</h2>
                <p>
                    Avec Node.js et Express, nous utilisons des routes virtuelles. C'est-à-dire que chaque route créée 
                    dans Express existe purement en mémoire. En effet, dans le code suivant, la 
                    route <IC>/user/order</IC> n'existe pas physiquement. Il n'y a pas de 
                    fichiers <IC>/user/order</IC>, mais le serveur comprends que c'est une route virtuelle et qu'il 
                    doit exécuter du code si on essait d'y accéder.
                </p>
                <CodeBlock language="js">{ expressRoute }</CodeBlock>
                <p>
                    En PHP, c'est un peu plus compliqué de créer des routes virtuelles. Nous utiliserons donc des 
                    routes physique. Ainsi, si vous voulez avoir la route ci-dessus, vous devrez vous créer un 
                    dossier <IC>user</IC> contenant un dossier <IC>order</IC> contenant un fichier <IC>index.php</IC> qui 
                    contiendra le code à exécuter si on accède à cette route.
                </p>
                <ColoredBox heading="À noter">
                    <p>
                        Si vous voulez vraiment créer des routes virtuelles en PHP, je vous recommande fortement d'aller 
                        lire l'article suivant. Il propose une solution intéressante et assez facile pour la gestion des 
                        routes.
                    </p>
                    <a href="https://steampixel.de/en/simple-and-elegant-url-routing-with-php/" target="_blank" rel="noopener noreferrer">
                        Simple and elegant URL routing with PHP
                    </a>
                </ColoredBox>
            </section>

            <section>
                <h2>Configuration de Apache</h2>
                <p>
                    Un des petits problèmes avec les routes en PHP, c'est que Apache, par défaut, nous force à 
                    utiliser des URL sous certains formats. Entres autres, si vous voulez accéder à la 
                    route <IC>/user/order</IC>, Apache forcera une redirection vers la 
                    route <IC>/user/order/</IC>, puis vers <IC>/user/order/index.php</IC>. Ce petit jeu de redirection 
                    va briser nos nos accès à la route à partir du client.
                </p>
                <p>
                    Heureusement, Apache est facilement configurable. Pour corriger ce problème, vous n'avez qu'à 
                    créer un fichier nommé <IC>.htaccess</IC> à la racine de votre projet et à y mettre le contenu 
                    suivant:
                </p>
                <CodeBlock language="apache">{ apacheConfig }</CodeBlock>
            </section>

            <section>
                <h2>Programmation de la route</h2>
                <p>
                    Il nous reste maintenant à programmer notre route dans le fichier <IC>index.php</IC> que nous 
                    venons de créer. Vous pouvez utiliser le code suivant dans le fichier PHP pour créer une route 
                    selon une certaine méthode HTTP:
                </p>
                <CodeBlock language="php">{ phpRoute }</CodeBlock>
                <p>
                    Si vous voulez utiliser plusieurs méthodes dans la même route, vous n'avez qu'à ajouter d'autres 
                    conditions.
                </p>
                <CodeBlock language="php">{ phpRouteMultiple }</CodeBlock>
                <p>
                    Je vous recommande aussi fortement d'ajouter une erreur si la méthode HTTP n'est pas supporté. 
                    Dans cas, simplement mettre une erreur 404 est suffisante.
                </p>
                <CodeBlock language="php">{ phpRoute404 }</CodeBlock>
                <p>
                    Comme vous pouvez le constater dans le code ci-dessus, la 
                    variable <IC>$_SERVER['REQUEST_METHOD']</IC> retourne la méthode HTTP utilisé pour accéder au 
                    fichier PHP. Nous pouvons donc simplement regarder son contenu pour rediriger notre code à la 
                    bonne place dépendant de sa valeur.
                </p>
                <p>
                    L'utilisation de la fonction <IC>http_response_code()</IC> et <IC>echo</IC> seront décrit plus 
                    loin dans cette page.
                </p>
            </section>

            <section>
                <h2>Accès au corps de la requête</h2>
                <p>
                    Si des données ont été envoyé avec la requête, il est important de pouvoir y accéder dans le code 
                    de notre route. Pour ce faire, vous pouvez utiliser le code suivant:
                </p>
                <CodeBlock language="php">{ phpBody }</CodeBlock>
                <p>
                    Après l'exécution de cette ligne, les données envoyées seront mises dans une variable 
                    nommé <IC>$body</IC>. 
                </p>
                <p>
                    Vous remarquerez peut-être aussi l'utilisation de la fonction <IC>json_decode</IC>. Cette fonction 
                    nous permet de convertir le JSON que nous avons reçu avec la requête dans un format un peu plus 
                    lisible pour le langage PHP. Ainsi, pour accéder aux différentes propriétés dans l'objet JSON 
                    reçu, nous utiliserons le code suivant:
                </p>
                <CodeBlock language="php">{ phpBodyProperty }</CodeBlock>
            </section>

            <section>
                <h2>Envoyer une réponse</h2>
                <p>
                    Si vous désirer envoyer une réponse au client, que ce soit une erreur avec du texte ou simplement 
                    des données en JSON, vous devrez utiliser la fonction <IC>echo</IC>. Quand nous l'utilisions dans 
                    templating, elle retournait généralement du texte ou du HTML. Ici, nous l'utiliserons pour 
                    retourner du texte ou du JSON.
                </p>
                <CodeBlock language="php">{ phpEcho }</CodeBlock>
                <p>
                    Par défaut, PHP suppose que votre requête retourne du HTML ou du texte. par conséquant, si vous 
                    désirez retourner dU JSON, vous devrez l'indiquer à PHP à l'aide de la fonction <IC>header()</IC>. 
                    Vous devrez aussi convertir vos données en JSON avant de les envoyer avec le <IC>echo</IC>. Pour 
                    ce faire, nous utiliserons la fonction <IC>json_encode()</IC>.
                </p>
                <CodeBlock language="php">{ phpEchoJson }</CodeBlock>
                <p>
                    Finalement, si vous désirez retourner une erreur ou simplement un code HTTP différent de 200, vous 
                    pouvez utiliser la fonction <IC>http_response_code()</IC>. Si vous retournez une erreur, je vous 
                    recommande aussi d'ajouter un petit texte indiquant l'erreur.
                </p>
                <CodeBlock language="php">{ phpEchoError }</CodeBlock>
            </section>

            <section>
                <h2>Utiliser à la route sur le client</h2>
                <p>
                    Sur le client, pour utiliser la route, nous ferons comme avec Node.js: nous utiliserons la 
                    fonction <IC>fetch()</IC>. En général, rien ne changera dans notre utilisation de cette fonction 
                    à part la route qui doit être légèrement différente puisque nous sommes sur un server Apache.
                </p>
                <CodeBlock language="js">{ fetchApache }</CodeBlock>
                <p>
                    Voyez-vous la différence? Elle est très subtile. Au lieu d'utiliser directement la 
                    route <IC>/user/order</IC>, nous ajouterons un point <IC>.</IC> devant. Cette correction est 
                    nécessaire puisque votre projet sur Apache n'est pas à la racine du serveur.
                </p>
            </section>
        </>;
    }
};
