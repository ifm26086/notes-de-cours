import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const nullOrEmpty = 
`const isPasswordValid = (password) => {
    return !!password;
}`;

const type = 
`const isPasswordValid = (password) => {
    return typeof password === 'string';
}`;

const array =
`const isServicesValid = (services) => {
    return Array.isArray(services);
}`;

const length = 
`const isPasswordValid = (password) => {
    return password.length >= 8 && password.length <= 32;
}`;

const valueFromMultiple = 
`const isBonheurValid = (bonheur) => {
    return ['faible', 'moyen', 'fort'].includes(bonheur);
}`;

const regex = 
`const isCodePostalValid = (codePostal) => {
    return codePostal.match(/^[A-Za-z]\\d[A-Za-z][ -]?\\d[A-Za-z]\\d$/);
}`;

const arrayMax = 
`const isServicesValid = (services) => {
    return services.length <= 3;
}`;

const arrayContains = 
`const isServicesValid = (services) => {
    return services.every((value) => ['admin', 'management', 'user'].includes(value));
}`;

const multipleValidation =
`const isPasswordValid = (password) => {
    return !!password && 
        typeof password === 'string' &&
        password.length >= 8 && 
        password.length <= 32;
}`;

const custom =
`const isSameAsOther = (valeur1, valeur2) => {
    return valeur1 === valeur2;
}`;

const validFormObject = 
`const isDonneeValid = (data) => {
    return isUserNameValid(data.username) &&
        isPasswordValid(data.password) &&
        isRememberMeValid(data.rememberMe) &&
        isServicesValid(data.services) 
};`;

const serverValid = 
`app.post('/login', (request, response) => {
    // Utilisation de la validation
    if(!isObjetFormulaireValid(request.body)){
        response.sendStatus(404);
        return;
    }

    // ...
});`;

class ValidationServeur extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La validation sur le serveur est fondamentalement différente de celle sur le client. Contrairement 
                    au client qui veut valider uniquement les champs où l'utilisateur peut faire des erreurs et les 
                    afficher dans son interface graphique, le serveur veut valider toutes les données reçu pour 
                    s'assurer que personne n'essait de le briser ou de le pirater. La validation du serveur fera donc
                    la même validation que le client, mais aussi des validations supplémentaires sur les types de 
                    données reçus ou encore des validations sur les champs qui ne nécessite pas de validation dans le 
                    client.
                </p>
                <p>
                    Vous vous demandez peut-être pourquoi il est nécessaire de valider sur le serveur alors que le
                    client performe déjà plusieurs validation. La réponse est simple: Un utilisateur peut modifier le
                    code d'un client s'il sait ce qu'il fait. De plus, rien n'empêche un utilisateur d'utiliser
                    Postman pour envoyer de fausses requêtes. Il est donc impératif de valider toutes les données
                    envoyées au serveur pour s'assurer qu'elles sont valides.
                </p>
            </section>

            <section>
                <h2>Fonction de validation</h2>
                <p>
                    Puisque la validation peut être longue, je vous suggère de l'ajouter dans son propre fichier 
                    Javascript sur le serveur. Je vous suggère aussi de créer une fonction pour valider chacune de 
                    propriété de l'objet de formulaire. De cette façon, il sera plus simple de comprendre votre code
                    et de le déboguer s'il y a des problèmes.
                </p>    
                <p>
                    Vous verrez presque toujours les mêmes types de validation à faire. Voici donc quelques 
                    situations que vous pourrez rencontrer:
                </p>
                <ul>
                    <li>
                        Valider qu'une propriété n'est pas nulle ou vide
                        <CodeBlock language="js">{ nullOrEmpty }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété a un certain type
                        <CodeBlock language="js">{ type }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété est un tableau
                        <CodeBlock language="js">{ array }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété a une certaine longueur
                        <CodeBlock language="js">{ length }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété a une valeur parmi plusieurs
                        <CodeBlock language="js">{ valueFromMultiple }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété suit un certain patron
                        <CodeBlock language="js">{ regex }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété est un tableau ayant une taille maximum
                        <CodeBlock language="js">{ arrayMax }</CodeBlock>
                    </li>
                    <li>
                        Valider qu'une propriété est un tableau contenant seulement certaines valeurs parmi plusieurs
                        <CodeBlock language="js">{ arrayContains }</CodeBlock>
                    </li>
                </ul>
                <p>
                    Si vous devez faire plusieurs validation sur le même champ, n'hésitez pas à les combiner dans une 
                    seule et même fonction de validation avec des ET booléens. Voici un exemple:
                </p>
                <CodeBlock language="js">{ multipleValidation }</CodeBlock>
                <p>
                    Si vous avez de la validation personnalisé à faire, n'hésitez pas à créer vos propres fonctions 
                    pour les tester.
                </p>
            </section>

            <section>
                <h2>Validation personnalisée</h2>
                <p>
                    Si vous avez des validations plus complexe, n'hésitez pas à vous créer d'autres fonctions de
                    validation qui retourne <IC>true</IC> ou <IC>false</IC>. Par exemple, si vous voulez valider que
                    2 données ont la même valeur, vous pourriez avoir une fonction comme celle-ci:
                </p>
                <CodeBlock language="js">{ custom }</CodeBlock>
            </section>

            <section>
                <h2>Valider un formulaire ou plusieurs données</h2>
                <p>
                    Un fois toutes les fonctions de validation faite pour toutes les propriétés, vous pouvez les 
                    les utiliser dans une fonction qui validera toutes vos données d'un seul coup. Voici un 
                    exemple:
                </p>
                <CodeBlock language="js">{ validFormObject }</CodeBlock>
                <p>
                    Vous pourrez par la suite utiliser cette fonction dans votre serveur pour valider l'objet au 
                    besoin.
                </p>
                <CodeBlock language="js">{ serverValid }</CodeBlock>
            </section>
        </>;
    }
}

export default ValidationServeur;
