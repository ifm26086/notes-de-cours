import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import https from '../resources/https.png';

const certificate =
`
C:\\"Program Files"\\Git\\usr\\bin\\openssl.exe req -x509 -nodes -sha256 -days 3650 -subj "/CN=localhost" -addext "subjectAltName = DNS:localhost" -newkey rsa:2048 -keyout "./security/localhost.key" -out "./security/localhost.cert"
`;

const trustWindows =
`
Import-Certificate -FilePath .\\security\\localhost.cert -CertStoreLocation cert:\\CurrentUser\\Root
`;

export default function HTTPSCertificat() {
    return <>
        <section>
            <h2>Protocole HTTPS</h2>
            <p>
                Le protocole HTTPS est la version sécurisé du protocole HTTP. Le principe du protocole HTTPS est
                exactement le même que celui du protocole HTTP, mais dont toutes les communications sont encryptées
                dans un tunnel SSL (Secure Sockets Layer) ou TLS (Transport Layer Security). L'étude de ces
                protocoles et technique sort grandement du cadre de ce cours, donc si ces acronymes ne vous disent
                rien, Google est votre ami!
            </p>
            <p>
                Son fonctionnement est assez simple:
            </p>
            <ol>
                <li>
                    Le client contacte le serveur en lui fournissant les méthodes de chiffrement qu'il supporte.
                    Typiquement, ces méthodes de chiffrement sont automatiquement intégré dans votre navigateur.
                </li>
                <li>
                    Le serveur choisi une des méthode de chiffrement et répond au client en lui retournant un
                    certificat guarantissant qu'il est bien le véritable serveur auquel le client tente de se
                    connecter. Ce certificat est donné par une autorité de certification de confiance. Un peu comme
                    un notaire. Intégré au certificat, le serveur envoit aussi une clé d'encryption publique. Cette
                    clé permet d'encrypter des données que seul le serveur peut décrypter avec une clé privée secrète.
                </li>
                <li>
                    Le client valide le certificat reçu. Il s'assure qu'il n'est pas périmé et que l'autorité de
                    certification le certifit toujours. 
                </li>
                <li>
                    Le client génère ensuite une clé aléatoire pour l'encryption, l'encrypte avec la clé publique du
                    serveur et l'envoie au serveur. Cette clé généré servira à encrypter les communications suivantes.
                </li>
            </ol>
            <img src={https} alt="Schémas de connexion au serveur avec le protocole HTTPS" />
            <p>
                Le protocole HTTPS est aujourd'hui plus utilisé que sa version non encryptée, avec raison. Le
                protocole HTTPS nous permet de naviguer sur le web en garantissant (théoriquement) notre
                confidentialité et l'intégrité des communications. Le port utilisé par ce protocole est le 443.
            </p>
        </section>

        <section>
            <h2>Certificat</h2>
            <p>
                Comme vu ci-dessus, pour que notre serveur puisse utiliser le protocole HTTPS, nous devons tout
                d'abords avoir un certificat avec une clé publique et privée. Ce processus nécessite généralement
                d'acheter un certificat à une autorité de certification. Un certificat peut être acheté seul, mais il
                est aussi généralement inclus lors de l'achat d'un nom de domaine. 
            </p>
            <p>
                Ceci n'est toutefois pas très pratique en développement. Si tous les développeurs sur chaque projet
                devaient achetés des certificats pour leur développpement, la création d'application web coûterait
                très cher. En développement, nous allons donc tricher un peu et générer notre propre certificat. Nous
                serons donc notre propre autorité de certification pour notre serveur de développement.
            </p>
            <p>
                Pour générer un certificat, nous aurons tout d'abords besoin d'une suite d'encryption. Nous
                utiliserons OpenSSL puisqu'il est gratuit. Sur Mac OS et Linux, OpenSSL vient par défaut. Sur windows,
                nous prendrons celui qui vient avec votre installation de Git. Il se trouve ici:
            </p>
            <CodeBlock language="shell">{'C:\\"Program Files"\\Git\\usr\\bin\\openssl.exe'}</CodeBlock>
            <p>
                Exécutez ensuite les étapes suivantes:
            </p>
            <ol>
                <li>
                    Créer un dossier nommé <IC>security</IC> à la racine de votre projet.
                </li>
                <li>
                    Dans un terminal dans votre projet, exécuter la commande ci-dessous pour générer votre certificat.
                    Sur Mac OS et Linux, vous pouvez simplement remplacer
                    le <IC>C:\"Program Files"\Git\usr\bin\openssl.exe</IC> par <IC>openssl</IC>.
                    <CodeBlock language="shell">{certificate}</CodeBlock>
                </li>
                <li>
                    (Optionnel) Ajouter le certificat dans la liste des certificats de confiance de votre système
                    d'exploitation.
                    <ul>
                        <li>
                            Sur windows, exécuter la commande suivante dans le terminal PowerShell de votre projet et
                            cliquer sur le bouton <IC>Oui</IC> ou <IC>Yes</IC> pour accepter le certificat.
                            <CodeBlock language="shell">{trustWindows}</CodeBlock>
                        </li>
                        <li>
                            Sur Mac OS et Linux, vous pouvez double-cliquer sur le
                            certificat <IC>/security/localhost.cert</IC> dans votre système d'exploitation et suivre
                            les étapes à l'écran pour l'ajouter correctement.
                        </li>
                    </ul>
                </li>
            </ol>
        </section>

        <section>
            <h2>Retirer un certificat</h2>
            <p>
                Si vous avez ajouté un certificat dans la liste des certificats de confiance de votre système
                d'exploitation, vous rencontrerez probablement quelques problèmes lorsque vous développez sur un autre
                projet qui n'utilise pas HTTPS. En effet, les navigateurs modernes vont voir le certificat et vont
                automatiquement essayé de rediriger vers une communication sécurisé, ce qui va être un problème si
                vous travaillez sur plusieurs projets, dont certain en HTTP non sécurisé. Dans ce cas, vous devrez
                retirer votre certificat. Voici comment faire sur Windows:
            </p>
            <ol>
                <li>Ouvrir l'outil de recherche et rechercher <IC>certificat</IC>.</li>
                <li>Cliquer sur le choix <IC>Manage user certificates</IC>.</li>
                <li>
                    Dans la fenêtre, cliquer sur le dossier <IC>Trusted Root Certification Authorities</IC> et
                    ensuite <IC>Certificates</IC>.
                </li>
                <li>
                    Dans la liste de certificat, trouver le ou les certificats qui ont été fait
                    pour <IC>localhost</IC>. Faites un clique droit sur ces certificats et cliquer
                    sur <IC>Delete</IC>.
                </li>
            </ol>
        </section>
    </>
}
