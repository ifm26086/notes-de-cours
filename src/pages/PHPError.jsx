import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const phpError =
`Warning: include(headers.php): 
failed to open stream: No such file or directory 
in C:\\xampp\\htdocs\\test\\index.php on line 2

Warning: include(): Failed opening 'headers.php' 
for inclusion (include_path='C:\\xampp\\php\\PEAR') 
in C:\\xampp\\htdocs\\test\\index.php on line 2`;

const phpDump = 
`<?php 
    $tab = array(0, 1, 2, 3, 4, 6, 7, 8, 9);

    /* Il semble y avoir une erreur dans notre */
    /* tableau, donc on l'affiche pour voir son */
    /* contenu */
    var_dump($tab);
?>

<div style="display: flex; justify-content: space-between;">
    <?php for($i = 0 ; $i < count($tab) ; $i++) { ?>
        <div>
            <?php echo $tab[$i]; ?>
        </div>
    <?php } ?>
</div>`;

const logs =
`[Fri Nov 06 10:22:43.471985 2020] [php7:warn] [pid 1496:tid 1808] [client ::1:52475] 
    PHP Warning:  include(header.php): failed to open stream: No such file or 
    directory in C:\\xampp\\htdocs\\test\\index.php on line 2
[Fri Nov 06 10:22:43.471985 2020] [php7:warn] [pid 1496:tid 1808] [client ::1:52475] 
    PHP Warning:  include(): Failed opening 'header.php' for inclusion 
    (include_path='C:\\xampp\\php\\PEAR') in C:\\xampp\\htdocs\\test\\index.php 
    on line 2
[Fri Nov 06 11:14:08.419928 2020] [php7:notice] [pid 1496:tid 1816] [client ::1:60581] 
    message
[Fri Nov 06 11:14:48.858874 2020] [php7:notice] [pid 1496:tid 1808] [client ::1:60582] 
    message`;

export default class PHPError extends React.Component {
    render() {
        return <>
            <section>
                <h2>Affichage des erreurs</h2>
                <p>
                    Malheureusement, contrairement à un serveur Node.js, le serveur Apache avec PHP n'a pas de 
                    console. S'il y a une erreur dans votre code PHP sur le serveur en développement, Apache vous 
                    l'affichera directement dans la fenêtre du navigateur. Voici à quoi peut ressembler ces erreurs:
                </p>
                <CodeBlock language="txt">{ phpError }</CodeBlock>
                <p>
                    En général, les erreurs sont assez bien expliqué. On peut les lire de façon similaire à des stack 
                    trace. Dans le cas de l'erreur ci-dessus, elle est causé par un <IC>include</IC> vers un fichier
                    inexistant.
                </p>
            </section>

            <section>
                <h2>Débogage</h2>
                <p>
                    Il n'existe pas vraiment de solution de débogage en PHP comme avec d'autres langages de 
                    programmation. Vous devrez donc vous satisfaire d'afficher le contenu de vos variables directement 
                    dans votre page HTML. Je vous recommande fortement d'utiliser la fonction <IC>var_dump</IC> pour 
                    ce faire.
                </p>
                <CodeBlock language="php-template">{ phpDump }</CodeBlock>
            </section>

            <section>
                <h2>Fichier de logs</h2>
                <p>
                    Dans certains cas, l'erreur dans le code PHP ne sera pas affiché dans le navigateur. Dans ces cas,
                    l'erreur est toutefois ajouté dans un fichier de logs. PHP utilise généralement 2 fichiers de 
                    logs:
                </p>
                <ul>
                    <li>
                        Le fichier de logs d'erreur de Apache dans le dossier <IC>/xampp/apache/logs/error.log</IC>
                    </li>
                    <li>
                        Le fichier de logs de PHP dans le dossier <IC>/xampp/php/logs/php_error_log</IC>
                    </li>
                </ul>
                <p>
                    Le fichier qui est utilisé dépends du système et de la configuration de PHP et Apache. Dans tous 
                    les cas, les erreurs sont ajouté à la fin du fichier s'il y en a.
                </p>
                <CodeBlock language="txt">{ logs }</CodeBlock>
            </section>
        </>;
    }
};
