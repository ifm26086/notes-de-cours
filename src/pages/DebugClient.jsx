import React from 'react';
import IC from '../component/InlineCode'

import debugChrome from '../resources/debug-chrome.png';
import debugChromeTool from '../resources/debug-chrome-tool.png';

export default class DebugClient extends React.Component {
    render() {
        return <>
            <section>
                <h2>Débogueur de Chrome</h2>
                <p>
                    Une bonne façon de déboguer les erreurs sur le client est de mettre des <IC>console.log()</IC> sur 
                    des variables aux endroits où l'erreur est survenu selon vos stack trace. Cette technique peut 
                    être très rapide si vous êtes habitué de faire du débogage (comme moi). Toutefois, si cette 
                    technique ne vous sort aucun résultat, je vous suggère fortement d'utiliser le débogueur.
                </p>
                <p>
                    Voici 3 façon d'y accèder:
                </p>
                <ul>
                    <li>
                        Faire un clique droit dans votre page Web et cliquer sur <IC>Inspecter</IC>.
                    </li>
                    <li>
                        Dans le menu en haut à droite, cliquer sur <IC>Plus d'outils</IC> &gt; <IC>Outils de développement</IC>
                    </li>
                    <li>
                        Utiliser le raccourci clavier <IC>Ctrl</IC> + <IC>Shift</IC> + <IC>I</IC>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Interface graphique</h2>
                <p>
                    Voici l'interface graphique de base pour l'accès au code dans votre navigateur ainsi que la 
                    description de certaines de ses sections:
                </p>
                <img src={ debugChrome } alt="Outils de développement de Chrome"/>
                <ol>
                    <li>
                        Onglet permettant de voir les fichiers du projet. En général, l'onglet <IC>Elements</IC> sert 
                        pour visualiser le HTML et le CSS de la page Web. L'onglet <IC>Source</IC> sert plutôt à voir 
                        le code Javascript (ou tout autre fichier) chargé par votre site Web.
                    </li>
                    <li>
                        La section à gauche montre tous le fichiers chargé par le site Web. Nous nous intéresserons 
                        principalement aux fichiers Javascript ici.
                    </li>
                    <li>
                        La section des lignes de code et des points d'arrêt (breakpoint). Pour mettre un point d'arrêt 
                        dans le navigateur, vous pouvez simplement cliquer sur le numéro de la ligne. Cliquer de 
                        nouveau sur un point d'arrêt pour l'enlever. Les points d'arrêt forceront le code à s'arrêter 
                        à une ligne spécifique et à attendre que vous lui indiquiez quoi faire.
                    </li>
                    <li>
                        Contenu du fichier sélectionné. Vous pouvez voir l'ensemble du fichier ici. Si vous êtes en 
                        mode débogage, vous pouvez sélectionner du code ou mettre votre souris sur des variables pour 
                        voir leur contenu.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Outil de débogage</h2>
                <p>
                    Si le code Javascript est arrêté à un point d'arrêt que vous avez mis, l'interface changera 
                    légèrement pour vous offrir de nouveaux outils:
                </p>
                <img src={ debugChromeTool } alt="Débogeur de Chrome"/>
                <ol>
                    <li>
                        La ligne présentement exécuté est toujours en bleu. Vous pouvez mettre votre curseur sur 
                        des variables ou sélectionner du code autour de cette ligne pour voir leur résultat.
                    </li>
                    <li>
                        Outil indiquant au code de recommencer son exécution normale jusqu'au prochain point d'arrêt 
                        s'il y en a un.
                    </li>
                    <li>
                        Outil indiquant au débogueur d'exécuter la ligne courante et de passer à la prochaine ligne de 
                        code à exécuter.
                    </li>
                    <li>
                        Outil indiquant au débogueur d'entrer dans la fonction à la ligne courante, s'il y a lieu.
                    </li>
                    <li>
                        Outil indiquant au débogueur de sortir de la fonction à la ligne courante, s'il y a lieu.
                    </li>
                </ol>
            </section>
        </>;
    }
};
