import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-16-distribué.zip';
import solution from '../resources/laboratoire-16-solution.zip';

const html = 
`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tweetcher</title>

    <link href="./css/normalize.css" rel="stylesheet" />
    <link href="./css/main.css" rel="stylesheet" />
</head>
<body>
    <header>
        <h1>Tweetcher</h1>
    </header>
    <main>
        <div class="post">
            <div class="post-header">
                <div class="post-user">Username</div>
                <div class="post-date">Date</div>
            </div>
            <div class="post-content">Message</div>
            <div class="post-footer">
                <img src="./img/like.svg" alt="Like">
                <div class="post-like">Likes</div>
            </div>
        </div>
    </main>
    <footer>
        &copy; Collège La Cité
    </footer>
</body>
</html>`;

const json = 
`[
    { 
        "username": "jwilki", 
        "date": "2020-09-08", 
        "message": "C'est la rentrée, j'enseigne à 8h ce matin et je suis pas prêt...", 
        "like": 124
    },
    { 
        "username": "Dude", 
        "date": "2020-10-05", 
        "message": "Bon matin tout le monde! J'aurais dormi 2h de plus!", 
        "like": 12
    },
    { 
        "username": "Student_A", 
        "date": "2020-10-05", 
        "message": "Me lever pour un cours à 8h du mat est vraiment trop dur! X_X", 
        "like": 2487
    },
    { 
        "username": "Dude", 
        "date": "2020-10-08", 
        "message": "Nice", 
        "like": 69
    },
    { 
        "username": "jwilki", 
        "date": "2020-10-26", 
        "message": "Semaine de d'étude pour les étudiants = Semaine de correction pour les profs", 
        "like": 89
    },
    { 
        "username": "Student_A", 
        "date": "2020-10-27", 
        "message": "Je suis le seul qui a pas accès au portail du collège? #isitmycomputer", 
        "like": 2651
    }
]`;

export default class Laboratoire16 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Dans ce projet, nous programmerons la liste TODO que nous avons fait avec Node.js, mais à l'aide 
                    des formulaires en ASP.NET. Voici les points importants pour réussir ce laboratoire:
                </p>
                <ul>
                    <li>
                        Télécharger et décompresser le fichier <IC>distribué.zip</IC>.
                    </li>
                    <li>
                        Ouvrir le projet dans votre éditeur de code favori.
                    </li>
                    <li>
                        Séparer le code HTML ci-dessous entre le fichier <IC>Index.cshtml</IC> et le fichier <IC>_Layout.cshtml</IC>.
                        Les parties réutilisable entre les pages du code devrait être dans le fichier <IC>_Layout.cshtml</IC>. Les
                        partie spécifique du code devrait être dans le fichier <IC>Index.cshtml</IC>.
                        <CodeBlock language="html">{ html }</CodeBlock>
                    </li>
                    <li>
                        Créer une classe <IC>Post</IC> qui va contenir les attributs ci-dessous et un constructeur paramétré:
                        <ul>
                            <li>Nom d'utilisateur</li>
                            <li>Date de publication (Utiliser la classe <IC>DateTime</IC>)</li>
                            <li>Message</li>
                            <li>Nombre de likes</li>
                        </ul>
                    </li>
                    <li>
                        Dans le côté serveur de votre page, ajouter une liste de la classe <IC>Post</IC>. Initialiser cette liste avec 
                        les valeurs ci-dessous dans le constructeur de votre page.
                        <CodeBlock language="json">{ json }</CodeBlock>
                    </li>
                    <li>
                        Dans le HTML de votre page, boucler sur la liste de <IC>Post</IC> pour les ajouter dans l'interface graphique.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
};
