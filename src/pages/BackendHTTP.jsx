import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';

export default function BackendHTTP() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                HTTP est un protocole de communication. En des termes plus simple, c'est un langage qui est utilisé par 
                les clients web pour communiquer avec les serveurs web. Un peu comme moi, qui vous parle ou vous écrit 
                en français pour partager la matière avec vous, les clients web parle le HTTP pour se faire comprendre 
                par un serveur web.
            </p>
            <p>
                HTTP est très flexible et extensible. Il a été inventé dans le début des années 1990 et est encore 
                aujourd'hui utilisé pour la majorité des communications sur l'Internet. Dans ce cours, nous 
                utiliserons le protocole HTTP pour communiquer entre un client (navigateur Web) et un serveur 
                (serveur Node.js).
            </p>
            <p>
                Comme mentionné dans les pages précédantes, n'oubliez pas que la communication client-serveur part 
                toujours du client. En effet, c'est le client qui enverra des messages ou requêtes HTTP au serveur. Le 
                contraire n'arrivera jamais (...presque jamais).
            </p>
            <p>
                Bien que l'utilisation de ce protocole peut être très complexe, nous essayerons de la simplifier le 
                plus possible.
            </p>
        </section>

        <section>
            <h2>Anatomie d'une requête HTTP</h2>
            <p>
                Pour envoyer un message en HTTP, il y a généralement 3 composants à configurer:
            </p>
            <dl>
                <dt>Méthode HTTP</dt>
                <dd>
                    C'est le type de requête HTTP. Nous programmerons généralement seulement 5 types de méthodes, soit 
                    le <IC>GET</IC>, <IC>POST</IC>, <IC>PUT</IC>, <IC>PATCH</IC> et <IC>DELETE</IC> qui sont 
                    respectivement des opérations de recherche, insertion, remplacement, modification et suppression.
                </dd>
                <dt>Adresse du serveur et route</dt>
                <dd>
                    L'adresse du serveur est simplement l'adresse URL du serveur web auquel on veut envoyer le message 
                    HTTP. Comme le serveur HTTP peut s'attendre à recevoir plusieurs messages, il ouvrira généralement 
                    plusieurs routes auxquelles les clients pourront envoyer des messages. Ces routes sont 
                    essentiellement des ressources ou des fonctionnalités que le serveur offre.
                </dd>
                <dt>Données à envoyer</dt>
                <dd>
                    Très souvent, nous allons vouloir envoyer des données dans nos messages HTTP. Par exemple, si nous 
                    voulons faire une recherche, nous devons envoyer le texte à rechercher. De la même façon, si nous 
                    voulons créer un compte sur un site web, nous devons envoyer les information nécessaire, comme le 
                    nom d'utilisateur et le mot de passe.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Méthodes de requêtes</h2>
            <p>
                Comme mentionné plus haut, la méthode d'une requête HTTP est son type de requête. Le type indique 
                généralement ce que la requête fait, donc comment la requête manipule les données. En bref, ça nous 
                indique si la requête sert à faire des recherches, à ajouter, à modifier ou à supprimer des données.
                La liste complète des méthodes HTTP est disponible au lien suivant:
            </p>
            <p>
                <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods" target="_blank" rel="noopener noreferrer">HTTP request methods</a>
            </p>
            <p>
                Voici la liste des méthodes les plus importantes pour le cours:
            </p>
            <dl>
                <dt>GET</dt>
                <dd>
                    Permet d'aller chercher une ressource sur le serveur. Les requêtes <IC>GET</IC> ne devrait 
                    jamais modifier des données sur un serveur, seulement les retourner. Quand vous naviguez sur 
                    un site Web, chaque fichier HTML, CSS et Javascript est demandé par une 
                    requêtes <IC>GET</IC> qui est faite automatiquement par votre navigateur Web.
                </dd>

                <dt>POST</dt>
                <dd>
                    La méthode <IC>POST</IC> sert à envoyer des données au serveur, souvent pour apporter des 
                    changements aux ressources du serveur. On l'utilise souvent pour ajouter (ou parfois modifier) 
                    des données sur le serveur.
                </dd>

                <dt>PUT</dt>
                <dd>
                    La méthode <IC>PUT</IC>  est similaire à la méthode <IC>POST</IC>. Elle sert à remplacer ou 
                    ajouter des ressources sur le serveur. On l'utilise souvent pour remplacer (ou parfois 
                    ajouter) des données sur le serveur.
                </dd>

                <dt>PATCH</dt>
                <dd>
                    La méthode <IC>PATCH</IC>  est similaire à la méthode <IC>PUT</IC>. Elle sert à modifier des 
                    ressources sur le serveur. On l'utilise souvent pour modifier partiellement des données sur le 
                    serveur.
                </dd>

                <dt>DELETE</dt>
                <dd>
                    La méthode <IC>DELETE</IC> sert à supprimer des ressources sur le serveur. On l'utilise pour 
                    supprimer des données sur notre serveur.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Adresse URL et routes</h2>
            <p>
                L'adresse URL est un moyen pour les programmes d'indiquer où nous voulons chercher certaines 
                ressources sur l'Internet. L'adresse URL défini comment nous naviguerons (protocole de 
                communication) et ou nous voulons aller. Le protocole HTTP est généralement utilisé lorsqu'on 
                utilise un URL quand on navigue sur le Web. Nos serveurs en feront autant. Il est donc important 
                de bien comprendre l'anatomie d'une adresse URL.
            </p>
            <p>
                Pour bien comprendre les différentes parties de l'adresse URL, je vous suggère fortement le lien
                ci-dessous:
            </p>
            <p>
                <a href="https://developer.mozilla.org/fr/docs/Apprendre/Comprendre_les_URL" target="_blank" rel="noopener noreferrer">Comprendre les URL et leur structure</a>
            </p>
            <p>
                Dans le cadre du cours, les parties importantes de l'URL seront: 
            </p>
            <dl>
                <dt>Nom de domaine</dt>
                <dd>
                    C'est le nom de domaine du serveur avec lequel nous voulons communiquer. Vous pourrez aussi 
                    voir des adresse IP ici. Si le serveur est sur le même ordinateur que le client (comme en 
                    développement), vous verrez souvent <IC>localhost</IC> ou <IC>127.0.0.1</IC>.
                </dd>
                <dt>Chemin (aussi appelé "route")</dt>
                <dd>
                    Les routes sont les différentes fonctionnalitées offertes par le serveur. Les clients appelent 
                    ces routes sur le serveur pour lui indiquer ce qu'ils veulent faire. Si un client appèle une 
                    route qui n'a pas été programmé sur un serveur, vous aurez généralement la réponse <IC>404 Not found</IC>.
                </dd>
                <dt>Paramètres (aussi appelé "query")</dt>
                <dd>
                    Les paramètres sont une façon simple d'envoyer de l'information au serveur. Les clients pourront modifier les 
                    paramètres pour envoyer des données spécifiques aux serveurs. Vous verrez qu'il y a plusieurs façon d'envoyer
                    des données au serveur et par conséquant, cette option n'est pas toujours choisie.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Envoyer des données</h2>
            <p>
                En général, nous voulons envoyer des données dans nos requêtes HTTP. Il y a 3 façons d'envoyer 
                des données:
            </p>
            <dl>
                <dt>Dans le corps (body) HTTP</dt>
                <dd>
                    Le corps, ou le <IC>body</IC> est généralement l'endroit où l'on va mettre les données à envoyer 
                    au serveur. Les données dans le corps peuvent être encryptées facilement pour la sécurité et il 
                    est possible d'envoyer des données volumineuses à cet endroit. Le seul problème de cette façon 
                    d'envoyer les données est qu'elle ne fonctionne pas avec la méthode <IC>GET</IC>. Il faut donc 
                    utiliser les 2 autres façon avec le <IC>GET</IC>. Dans le cours, nous enverrons les données dans 
                    le corps sous le format JSON.
                </dd>
                <dt>Dans la route</dt>
                <dd>
                    Il est possible d'envoyer certaines données dans la route directement. Par exemple, si nous 
                    cherchons demandons au serveur de retourner les données de l'usager ayant 
                    l'identifiant <IC>69</IC>, nous pourrions utiliser la route suivante <IC>/user/69</IC>. Les 
                    données dans la route ne peuvent pas être encryptées et sont limitées à de petites tailles.
                </dd>
                <dt>Dans les paramètres</dt>
                <dd>
                    Si vous n'aimez pas envoyer des données directement dans la route, une autre façon de faire est 
                    avec les paramètres. Si nous reprenons l'exemple ci-dessus, pour chercher les données de l'usager
                    ayant l'identifiant <IC>69</IC>, nous pourrions utiliser l'URL suivant <IC>/user?id=69</IC>. Les 
                    données dans les paramètres ne peuvent pas être encryptées et sont limitées à de petites tailles.
                </dd>
            </dl>
            <ColoredBox heading="Attention">
                Il est important de comprendre que l'on ne peut pas envoyer de données dans le corps HTTP pour une 
                requête <IC>GET</IC>. Il faudra donc utiliser la route et les paramètres pour les requêtes utilisant
                ces méthodes. Pour les autres méthodes, vous pouvez utiliser n'importe quelle façon d'envoyer des 
                données. Il est même possible de combiner plusieurs façon ensemble.
            </ColoredBox>
        </section>

        <section>
            <h2>Réponse d'une requête HTTP</h2>
            <p>
                Après le traitement d'une requête HTTP, le serveur doit toujours retourner une réponse au client. 
                Cette réponse va souvent contenir des données à retourner au client, mais peut aussi être vide. Elle 
                doit toutefois toujours contenir un code de status. Ces codes de status sont un indicateur pour le 
                client. Ils lui permettront rapidement de voir si la requête a été réussis avec succès ou s'il y a eu 
                des erreurs. Même si vous pensez ne pas les connaître, vous avez assurément entendu parler de 
                certaines d'entre elle. La plus connu est <IC>404 - Not found</IC>. La liste complète des code de 
                status peut être trouvé ici:
            </p>
            <p>
                <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status" target="_blank" rel="noopener noreferrer">HTTP response status codes</a>
            </p>
            <p>
                Voici la liste des codes de status les plus importants pour le cours:
            </p>
            <table>
                <tr>
                    <th>Code</th><th>Nom</th><th>Description</th>
                </tr>
                <tr>
                    <td>200</td>
                    <td>OK</td>
                    <td>Indique que la requête est retourné avec succès</td>
                </tr>
                <tr>
                    <td>400</td>
                    <td>Bad Request</td>
                    <td>La syntaxe de la requête n'était pas bonne</td>
                </tr>
                <tr>
                    <td>403</td>
                    <td>Forbidden</td>
                    <td>Vous n'avez pas les accès pour faire cette requête</td>
                </tr>
                <tr>
                    <td>404</td>
                    <td>Not found</td>
                    <td>La ressource demandé par la requête n'existe pas</td>
                </tr>
                <tr>
                    <td>409</td>
                    <td>Conflict</td>
                    <td>La requête n'a pas pu se faire à cause d'un conflit</td>
                </tr>
                <tr>
                    <td>418</td>
                    <td>I'm a teapot</td>
                    <td>¯\_(ツ)_/¯</td>
                </tr>
                <tr>
                    <td>500</td>
                    <td>Internal server error</td>
                    <td>La requête a échoué par une erreur sur le serveur</td>
                </tr>
                <tr>
                    <td>501</td>
                    <td>Not implemented</td>
                    <td>La requête a échoué parce qu'elle n'est pas encore implémenté</td>
                </tr>
                <tr>
                    <td>502</td>
                    <td>Bad Gateway</td>
                    <td>
                        Indique a échoué parce que le serveur n'a pas réussi à accéder à une ressource 
                        (généralement une base de données)
                    </td>
                </tr>
            </table>
        </section>

        <section>
            <h2>Limitation</h2>
            <p>
                Historiquement, le protocole HTTP avait une grande limitation. Malgré sa flexibilité, il était 
                incapable d'envoyer un message du serveur jusqu'au client sans que le client en est préalablement 
                fait la requête. Ce genre de problèmes semble trivial à première vu, mais est très important pour 
                nos applications Web moderne qui doivent réagir en temps réel avec les changements sur un serveur.
            </p>
            <p>
                La solution utilisé jusqu'à récemment était les WebSockets. C'était un autre protocole de 
                communication qui pouvait être utilisé pour communiquer de façon bi-directionnelle entre un client 
                et un serveur. Ils sont encore utilisé, mais on va préféré une nouvelle technologie aujourd'hui.
            </p>
            <p>
                Depuis 2015, une nouvelle version du protocole HTTP (HTTP/2) nous permet de faire des 
                communications bi-directionnelle de façon plus ou moins simple. Aujourd'hui, la plupart des navigateurs le 
                supporte sans problèmes. On estime présentement que plus de 95% des navigateurs Web utilisés dans 
                le monde supporte aujourd'hui HTTP/2, ce qui rends les WebSockets moins utile.
            </p>
        </section>
    </>;
}
