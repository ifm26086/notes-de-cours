import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

export default class ASPCSharp extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction et tutoriels</h2>
                <p>
                    C# est le langage de programmation objet de Microsoft. C'est un langage qui ressemble beaucoup à 
                    Java. Vous pouvez presque copier/coller du code Java dans un fichier C# et le faire fonctionner 
                    sans problème.
                </p>
                <p>
                    Si vous voulez une introduction un peu plus approfondie au langage C#, vous pouvez voir les 
                    tutoriels aux liens suivants:
                </p>
                <ul>
                    <li>
                        <a href="https://docs.microsoft.com/en-us/dotnet/csharp/" target="_blank" rel="noopener noreferrer">
                            C# documentation
                        </a>
                    </li>
                    <li>
                        <a href="https://www.w3schools.com/cs/" target="_blank" rel="noopener noreferrer">
                            C# Tutorial
                        </a>
                    </li>
                    <li>
                        <a href="https://www.codecademy.com/learn/learn-c-sharp" target="_blank" rel="noopener noreferrer">
                            Learn C#
                        </a>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Différences avec Java</h2>
                <p>
                    Voici une liste des différences entre les mot-clés de Java et de C# que nous rencontrerons dans le 
                    cours:
                </p>
                <table>
                    <tr><th>Java</th><th>C#</th></tr>
                    <tr><td><IC>import</IC></td><td><IC>using</IC></td></tr>
                    <tr><td><IC>package</IC></td><td><IC>namespace</IC></td></tr>
                    <tr><td><IC>extends</IC></td><td><IC>:</IC></td></tr>
                    <tr><td><IC>boolean</IC></td><td><IC>bool</IC></td></tr>
                    <tr><td>getter/setter</td><td>property</td></tr>
                </table>
                <p>
                    Les properties de C# sont un moyen de remplacer les getter et setter de Java. Pour une meilleure 
                    idée de comment les utiliser, je vous suggère le lien suivant:
                </p>
                <p>
                    <a href="https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties" target="_blank" rel="noopener noreferrer">
                        Properties (C# Programming Guide)
                    </a>
                </p>
                <p>
                    Le reste du code C#, soit les conditions, les boucles, la déclaration de variable et la création 
                    de méthodes est exactement comme en Java.
                </p>
            </section>

            <section>
                <h2>Nomenclature</h2>
                <p>
                    La nomenclature du C# est la même que celle de Java à quelques exceptions:
                </p>
                <ul>
                    <li>
                        Les noms de méthodes et fonctions commencent par une lettre majuscule et utilisent le CamelCase.
                    </li>
                    <li>
                        Les noms de properties commencent par une lettre majuscule et utilisent le CamelCase.
                    </li>
                    <li>
                        Les noms de classes commencent par une lettre majuscule et utilisent le CamelCase.
                    </li>
                    <li>
                        Les noms de variables commencent par une lettre minuscule et utilisent le CamelCase.
                    </li>
                    <li>
                        La classe <IC>string</IC> commence utilise une lettre minuscule.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Librairies de base</h2>
                <p>
                    Quand vous commencez à utiliser C#, il est utile de connaître les librairies de code ci-dessous.
                    Ces librairies sont directement disponible avec le compilateur. Vous n'avez donc pas besoin de 
                    faire de téléchargement.
                </p>
                <dl>
                    <dt>System</dt>
                    <dd>
                        <p>
                            Contient des utilitaires de base du langage C# comme entre autre les fonctions pour écrire 
                            dans la console.
                        </p>
                        <CodeBlock language="csharp">using System;</CodeBlock>
                    </dd>

                    <dt>System.Collections.Generic</dt>
                    <dd>
                        <p>
                            Contient les structures de données de base du langage C# comme entre autre les listes, les 
                            ensembles et les ensembles clé/valeur.
                        </p>
                        <CodeBlock language="csharp">using System.Collections.Generic;</CodeBlock>
                    </dd>
                </dl>
            </section>
        </>;
    }
};
