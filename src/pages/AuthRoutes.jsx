import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const inscription =
`app.post('/inscription', async (request, response, next) => {
    // On vérifie le le courriel et le mot de passe
    // envoyé sont valides
    if (validateCourriel(request.body.email) &&
        validateMotDePasse(request.body.password)) {
        try {
            // Si la validation passe, on crée l'utilisateur
            await addUtilisateur(request.body.courriel, request.body.motDePasse);
            response.sendStatus(201);
        }
        catch (error) {
            // S'il y a une erreur de SQL, on regarde
            // si c'est parce qu'il y a conflit
            // d'identifiant
            if(error.code === 'SQLITE_CONSTRAINT') {
                response.sendStatus(409);
            }
            else
            {
                next(error);
            }
        }
    }
    else {
        response.sendStatus(400);
    }
});`;

const connexion =
`app.post('/connexion', (request, response, next) => {
    // On vérifie le le courriel et le mot de passe
    // envoyé sont valides
    if (validateCourriel(request.body.email) &&
        validateMotDePasse(request.body.password)) {
        // On lance l'authentification avec passport.js
        passport.authenticate('local', (error, user, info) => {
            if (error) {
                // S'il y a une erreur, on la passe
                // au serveur
                next(error);
            }
            else if (!user) {
                // Si la connexion échoue, on envoit
                // l'information au client avec un code
                // 401 (Unauthorized)
                response.status(401).json(info);
            }
            else {
                // Si tout fonctionne, on ajoute
                // l'utilisateur dans la session et
                // on retourne un code 200 (OK)
                request.logIn(user, (error) => {
                    if (error) {
                        next(error);
                    }

                    response.sendStatus(200);
                });
            }
        })(request, response, next);
    }
    else {
        response.sendStatus(400);
    }
});`;

const deconnexion =
`app.post('/deconnexion', (request, response) => {
    // Déconnecter l'utilisateur
    request.logout();

    // Rediriger l'utilisateur vers une autre page
    response.redirect('/');
});`;

export default function AuthRoutes() {
    return <>
        <section>
            <h2>Routes d'authentification</h2>
            <p>
                À la base, nous aurons besoin uniquement de 3 routes pour gérer l'authentification.
            </p>
            <ol>
                <li>Route pour l'inscription d'un compte</li>
                <li>Route pour la connexion à un compte</li>
                <li>Route pour la déconnexion d'un compte</li>
            </ol>
            <p>
                Cette page détaillera comment programmer ces 3 routes dans votre fichier <IC>server.js</IC>.
            </p>
        </section>

        <section>
            <h2>Inscription</h2>
            <p>
                L'inscription est à la base une route assez facile à programmer. Elle suit les même règle que les
                routes précédemment créer dans le cours. Sa seule difficulté est que nous voulons probablement
                retourner une erreur différente si l'utilisateur essait de se créer un compte avec un identifiant déjà
                existant. Dans notre cas, si un compte existe déjà avec l'adresse courriel fourni par l'utilisateur.
            </p>
            <p>
                Voici comment on pourrait faire:
            </p>
            <CodeBlock language="js">{inscription}</CodeBlock>
        </section>

        <section>
            <h2>Connexion</h2>
            <p>
                La connexion est un peu plus compliqué que l'inscription. Lors de la connexion, nous devons lancer
                l'authentification avec <IC>passport</IC>. Ensuite, selon le résultat de <IC>passport</IC>, nous
                devons retourner un message au client. Le code est donc un peu plus complexe et différent de ce que
                l'on fait habituellement. Voici un exemple qui pourrait fonctionner:
            </p>
            <CodeBlock language="js">{connexion}</CodeBlock>
            <p>
                Si vous utiliser ce code, assurez-vous de mettre les paranthèses et accolades aux bonnes places. Si
                vous en mettez trop, pas assez ou aux mauvais endroits, ça pourrait vous causer des problèmes.
            </p>
        </section>

        <section>
            <h2>Déconnexion</h2>
            <p>
                La déconnexion est définitivement le plus facile des 3 routes à programmer. Il suffit simplement
                d'indiquer à <IC>passport</IC> de déconnecter l'utilisateur en vidant sa session.
            </p>
            <CodeBlock language="js">{deconnexion}</CodeBlock>
        </section>
    </>
}