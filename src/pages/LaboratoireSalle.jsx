import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-salle-solution.zip'

export default function LaboratoireSalle() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Programmez un serveur Web avec Node.js et Express qui répond aux besoins de la mise en situation 
                ci-dessous.
            </p>
            <p>
                Si vous désirez tester la solution, n'oubliez pas de faire un <IC>npm install</IC> sur le projet 
                au préalable pour qu'il télécharge les packages nécessaires. La solution contient une collection
                Postman pour vous aider à tester votre serveur.
            </p>
        </section>

        <section>
            <h2>Mise en situation</h2>
            <p>
                Vous voulez créer un serveur permettant de créer des salles de clavardage. Vous voulez permettre 
                aux utilisateurs de visualiser les salles existante, de créer des salles et de les supprimer. De 
                plus, vous voulez pouvoir rejoindre une salle de clavardage, en sortir et voir quels sont les 
                utilisateurs présent dans une salle.
            </p>
            <p>
                Vous ne savez pas encore comment faire les fonctions de clavardage, donc vous les ajouterez 
                probablement dans une future version 2.0 de votre serveur (qui n'arrivera jamais...).
            </p>
            <p>
                Vous avez vraiment confiance aux utilisateurs (vous ne devriez probablement pas, mais bon...), donc 
                vous ne vous souciez pas du côté sécurité du serveur.
            </p>
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
