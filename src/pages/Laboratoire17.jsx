import React from 'react';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-17-solution.zip';

export default class Laboratoire17 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Dans ce projet, nous programmerons la liste TODO que nous avons fait avec Node.js, mais à l'aide 
                    des formulaires en ASP.NET. Voici les points importants pour réussir ce laboratoire:
                </p>
                <ul>
                    <li>
                        Créer le projet ASP.NET.
                    </li>
                    <li>
                        Faire le ménage des fichiers inutiles dans votre projet.
                    </li>
                    <li>
                        Copier le HTML et le CSS de la liste TODO dans votre nouveau projet dans les bons fichiers.
                    </li>
                    <li>
                        Mettre la liste de TODO HTML dans un formulaire.
                    </li>
                    <li>
                        Programmer une classe représentant un TODO et un contexte contenant la liste de TODO qui sera utilisé comme service.
                    </li>
                    <li>
                        Programmer l'ajout et le cochage des TODO dans le formulaire.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
};
