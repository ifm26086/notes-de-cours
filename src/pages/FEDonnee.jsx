import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import WebExample from '../component/WebExample';

const dataHTML =
`<label>
    Nom:
    <input type="text" id="inputNom">
</label>

<input id="buttonEnvoyer" type="button" value="Envoyer">`;

const dataJs =
`// Variables d'éléments HTML
let inputNom = document.getElementById('inputNom');
let buttonEnvoyer = document.getElementById('buttonEnvoyer');

// Fonction pour aller chercher le nom dans
// l'interface graphique et l'envoyer avec "fetch"
const envoyerNom = async () => {
    // On va chercher le nom dans le input
    let data = { nom: inputNom.value };

    // On envoit les données au serveur
    fetch('/route-inexistante', { 
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
}

// On exécute la fonction "envoyerNom" lors
// du clique sur le bouton
buttonEnvoyer.addEventListener('click', envoyerNom);`;

const dataObject =
`// On peut envoyer un objet complexe
let data = {
    id: inputId.value,
    user: {
        name: inputName.value,
        color: inputColor.value,
        order: Math.floor(Math.random() * 101) + 1 
    },
    items: []
}

// On peut aussi envoyer des tableaux de données
for(let i = 0 ; i < 10 ; i++){
    data.items.push({ value: i });
}`;

export default function FEDonnee() {
    return <>
        <section>
            <h2>Trouver les données</h2>
            <p>
                Lorsque nous faisons des requêtes <IC>fetch</IC> à un serveur, nous avons souvent besoin d'envoyer des
                données. Que ce soit pour demander au serveur de retourner l'information sur le profil d'un
                utilisateur ou encore envoyer les données d'un élément à ajouter dans une base de données, nous
                devrons fournir des données à notre requête <IC>fetch</IC>. Où trouverons-nous ces données du côté du
                client?
            </p>
            <p>
                En général, du côté client, c'est l'utilisateur qui entre les données dans une interface graphique.
                Nous devrons donc être en mesure d'extraire ces données de l'interface graphique au moment voulu,
                comme par exemple, sur le clique d'un bouton:
            </p>
            <WebExample>
                <WebExample.Code type="html">{dataHTML}</WebExample.Code>
                <WebExample.Code type="js">{dataJs}</WebExample.Code>
            </WebExample>
        </section>

        <section>
            <h2>Organiser les données</h2>
            <p>
                Lorsqu'on veut envoyer les données, on va généralement les organiser dans un objet <IC>data</IC> que
                l'on enverra au serveur. Cet objet <IC>data</IC> peut être organisé de la façon que vous voulez, tant
                qu'il reste un objet.
            </p>
            <CodeBlock language="js">{dataObject}</CodeBlock>
            <p>
                Assurez-vous de bien organiser vos données dans l'objet <IC>data</IC>. Cela facilitera leur lecture
                sur le serveur.
            </p>
        </section>
    </>;
}
