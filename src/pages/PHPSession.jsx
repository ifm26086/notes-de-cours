import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const startSession = 
`session_start();`;

const addSession = 
`$_SESSION['nom-de-variable'] = 'Une valeur';`;

const addSessionStart =
`session_start();
if(!isset($_SESSION['nom-de-variable'])){
    $_SESSION['nom-de-variable'] = array();
}`;

const getSession = 
`echo $_SESSION['nom-de-variable'];`;

const deleteSession =
`unset($_SESSION['nom-de-variable']);`;

export default class PHPSession extends React.Component {
    render() {
        return <>
            <section>
                <h2>Concept</h2>
                <p>
                    Une session est un moyen de sauvegarder des informations de façon temporaire pour chaque connexion 
                    courante à notre application Web. Essentiellement, chaque client connecté à votre application a un 
                    espace mémoire pour y stocker des données sur le serveur. Si le client n'intéragit pas avec le 
                    serveur pendant un certain temps, ces données sont automatiquement supprimé pour faire de la place 
                    sur le serveur.
                </p>
                <p>
                    Cette pratique est donc très utile pour les services de connexion, pour les applications 
                    utilisant beaucoup de communication en temps réel ou simplement pour enregistrer des données 
                    temporaires pour chaque utilisateur.
                </p>
            </section>

            <section>
                <h2>Démarrer une session</h2>
                <p>
                    Pour pouvoir utiliser une session, il tout d'abords la démarrer. Dans chacun de vos fichiers PHP 
                    utilisant les sessions, vous devez donc ajouter la fonction suivante:
                </p>
                <CodeBlock language="php">{ startSession }</CodeBlock>
                <p>
                    Si vous n'exécutez pas cette fonction, vous ne pourrez pas utiliser les sessions dans votre 
                    fichier PHP.
                </p>
            </section>

            <section>
                <h2>Manipuler les données dans une session</h2>
                <p>
                    Une fois les sessions démarrées, vous aurez accès à la variable <IC>$_SESSION</IC>. Cette variable 
                    est en fait un tableau associatif (clé-valeur) dans lequel vous pouvez manipuler les données de la 
                    session de l'utilisateur qui est présentement en train de faire un requête.
                </p>
                <p>
                    Pour ajouter des données dans la session, vous n'avez qu'à utiliser un code similaire à celui-ci:
                </p>
                <CodeBlock language="php">{ addSession }</CodeBlock>
                <p>
                    Si vous désirez ajouter des données automatiquement au démarrage de la session, je vous suggère 
                    toutefois un code un peu différent pour vous assurez de ne pas écraser vos données à chaque appel 
                    au serveur. La fonction <IC>isset()</IC> ci-dessous nous permettra de vérifier si la variable 
                    existe déjà ou non.
                </p>
                <CodeBlock language="php">{ addSessionStart }</CodeBlock>
                <p>
                    Vous pouvez par la suite accéder à vos données dans la session de l'utilisateur simplement en 
                    spécifiant sa clé de la façon suivante:
                </p>
                <CodeBlock language="php">{ getSession }</CodeBlock>
                <p>
                    Finalement, si vous voulez supprimer des données dans une session, vous pouvez utiliser la 
                    fonction <IC>unset()</IC> de la façon suivante:
                </p>
                <CodeBlock language="php">{ deleteSession }</CodeBlock>
            </section>
        </>;
    }
};
