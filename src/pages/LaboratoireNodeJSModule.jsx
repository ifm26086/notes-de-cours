import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-nodejs-module-solution.zip';

export default function LaboratoireNodeJSModule() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Écrire un programme en Node.js pour chacune des situations suivantes:
            </p>
            <ol>
                <li>
                    Programmer un algorithme de tri par bulle dans votre propre module pour pouvoir le réutiliser.
                </li>
                <li>
                    Trouvez et affichez tous les nombres premiers entre 2 et 1000. Séparez votre code qui indique 
                    si un nombre premier et le code pour trouver les nombres premiers entre 2 et 1000 dans 
                    2 modules différents.
                </li>
                <li>
                    Créer un programme qui génère 10 séquences aléatoires de 10 lettres. Vous devez ensuite faire 
                    taper par l'utilisateur ces 10 séquences une à une le plus vite possible. Calculez le nombre 
                    de millisecondes prise par l'utilisateur pour taper ces séquences. Vous pouvez 
                    utiliser <IC>Date.now()</IC> qui retourne le nombre de millisecondes depuis le 1 janvier 1970 
                    pour calculer le temps. Sauvegardez le nom et le temps en millisecondes du top 5 des joueurs 
                    les plus rapide. Vous devez afficher ce palmarès à la fin de votre programme. Séparez votre 
                    code dans plusieurs modules pour favoriser la lisibilité.
                </li>
            </ol>
        </section>

        <section>
            <h2>Solution</h2>
            <p>
                Merci à Sébastien Hamon pour sa contribution aux solutions.
            </p>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
