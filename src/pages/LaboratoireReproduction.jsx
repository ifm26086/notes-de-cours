import React from 'react';
import DownloadBlock from '../component/DownloadBlock';

import designMobile from '../resources/laboratoire-reproduction-mobile.png';
import designPC from '../resources/laboratoire-reproduction-pc.png';
import solution from '../resources/laboratoire-reproduction-solution.zip';

export default function LaboratoireReproduction() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <ol>
                <li>
                    À partir des images de design ci-dessous, essayer de 
                    reproduire l'interface en HTML et CSS. 
                </li>
                <li>
                    <p>
                        Les polices de caractères utilisées sont Roboto et 
                        Asap. Vous pouvez les retrouver sur le site de Google
                        Fonts.
                    </p>
                    <p>
                        <a href="https://fonts.google.com/" target="_blank" rel="noopener noreferrer">
                            Google Fonts
                        </a>
                    </p>
                </li>
                <li>
                    <p>
                        Les icônes peuvent être trouvé gratuitement sur iconmonstr.
                    </p>
                    <p>
                        <a href="https://iconmonstr.com/" target="_blank" rel="noopener noreferrer">
                            iconmonstr
                        </a>
                    </p>
                </li>
            </ol>
        </section>

        <section>
            <h2>Conseil</h2>
            <ul>
                <li>
                    Vous n'avez pas à programmer les comportements de l'interface graphique. Vous n'avez qu'à faire le 
                    HTML et CSS.
                </li>
                <li>
                    Les couleurs peuvent être approximative. Ne vous préoccupez pas trop de celle-ci.
                </li>
                <li>
                    Cet exercice est difficile, n'hésitez pas à vous référer à la solution si vous êtes bloqué.
                </li>
            </ul>
        </section>

        <section>
            <h2>Images de design</h2>
            <img src={ designMobile } alt="Design mobile" />
            <img src={ designPC } alt="Design PC" />
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
