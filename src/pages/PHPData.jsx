import React from 'react';

export default class PHPData extends React.Component {
    render() {
        return <>
            <section>
                <h2>Mise en situation</h2>
                <p>
                    Contrairement à un serveur Node.js, un serveur PHP n'offre pas de persistance de données. Cela 
                    veut dire que vos variables sont réinitialisé à chaque appel de votre route. Les applications Web
                    devant stoker ou partager des données entre les utilisateurs devront être bâtit un peu 
                    différemment.
                </p>
                <p>
                    Si vous voulez faire persister des données entre chaque appel, voici quelques solutions:
                </p>
            </section>

            <section>
                <h2>Un fichier</h2>
                <p>
                    Cette approche très simple vous permettra de garder les données et de les partager entre 
                    plusieurs clients. Essentiellement, à chaque fois qu'on doit aller chercher les données, on va 
                    lire le fichier. Si on doit modifier ou supprimer des données, on va modifier le fichier. Cette 
                    approche peut fonctionner pour de petites applications, mais causera beaucoup de problèmes 
                    autrement. Vous aurez, en effet, beaucoup de problèmes si vous recevez des requêtes HTTP 
                    simultanées qui doivent écrire dans le fichier.
                </p>
                <p>
                    Voici quelques liens qui peuvent vous aider si vous voulez utiliser cette approche:
                </p>
                <p>
                    <a href="https://www.w3schools.com/php/php_file_open.asp" target="_blank" rel="noopener noreferrer">
                        PHP File Open/Read/Close
                    </a>
                </p>
                <p>
                    <a href="https://www.w3schools.com/php/php_file_create.asp" target="_blank" rel="noopener noreferrer">
                        PHP File Create/Write
                    </a>
                </p>
            </section>

            <section>
                <h2>Une base de données</h2>
                <p>
                    Cette solution très robuste permet de faire persiter vos données de façon sécuritaire et efficace. 
                    Elle nécessite toutefois l'installation d'un serveur de base de données et la création d'une base 
                    de données, ce qui peut être beaucoup plus long à développer.
                </p>
                <p>
                    Voici un lien qui peut vous aider si vous voulez utiliser cette approche:
                </p>
                <a href="https://www.php.net/manual/en/book.mysqli.php" target="_blank" rel="noopener noreferrer">
                    MySQL Improved Extension
                </a>
            </section>

            <section>
                <h2>Un serveur de cache</h2>
                <p>
                    Une base de données peut être pratique, mais puisque la plupart des données sont enregistré sur 
                    le disque dur, l'accès aux données peut être un peu plus lente. Si vous avez vraiment besoin de 
                    performances, c'est plutôt un serveur de cache qui vous sera utile. Un serveur de cache vous 
                    permettra de stocker des données directement dans la mémoire vive du serveur et d'y accéder à la 
                    vitesse de l'éclair!
                </p>
                <p>
                    Voici un lien qui peut vous aider si vous voulez utiliser cette approche:
                </p>
                <a href="https://www.php.net/manual/en/book.memcached.php" target="_blank" rel="noopener noreferrer">
                    Memcached
                </a>
            </section>

            <section>
                <h2>Une session</h2>
                <p>
                    Une session est un moyen de sauvegarder des informations de façon temporaire pour chaque connexion 
                    courante à notre application Web. Essentiellement, chaque client connecté à votre application a un 
                    espace mémoire pour y stocker des données sur le serveur. Si le client n'intéragit pas avec le 
                    serveur pendant un certain temps, ces données sont automatiquement supprimé pour faire de la place 
                    sur le serveur. Cette pratique est donc très utile pour les services de connexion ou pour les 
                    applications utilisant beaucoup de communication en temps réel. En arrière-plan, une session 
                    utilise en fait un fichier ou un serveur de cache pour fonctionner.
                </p>
                <p>
                    Nous verrons dans les prochaines pages comment utiliser les sessions en PHP.
                </p>
            </section>
        </>;
    }
};
