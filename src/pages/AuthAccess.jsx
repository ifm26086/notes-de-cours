import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const error401 =
`app.post('/truc-secret', async (request, response) => {
    if (!request.user) {
        // Si l'utilisateur n'est pas connecté, on
        // retourne l'erreur 401
        response.sendStatus(401);
    }
    else {
        // Restant du code de la route ...
    }
});`;

const error401HTML =
`app.get('/page-secrete', async (request, response) => {
    if (!request.user) {
        // Si l'utilisateur n'est pas connecté, on
        // retourne l'erreur 401
        response.sendStatus(401);
    }
    else {
        response.render('page-secrete', {
            title: 'Une page secrete'
        });
    }
});`;

const error403 =
`app.patch('/truc-super-secret', async (request, response) => {
    if (!request.user) {
        response.sendStatus(401);
    }
    else if (request.user.niveau_acces !== 2) {
        response.sendStatus(403);
    }
    else {
        // Restant du code de la route ...
    }
});`;

const passUser =
`app.get('/', async (request, response) => {
    response.render('accueil', {
        title: 'Accueil',
        user: request.user
    });
});`;

const ifUser =
`<!--
On affiche le bouton de déconnexion uniquement si
l'utilisateur est connecté
-->
{{#if user}}
    <form method="post" action="/deconnexion">
        <input type="submit" value="Deconnexion" />
    </form>
{{/if}}

<!--
On affiche le lien vers la page de connexion
uniquement si l'utilisateur n'est pas connecté
-->
{{#unless user}}
    <a href="/connexion">Connexion</a>
{{/unless}}`;

const showUser =
`{{#if user}}
    <div>Courriel:</div>
    <div>{{user.courriel}}</div>
{{/if}}`;

const isAdmin =
`app.get('/', async (request, response) => {
    response.render('accueil', {
        title: 'Accueil',
        user: request.user
        isAdmin: request?.user?.niveau_acces > 3
    });
});`;

const ifIsAdmin =
`{{#if isAdmin}}
    <a href="/gestion">Lien vers page administrateur</a>
{{/if}}`;

export default function AuthAccess() {
    return <>
        <section>
            <h2>Prévenir l'accès à une route</h2>
            <p>
                La gestion des accès se fait principalement dans le fichier <IC>server.js</IC>. Pour chaque route que
                nous avons, nous devons donc nous demander si l'utilisateur peut y envoyer une requête même s'il n'est
                pas connecté. Si vous voulez que seuls les utilisateurs connectés puissent utiliser une route, vous
                devrez utiliser la variable <IC>request.user</IC>, qui contient les données de l'utilisateur s'il est
                connecté. S'il n'est pas connecté, <IC>request.user</IC> sera <IC>undefined</IC> ce que nous pouvons
                utiliser pour vérifier que l'utilisateur est connecté:
            </p>
            <CodeBlock language="js">{error401}</CodeBlock>
            <p>
                Si vous voulez prévenir l'accès à une page complètement, assurez-vous que la page est généré sur le
                serveur, avec Handlebars par exemple. Puisque la génération de page serveur est utilisé dans vos
                routes, vous pourrez donc les protéger.
            </p>
            <CodeBlock language="js">{error401HTML}</CodeBlock>
        </section>

        <section>
            <h2>Accès spéciaux</h2>
            <p>
                Certains utilisateurs dans votre système auront peut-être des accès spéciaux de plus que d'autres
                utilisateur. Par exemple, un utilisateur administrateur aura peut-être droit d'utiliser des
                fonctionnalités supplémentaires dans votre site web. Pour se faire, assurez-vous de stocker les
                informations des accès spéciaux de l'utilisateur dans la base de données. Une fois fait, vous pourrez
                tester valider vos routes comme dans la section précédante en allant voir les données de l'utilisateur
                dans sa variable <IC>request.user</IC>.
            </p>
            <CodeBlock language="js">{error403}</CodeBlock>
            <p>
                Vous noterez dans le code ci-dessus que nous regardons encore si l'utilisateur est connecté. Cette
                opération est importante pour que la deuxième condition fonctionne correctement. Nous retournons ici
                une erreur 403 si l'utilisateur n'a pas le niveau d'accès pour faire la requête. Cette
                erreur <IC>Forbidden</IC> indique que l'utilisateur a l'interdiction d'exécuter cette requête.
            </p>
            <p>
                Vous pouvez aussi utiliser ce code pour empêcher un utilisateur d'accéder à une page, un peu comme
                dans la section précédante.
            </p>
        </section>

        <section>
            <h2>Cacher des éléments dans la page HTML</h2>
            <p>
                Si vous désirer cacher des éléments dans la page HTML si l'utilisateur est connecté, nous utiliserons
                des instructions dans les fichiers Handlebars. Pour bien se faire, vous devrez passer les données de
                l'utilisateur à la page <IC>.handlebars</IC>.
            </p>
            <CodeBlock language="js">{passUser}</CodeBlock>
            <p>
                En passant l'objet d'utilisateur <IC>request.user</IC> au fichier <IC>.handlebars</IC>, il nous sera
                possible de regarder si l'utilisateur existe dans le HTML avec les
                instructions <IC>{'{{#if}}'}</IC> et <IC>{'{{#unless}}'}</IC>.
            </p>
            <CodeBlock language="handlebars">{ifUser}</CodeBlock>
            <p>
                Il est aussi possible d'afficher l'information de l'utilisateur dans la page. Faites toutefois
                attention de ne pas afficher de l'information sensible.
            </p>
            <CodeBlock language="handlebars">{showUser}</CodeBlock>
            <p>
                Si vous voulez plutôt afficher des informations dans votre page uniquement pour les utilisateurs ayant
                un certain niveau d'accès, vous pourriez le faire de la façon suivante:
            </p>
            <CodeBlock language="js">{isAdmin}</CodeBlock>
            <CodeBlock language="handlebars">{ifIsAdmin}</CodeBlock>
            <p>
                Dans le code ci-dessus, on suppose qu'un utilisateur est administrateur si son niveau d'accès est
                supérieur à 3. Dans votre système, il se peut que votre code diffère.
            </p>
            <p>
                Vous noterez aussi l'utilisation de l'opérateur <IC>?.</IC>. Cet opérateur permet d'aller chercher une
                variable dans un objet comme l'opérateur <IC>.</IC>, mais ne lance pas d'erreur si l'objet
                est <IC>null</IC> ou <IC>undefined</IC>. Dans ce cas-ci, c'est pratique puisque nous ne savons pas si
                la variable <IC>request.user</IC> existe ou non.
            </p>
        </section>
    </>
}